package interview;

/**
 * 斐波那契数
 */
public class Fibonacci {

    /**
     * 第N个斐波那契数
     */
    private static long N = 13;

    public static void main(String[] args) {
        long startTime1 = System.currentTimeMillis();
        System.out.println(getFibonacci(N));
        System.out.println("consume time: "
                           + String.valueOf(System.currentTimeMillis() - startTime1) + "ms");

		long startTime2 = System.currentTimeMillis();
        System.out.println(getFibonacciByIterate(N));
		System.out.println("consume time: "
				+ String.valueOf(System.currentTimeMillis() - startTime2) + "ms");
	}

	
    public static long getFibonacci(long n) {
		long fibNMinusOne = 0;
		long fibNMinusTwo = 1;
		long fibN = 1;
		
        if (n == 1) {
			return fibNMinusOne;
        } else if (n == 2) {
			return fibNMinusTwo;
		}
		
        for (int i = 3; i <= n; i++) {
			fibN = fibNMinusOne + fibNMinusTwo;
			fibNMinusOne = fibNMinusTwo;
			fibNMinusTwo = fibN;
		}
		
		return fibN;
	}

    public static long getFibonacciByIterate(long n) {
        long fibNMinusOne = 0;
        long fibNMinusTwo = 1;
        long fibN = 0;

        if (n == 1) {
            return fibNMinusOne;
        } else if (n == 2) {
            return fibNMinusTwo;
        }

        for (long i = 3; i <= n; i++) {
            fibN = getFibonacciByIterate(n - 1) + getFibonacciByIterate(n - 2);
        }

        return fibN;
    }
}
