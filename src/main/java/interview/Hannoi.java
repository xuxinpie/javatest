package interview;

/**
 * ��ŵ������
 * @author XINUX
 *
 */
public class Hannoi {
	private int n;
	private char from;
	private char buffer;
	private char to;
	
	public Hannoi(int n, char from, char buffer, char to) {
		this.n = n;
		this.from = from;
		this.buffer = buffer;
		this.to = to;
	}
	
	public void hannoiMove(int n, char from, char buffer, char to) {
		if (n == 1) {
			System.out.println("Move disk " + n + "from " + from + "to " + to);
		} else {
			hannoiMove(n-1, from, to, buffer);
			System.out.println("Move disk " + n + "from" + from + "to" + to);
			hannoiMove(n-1, buffer, from, to);
		}
	}
}
