package interview;

/**
 * 动态规划解爬楼梯问题
 * 每次可以爬一级或者二级楼梯
 * Created by Xinux on 10/13/14.
 */

public class StepQuestion {
	public static int[] A = new int[100];
	public static void main(String[] args) {

	}

	public static int f3(int n) {
		if (n < 2) {
			A[n] = n;
		}
		if (n > 0) {
			return A[n];
		} else {
			A[n] = f3(n-1) + f3(n-2);
		}

		return A[n];

	}
}
