package interview;

public class ConsumerProducer {
	public static void main(String[] args) {
		SyncStack ss = new SyncStack();  //同步堆栈
		Producer p = new Producer(ss);
		Consumer c = new Consumer(ss);
		new Thread(p).start();
		new Thread(c).start();
	}
}


//定义一个窝头类WoTou  用于生产和消费
class WoTou {
	int id;

	public WoTou(int id) {
		this.id = id;
	}

	public String toString() {
		return "WoTou : " + id;
	}
}

class SyncStack {
	int index = 0;
	WoTou[] arryWT = new WoTou[6];

	public synchronized void push(WoTou wt) {
		while (index == arryWT.length) {
			try {
				this.wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		this.notify();
		arryWT[index] = wt;
		index++;
	}

	public synchronized WoTou pop() {
		while (index == 0) {
			try {
				this.wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		this.notify();
		index--;
		return arryWT[index];
	}
}

class Producer implements Runnable {
	SyncStack ss = null;

	public Producer(SyncStack ss) {
		this.ss = ss;
	}

	public void run() {
		for(int i = 0; i <= 20; i++) {
			WoTou wt = new WoTou(i);
			ss.push(wt);
			System.out.println("生产了 " + wt);
			try {
				Thread.sleep((int)(Math.random() * 1000));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

class Consumer implements Runnable {
	SyncStack ss = null;

	public Consumer(SyncStack ss) {
		this.ss = ss;
	}
	public void run() {
		for(int i = 0; i <= 20; i++) {
			WoTou wt = ss.pop();
			System.out.println("消费了 " + wt);
			try {
				Thread.sleep((int)(Math.random() * 1000));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
