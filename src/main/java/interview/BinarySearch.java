package interview;

/**
 * 分别用递归和非递归的方法实现二分查找
 * 二分查找的前提是序列有序
 * Created by Xinux on 10/20/14.
 */

public class BinarySearch {
	Integer[] srcArray;
	int des;

	public BinarySearch(Integer[] srcArray, int des) {
		this.srcArray = srcArray;
		this.des = des;
	}

	//非递归方法实现
	public int binarySearch1() {
		int low = 0;
		int high = srcArray.length - 1;
		while(low <= high) {
			int mid = (low + high) / 2;
			if (des == srcArray[mid]) {
				return mid;
			} else if (des > srcArray[mid]) {
				low = mid + 1;
			} else {
				high = mid - 1;
			}
		}

		return -1;
	}

	//递归方法实现
	public int binarySearch2(int[] array, int low, int high, int key) {
		if (low <= high) {
			int mid = (low + high) / 2;
			if (key == array[mid]) {
				return mid;
			} else if (key > array[mid]) {
				return binarySearch2(array, mid+1, high, key);
			} else {
				return binarySearch2(array, low, mid-1, key);
			}
		}

		return -1;
	}
}
