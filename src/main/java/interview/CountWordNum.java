package interview;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * ͳ�Ƴ��ı��е��ʵĳ��ֵĸ���
 * Created by Xinux on 10/23/14.
 */
public class CountWordNum {
	public static void main(String[] args) throws IOException {
		/**
		 * ����һ��split�Ĺ��ܣ���collections ֮���ת��
		 */
		String str = "hello,java,c,c++,python,javascript";
		String[] sArray = str.split(",");
		for(int i = 0; i < sArray.length; i++) {
			System.out.print(sArray[i] + " ");
		}
		System.out.println();

		List<String> slist = Arrays.asList(sArray);  //������תΪList
		Iterator<String> iterator = slist.iterator(); //��listתΪiterator�������
		while(iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}
		System.out.println();

		//ͳ���ı��е��ʳ��ֵĴ��������е����á���������
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader("/Users/Xinux/helloXinux/input.txt"));
			Map<String, Integer> map = new HashMap<String, Integer>();
			String sline = br.readLine();
			StringBuffer sb = new StringBuffer();
			while(sline != null) {
				sb.append(sline);
				sline = br.readLine();
			}
			System.out.println(sb.toString());
			String[] array = sb.toString().split(",");
			for(int i = 0; i < array.length; i++) {
				String temp = array[i];
				if (map.containsKey(temp)) {
					map.put(temp, map.get(temp) + 1);
				} else {
					map.put(temp, 1);
				}
			}

			/**
			 * Map.Entry��Map������һ���ڲ��ӿڣ�
			 * �˽ӿ�Ϊ���ͣ�����ΪEntry<K,V>������ʾMap�е�һ��ʵ�壨һ��key-value�ԣ���
			 * �ӿ�����getKey(),getValue������
			 */
			//������ֵĵ����Լ����ʵĸ���
			for(Iterator<Map.Entry<String,Integer>> it = map.entrySet().iterator(); it.hasNext();) {
				Map.Entry<String, Integer> entry = it.next();
				System.out.println(entry.getKey() + " --> " + entry.getValue() + " times");
			}

			//������ֵĵ����Լ����ʵĸ�������iterator.iterator���㣬�ʺ��������ʱ���Ƽ�
			/*for(Map.Entry<String, Integer> entry : map.entrySet()) {
				System.out.println(entry.getKey() + " --> " + entry.getValue() + " times");
			}*/

			//����һ�����ܣ�ʵ�ְ����ʸ����Ӵ�С���
			List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>();
			list.addAll(map.entrySet());
			CountWordNum.VauleComparator vc = new VauleComparator();
			Collections.sort(list, vc);
			for(Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
				Map.Entry<String, Integer> entry = it.next();
				System.out.println(entry.getKey() + " --> " + entry.getValue() + " times");
			}


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			br.close();
		}

	}

	//ʵ��һ���Ƚ����࣬��дcompare����
	private static class VauleComparator implements Comparator<Map.Entry<String, Integer>> {

		public int compare(Map.Entry<String, Integer> m, Map.Entry<String, Integer> n) {
			return n.getValue() - m.getValue();
		}
	}
}
