package interview;


/**
 * 用递归方法求阶乘
 * @author XINUX
 *
 */
public class Factorial {
	public static void main(String[] args) {
		int  n = 5;
		Rec fr = new Rec();
		long result = fr.rec(n);
		System.out.println(n + "! = " + result);
	}

	public static class Rec {

		public long rec(int n) {
			if (n < 0) {
				System.out.println("Error: Parameter error!");
				return -1;
			}
			long value = 0;
			if (n == 0 || n == 1) {
				value = 1;
			} else {
				value = n * rec(n-1);
			}

			return value;
		}

	}
}
