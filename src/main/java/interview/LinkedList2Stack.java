package interview;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

/**
 * 将链表转换为栈
 */
public class LinkedList2Stack {
	public static void main(String[] args) {
		LinkedList<Integer> aList = new LinkedList<Integer>();
		Stack<Integer> stack = new Stack<Integer>();
		for (int i = 0; i < 20; i++) {
			aList.add(i);
		}
//		System.out.println(aList);
		Iterator<Integer> iterator = aList.iterator();
		while(iterator.hasNext()) {
			stack.push(iterator.next());
		}
		
		while(!stack.isEmpty()) {
			System.out.println(stack.pop());
		}
		
	}
}
