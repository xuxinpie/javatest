package interview;

public class ThreadOrder {
	private boolean isThreadA = true;
	private boolean isThreadB = false;
	private boolean isThreadC = false;
	
	private Object lock = new Object();   //作为一个互斥量
	private static int PRINTTIMES = 10;


	//synchronized的底层实现主要依靠Lock-Free的队列，
	//基本思路是自旋后阻塞，竞争切换后继续竞争锁，稍微牺牲了公平性，但获得了高吞吐量
	public static void main(String[] args) {
		ThreadOrder tOrder = new ThreadOrder();
		new Thread(tOrder.new ThreadA()).start();
		new Thread(tOrder.new ThreadB()).start();
		new Thread(tOrder.new ThreadC()).start();
	}
	
	public class ThreadA implements Runnable {

		@Override
		public void run() {
			int count = 0;
			while(count < PRINTTIMES) {
				synchronized(lock) {
					if (isThreadA) {
						System.out.print("A");
						count++;
						isThreadA = false;
						isThreadB = true;
						isThreadC = false;
						lock.notifyAll();
					} else {
						try {
							lock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
	}
	
	public class ThreadB implements Runnable {

		@Override
		public void run() {
			int count = 0;
			while(count < PRINTTIMES) {
				synchronized(lock) {
					if (isThreadB) {
						System.out.print("B");
						count++;
						isThreadA = false;
						isThreadB = false;
						isThreadC = true;
						lock.notifyAll();
					} else {
						try {
							lock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
	}
	
	public class ThreadC implements Runnable {

		@Override
		public void run() {
			int count = 0;
			while(count < PRINTTIMES) {
				synchronized(lock) {
					if (isThreadC) {
						System.out.print("C");
						count++;
						isThreadA = true;
						isThreadB = false;
						isThreadC = false;
						lock.notifyAll();
					} else {
						try {
							lock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		
	}
}
