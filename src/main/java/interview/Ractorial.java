package interview;

/**
 * 例如 input = 75  输出 18�?15+3�?
 */
import java.util.Scanner;

public class Ractorial {
	public static void main(String[] args) {
		Scanner cin = new Scanner(System.in);
		int input = cin.nextInt();
		while(input != 0) {
			System.out.println(trailingZeros(input));
			input = cin.nextInt();
		}
		System.out.println();
	}

	private static int trailingZeros(int input) {
		int quotient;
		int fiveFactors = 0;
		
		quotient = input / 5;
		int i = 1;
		while(quotient != 0) {
			fiveFactors += quotient;
			i++;
			quotient = (int) (input / Math.pow(5, i));
		}
		
		return fiveFactors;
	}
}
