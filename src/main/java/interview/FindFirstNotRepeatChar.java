package interview;

import java.util.HashMap;
import java.util.Map;

public class FindFirstNotRepeatChar {
	private String str;

	public FindFirstNotRepeatChar(String s) {
		str = s;
	}

	//方法1：利用数组模拟HashMap
	public char find() {
		char[] cArray = str.toCharArray();
		int[] array = new int[256];
		for (int i = 0; i < array.length; i++) {
			array[i] = 0;
		}
		for (int i = 0; i < cArray.length; i++) {
			array[(int)cArray[i]]++;
		}
		for (int i = 0; i < array.length; i++) {
			//依然是按照cArray的顺序来进行查找
			if (array[(int)cArray[i]] == 1) {
				return cArray[i];
			}
		}

		return 0;
	}

	//方法2：利用HashMap实现
	public char find1() {
		Map<Character, Integer> count = new HashMap<Character, Integer>();
		char[] cTemp = str.toCharArray();
		for(char c : cTemp) {
			if (count.containsKey(c)) {
				int value = count.get(c);
				count.put(c, ++value);
			} else {
				count.put(c, 1);
			}
		}

		for(char key : cTemp) {
			if (count.get(key) == 1) {
				return key;
			}
		}

		return 0;
	}
}
