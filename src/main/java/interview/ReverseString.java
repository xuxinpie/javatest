package interview;

/**
 * 翻转字符串，但对字符串语句中的单词不执行反转
 * e.g.   "I love China"  --->   "China love I"
 * 1. 首先将整个字符串进行翻转
 * 2. 对字符串中的单词进行翻转,变为正确的顺序
 * Created by Xinux on 10/19/14.
 */
public class ReverseString {
	private String s;

	public ReverseString(String s) {
		this.s = s;
	}

	private void reverse(char[] cArray, int begin, int end) {
		while(begin < end) {
			swap(cArray, begin, end);
			begin++;
			end--;
		}
	}

	private void swap(char[] array, int i, int j) {
		char temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

	public String reverseSentence(String str, int begin, int end) {
		if (begin < 0 || end > str.length() || begin > end) {
			System.out.println("Error: parameters error!");
			return " ";
		}
		char[] cArray = str.toCharArray();
		reverse(cArray, begin, end);
		System.out.println(String.valueOf(cArray));
		int pStart = 0;
		int pEnd = 0;
		while(pEnd <= end) {
			if (cArray[pStart] == ' ') {
				pStart++;
				pEnd++;
			} else if (cArray[pEnd] == ' ') {
				reverse(cArray, pStart, pEnd-1);
				pStart = pEnd;
			} else {
				pEnd++;
			}
		}

		return String.valueOf(cArray);
	}
}
