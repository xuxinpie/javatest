package interview;

import java.util.Collections;
import java.util.List;

/**
 * 寻找数组中超过一半相同的数
 */
public class FindHalfNumber {
	private List<Integer> array;

	public FindHalfNumber(List<Integer> array) {
		this.array = array;
	}

	public int find() {
		Collections.sort(array);
		int count = 1;
		for (int i = 1; i < array.size()-1; i++) {
			if (array.get(i) == array.get(i+1)) {
				count++;
				if (count * 1.0 / array.size() >= 0.5) {            // count * 1.0 先将int型转成double类型再计算
					return array.get(i);
				}
			} else {
				count = 1;
			}
		}

		System.out.println("don't exists half number");
		return 0;

	}

	//不用先进行排序，时间复杂度为O(n)
	public int find1() {
		int times = 1;
		int result = array.get(0);

		for (int i = 0; i < array.size(); i++) {
			if (times == 0) {
				result = array.get(i);
				times = 1;
			} else if (array.get(i) == result) {
				times++;
			} else {
				times--;
			}
		}

		return result;
	}
}
