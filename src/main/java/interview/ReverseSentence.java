package interview;

/**
 * 翻转字符串
 * 实现字符串左旋操作，将字符串前面的若干的字符转移到字符串的尾部
 * @author XINUX
 *
 */
public class ReverseSentence {
	private String s;

	public ReverseSentence(String string) {
		s = string;
	}

	public String reverse(String s, int begin, int end) {
		if (begin < 0 || end > s.length() || begin > end) {
			System.out.println("Error: parameters error!");
			return " ";
		}
		char[] cArray = s.toCharArray();
		char temp;
		while(begin < end) {
			temp = cArray[begin];
			cArray[begin] = cArray[end];
			cArray[end] = temp;
			begin++;
			end--;
		}

		return String.valueOf(cArray);
	}

	//左旋字符串，
	public String leftRotateString(String s, int n) {
		int length = s.length();
		String s1 = null;
		String s2 = null;
		String s3 = null;
		if (n > 0 && length > 0 && n < length) {
			int firstStart = 0;
			int firstEnd = n - 1;
			int secondStart = n;
			int secondEnd = length - 1;

			s1 = reverse(s, firstStart, firstEnd);
			s2 = reverse(s1, secondStart, secondEnd);
			s3 = reverse(s2, firstStart, secondEnd);
		}

		return s3;
	}
}
