/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package interview;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * 用两个队列实现栈
 * 1. 入栈都插入queue1
 * 2. 出栈从queue2中取
 *
 * @author Xinux
 * @version $Id: Queue2Stack.java, v 0.1 2016-07-12 5:23 PM Xinux Exp $$
 */
public class Queue2Stack {

    private Queue<String> queue1 = new ArrayBlockingQueue<String>(10);
    private Queue<String> queue2 = new ArrayBlockingQueue<String>(10);

    /**
     * 构造函数
     * @param queue1
     * @param queue2
     */
    public Queue2Stack(Queue<String> queue1, Queue<String> queue2) {
        this.queue1 = queue1;
        this.queue2 = queue2;
    }

    /**
     * 入栈
     * @param value
     */
    public void push(String value) {
        queue1.add(value);
    }

    /**
     * 出栈
     * @return
     */
    public String pop() {
        if (getStackSize() > 0) {
            if (!queue1.isEmpty()) {
                // queue1为空,需要将queue1中的数据移动到queue2中,保留队尾元素
                moveAllToAnOtherQueue();
                return queue1.remove();
            } else {
                // queue2为空,需要将queue1中的数据移动到queue1中,保留队尾元素
                moveAllToAnOtherQueue();
                return queue2.remove();
            }
        } else {
            System.out.println("栈已为空,不能出栈!");
            return null;
        }
    }

    /**
     * 从非空中出队n-1个到另一个队列
     */
    private void moveAllToAnOtherQueue() {
        if (!queue1.isEmpty()) {
            while (queue1.size() > 1) {
                queue2.add(queue1.remove());
            }
        } else if (!queue2.isEmpty()) {
            while (queue2.size() > 1) {
                queue1.add(queue2.remove());
            }
        }

    }

    /**
     * 获取栈大小
     * 等于两个队列之和
     * @return
     */
    private int getStackSize() {
        return queue1.size() + queue2.size();
    }
}