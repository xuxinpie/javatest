package interview;
/**
 * 求最大公约数和最小公倍数
 */
import java.util.Scanner;

public class CommonMultiple {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
        System.out.println("请键入第一个数：");
		int num1 = scanner.nextInt();
        System.out.println("请键入第二个数：");
		int num2 = scanner.nextInt();

        //求最大公约数
		int commonDivisor = commonDivisor(num1, num2);

        //求最小公倍数
		int commonMultiple = num1 * num2 / commonDivisor;

		System.out.println("Greatest Common Multiple is: " + commonMultiple);
		System.out.println("15Lowest Common Divisor is: " + commonDivisor);
	}

	/**
	 * 求最大公约数
	 *
	 * @param num1
	 * @param num2
     * @return
     */
	private static int commonDivisor(int num1, int num2) {
		// 比较num1和num2大小,其中大值赋值给num1
		if (num1 < num2) {
			num1 = num1 + num2;
			num2 = num1 - num2;
			num1 = num1 - num2;
		}

		while(num2 != 0) {
			if (num1 == num2) {
				return num1;
			} else {
				int k = num1 % num2;
				num1 = num2;
				num2 = k;
			}
		}

		return num1;
	}

}
