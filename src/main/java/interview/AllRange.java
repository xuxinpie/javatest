package interview;


/**
 * 全排列问题
 * @author XINUX
 *
 */
public class AllRange {

	public static int total = 0;
	public static void swap(String[] str, int i, int j) {
		String temp = new String();
		temp = str[i];
		str[i] = str[j];
		str[j] = temp;
	}

	public static void allrange(String[] str, int begin, int length) {
		if (begin == length-1) {
			for (int i = 0; i < str.length; i++) {
				System.out.print(str[i] + "  ");
			}
			System.out.println();
			total++;
		} else {
			for (int i = begin; i < str.length; i++) {
				swap(str, begin, i);
				allrange(str, begin+1, length);
				swap(str, begin, i);
			}
		}
	}


	public static void main(String[] args) {
		String[] str = {"a", "b", "c"};
		allrange(str, 0, str.length);
		System.out.println(total);
	}

}
