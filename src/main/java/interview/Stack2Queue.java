package interview;

import java.util.Stack;

/**
 * 用两个栈实现队列
 * stack1专门用作入栈 stack2专门用作出栈
 *
 */
public class Stack2Queue {
		private Stack<Integer> stack1;
		private Stack<Integer> stack2;
		
		public Stack2Queue(Stack<Integer> s1, Stack<Integer> s2) {
			this.stack1 = s1;
			this.stack2 = s2;
		}
		
		public void add(Integer item) {
			stack1.push(item);
		}
		
		public Integer poll() {
			Integer head = new Integer(0);
			if (stack2.size() > 0) {
				head = stack2.pop();
			} else if(stack2.size() == 0) {
            	if (stack1.size() == 1) {
                	head = stack1.pop();
            	} else {
                	// 如果stack2为空,需要将stack1中的数据都放到stack2中
                	while (stack1.size() > 0) {
                    	stack2.push(stack1.pop());
                	}
                	head = stack2.pop();
				}
			} 
				
			return head;  
	}
}
		

