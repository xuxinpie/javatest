package interview;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class ThreadOrder2 {
	public static void main(String[] args) {
		ThreadOrder2 obj = new ThreadOrder2();
		obj.init();
	}

	private void init() {
		final TestBusiness tb = new TestBusiness();
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				for (int i = 0; i < 10; i++) {
					tb.printA();
				}
			}
		});
		t1.setName("A");
		
		Thread t2 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (int i = 0; i < 10; i++) {
					tb.printB();
				}
			}
		});
		t2.setName("B");
		
		Thread t3 = new Thread(new Runnable() {
			
			@Override
			public void run() {
				for (int i = 0; i < 10; i++) {
					tb.printC();
				}
			}
		});
		t3.setName("C");
		
		
		t1.start();
		t2.start();
		t3.start();
		
	}
	
	class TestBusiness {
		
		private String flag = "A";
		private Lock lock = new ReentrantLock();
		private Condition conditionA = lock.newCondition();
		private Condition conditionB = lock.newCondition();
		private Condition conditionC = lock.newCondition();
		
		public void printA() {
			try {
				lock.lock();
				if (!flag.equals("A")) {
					conditionA.await();
				}
				System.out.println(Thread.currentThread().getName());
				Thread.sleep(1000);
				flag = "B";
				conditionB.signal();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
			
		}

		public void printB() {
			try {
				lock.lock();
				if (!flag.equals("B")) {
					conditionB.await();
				}
				System.out.println(Thread.currentThread().getName());
				Thread.sleep(1000);
				flag = "C";
				conditionC.signal();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
		}

		public void printC() {
			try {
				lock.lock();
				if (!flag.equals("C")) {
					conditionC.await();
				}
				System.out.println(Thread.currentThread().getName());
				Thread.sleep(1000);
				flag = "A";
				conditionA.signal();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
		}
		
	}
}
