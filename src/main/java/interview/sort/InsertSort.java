package interview.sort;

public class InsertSort {
	private int[] array;
	
	public InsertSort(int[] array) {
		this.array = array;
	}
	
	public void sort() {
		for (int i = 0; i < array.length; i++) {
			for (int j = i; j > 0 && array[j] < array[j-1] ; j--) {
				swap(array, j, j-1);
			}
		}
	}

	private void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}
