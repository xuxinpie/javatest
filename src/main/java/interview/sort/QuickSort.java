package interview.sort;

/**
 * 快速排序
 */

public class QuickSort {
	
	public int partition(int[] a, int start, int end) {
		if(start < 0 || a.length <= end) {
			System.err.println("Error: parameters error!");
			return -1;
		}
		
		int i = start;
		int j = end;
		int pivot = a[i];
		while(i < j) {
			while(i < j && pivot <= a[j]) {
				j--;
			}
			if (i < j) {
				a[i] = a[j];
				i++;
			}
			
			while(i < j && pivot >= a[i]) {
				i++;
			}
			if (i < j) {
				a[j] = a[i];
				j--;
			}
		}
		a[i] = pivot;
		return i;
	}
	
	public void sort(int[] a, int start, int end) {
		if (start < end) {
			int i = partition(a, start, end);
			sort(a, start, i - 1);
			sort(a, i + 1, end);
		}
	}
}
