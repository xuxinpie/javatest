package interview.sort;

/**
 * 二路归并排序
 * @author XINUX
 *
 */

public class MergeSort {
//	private int[] array;
//
//	public MergeSort(int[] array) {
//		this.array = array;
//	}

	public void sort(int[] array, int left, int right) {
		if (left >= right) {
			return;
		}

		int mid = (left + right) / 2;

		sort(array, left, mid);
		sort(array, mid+1, right);
		merge(array, left, mid, right);
	}

	private void merge(int[] array, int left, int mid, int right) {

		int[] tmp = new int[array.length];
		int rl = mid + 1;
		int tIndex = left;
		int cIndex = left;

		while(left <= mid && rl <= right) {
			if(array[left] <= array[rl]) {
				tmp[tIndex++] = array[left++];
			} else {
				tmp[tIndex++] = array[rl++];
			}
		}

		//将左边剩余的元素归并进临时数组
		while(left <= mid) {
			tmp[tIndex++] = array[left++];
		}

		//将右边剩余的元素归并进临时数组
		while(rl <= right) {
			tmp[tIndex++] = array[rl++];
 		}

		//从临时数组拷贝进原数组
		while(cIndex <= right) {
			array[cIndex] = tmp[cIndex];
			cIndex++;
		}
	}
}
