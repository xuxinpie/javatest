package interview.sort;

public class ShellSort {
	private int[] al;
	private int length;
	
	public ShellSort(int[] al, int length) {
		this.al = al;
		this.length = length;
	}
	
	public void sort1() {
		int i, j, gap;
		
		for (gap = length / 2;  gap > 0; gap /= 2) {
			for (i = 0; i < gap; i++) {
				for (j = i + gap;  j < length; j += gap) {
					if (al[j] < al[j - gap]) {
						int temp = al[j];
						int k = j - gap;
						while(k >= 0 && al[k] > temp){
							al[k + gap] = al[k];
							k -= gap;
						}
						
						al[k + gap] = temp;
					}
					
				}
			}
		}
	}
	
	
	public void sort2() {
		for (int gap = length/2; gap > 0; gap /= 2) {
			for (int j = gap; j < length; j += gap) {
				if (al[j] < al[j - gap]) {
					int temp = al[j];
					int k = j - gap;
					while(k >= 0 && al[k] > temp) {
						al[k + gap] = al[k];
						k -= gap;
					}
					al[k+gap] = temp;
				}
			}
		}
	}
	
}
