package interview.sort;

public class SelectSort {
	private int[] array;
	
	public SelectSort(int[] array) {
		this.array = array;
	}
	
	public void sort() {
		for (int i = 0; i < array.length-1; i++) {
			int lowIndex = i;
			for (int j = i; j < array.length; j++) {
				if (array[j] < array[lowIndex] ) {
					lowIndex = j;
				}
			}
			swap(array, lowIndex, i);
		}
	}

	private void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}
