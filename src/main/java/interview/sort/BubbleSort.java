package interview.sort;

public class BubbleSort {
	private int[] a;

	public BubbleSort(int[] a) {
		this.a = a;
	}

	public void sort() {
		if (a.length <= 0) {
			System.out.println("Error: Array Length Error");
		}
		//设置交换标志
		Boolean exchange = true;
		for (int i = 0; i < a.length - 1; i++) {
			exchange = false;
			for (int j = i + 1; j < a.length; j++) {
				if (a[i] > a[j]) {
					int temp = a[i];
					a[i] = a[j];
					a[j] = temp;
					exchange = true;
				}
				//如果本次排序未发生交换，则提前停止算法
				if (!exchange) {
					return;
				}
			}
		}
	}

	public void swap(int[] array, int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j]= temp;
	}
}
