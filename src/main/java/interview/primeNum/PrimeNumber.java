package interview.primeNum;

/**
 * 求101-200之间都多少个素数
 * @author XINUX
 *
 */
public class PrimeNumber {
	public static void main(String[] args) {
		int primeCount = 0;
		boolean isPrimeNum = true;
		for (int i = 101; i <= 200; i+=2) {
			int j = 2;
			isPrimeNum = true;
			for (; j <= Math.sqrt(i); j++) {
				if (i % j == 0) {
					isPrimeNum = false;
					break;
				}
			}
			if (isPrimeNum) {
				primeCount++;
				System.out.println(i);
			}
		}

		System.out.println("素数的个数是：" + primeCount);
	}
}
