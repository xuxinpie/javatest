/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package interview.primeNum;

/**
 * @author Xinux
 * @version $Id: judgePrimeNum.java, v 0.1 2016-07-21 10:46 AM Xinux Exp $$
 */
public class judgePrimeNum {

    private int num;

    public judgePrimeNum(int num) {
        this.num = num;
    }

    /**
     * 判断是否为质数
     *
     * @return
     */
    public boolean isPrime() {
        if (1 == num) {
            return false;
        }

        for (int i = 2; i < Math.sqrt(num); i++) {
            if (0 == num % i) {
                return false;
            }
        }

        return true;
    }
}