/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package interview.primeNum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 获取0~n范围内的素数
 *
 * @author Xinux
 * @version $Id: GetPrimeNumber.java, v 0.1 2016-07-12 5:50 PM Xinux Exp $$
 */
public class GetPrimeNumber {

    private static int num;

    public GetPrimeNumber(int num) {
        this.num = num;
    }

    public static List<Integer> getAllPrimeNums() {

        List<Integer> primeNums = new ArrayList<Integer>();

        if (num <= 0) {
            System.out.println("输入num有误");
            return Collections.emptyList();
        }

        if (num == 1) {
            System.out.println("1不是素数");
            return Collections.emptyList();
        }
        if (num == 2) {
            primeNums.add(2);
            return primeNums;
        }

        primeNums.add(2);
        boolean isPrime = true;
        for (int i = 3; i <= num; i++) {
            isPrime = true;
            for (int j = 2; j <= Math.sqrt(num); j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                primeNums.add(i);
            }

        }

        return primeNums;
    }

    public static void main(String[] args) {
        GetPrimeNumber getPrimeNumber = new GetPrimeNumber(2);
        List<Integer> primes = getPrimeNumber.getAllPrimeNums();

        for (Integer item : primes) {
            System.out.print(item + " ");
        }
    }

}