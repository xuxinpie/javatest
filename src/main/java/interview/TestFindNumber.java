package interview;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestFindNumber {
	public static void main(String[] args) {
//		//找出出现次数超过一半的数
//		List<Integer> list = new ArrayList<>(Arrays.asList(1,2,3,2,2,2,5,4,2));
//		System.out.println(list);
//		FindHalfNumber findNumber = new FindHalfNumber(list);
//		int haldnum = findNumber.find1();
//		System.out.println("The half number is: " + haldnum);

		//找出字符串中第一个只出现一次的字符
		String string = "rbccddeeae";
		FindFirstNotRepeatChar findFirst = new FindFirstNotRepeatChar(string);
		System.out.println(findFirst.find());
		
		
	}
}
