package interview;

/**
 * 求某数字在已排序数组中出现的次数
 * @author XINUX
 *
 */
public class GetKnum {
	int[] data;

	public GetKnum(int[] data) {
		this.data = data;
	}

	public int getNumberofK(int[] data, int length, int k) {
		int number = 0;
		int first = getFirstK(data, length, k, 0, length-1);
		int last = getLastK(data, length, k, 0, length-1);

		number = last - first + 1;

		return number;
	}

	private int getLastK(int[] data, int length, int k, int start, int end) {
		if (start > end) {
			return -1;
		}

		int middleIndex = (start + end) / 2;
		int middleData = data[middleIndex];

		if (middleData == k) {
			if ((middleIndex > 0 && data[middleIndex-1] != k) || middleIndex == 0) {
				return middleIndex;
			} else {
				end = middleIndex - 1;
			}
		} else if(middleData > k){
			end = middleIndex - 1;
		} else {
			start = middleIndex + 1;
		}
		return getFirstK(data, length, k, start, end);
	}

	private int getFirstK(int[] data, int length, int k, int start, int end) {
		if (start > end) {
			return -1;
		}

		int middleIndex = (start + end) / 2;
		int middleData = data[middleIndex];

		if (middleData == k) {
			if ((middleIndex < end && data[middleIndex+1] != k) || middleData == length-1) {
				return middleIndex;
			} else {
				start = middleIndex + 1;
			}
		} else if (middleData > k) {
			end = middleIndex - 1;
		} else {
			start = middleIndex + 1;
		}
		return getLastK(data, length, k, start, end);

	}
}
