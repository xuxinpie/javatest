package interview;

/**
 * 查找和最大的子数组
 */
public class FindGreatestSumOfSubArray {
	private int[] array;
	
	public FindGreatestSumOfSubArray(int[] array) {
		this.array = array;
	}
	
	public int findSum() {
		int curSum = 0;
		int GreatestSum = 0;
		
		for (int i = 0; i < array.length; i++) {
			if (curSum <= 0) {
				curSum = array[i];
			} else {
				curSum += array[i];
			}
			
			if (curSum > GreatestSum) {
				GreatestSum = curSum;
			}
		}
		
		return GreatestSum;
	}
}
