package interview;

import java.util.Vector;

public class Permu {
	private Vector<String> v;
	
	public Permu() {
		v.add("");
	}
	
	public Vector<String> permu(String s) {
		if (s == "") {
			v.add("");
			return v;
		}
		for (int i = 0; i < s.length(); i++) {
			String firstChar = s.substring(i, 1);
			String subString = s;
			Vector<String> res = permu(subString.substring(i));
			for (int j = 0; j < res.size(); j++) {
				v.add(firstChar + res.get(i));
			}
		}
		
		return v;
	}
}
