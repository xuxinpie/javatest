package interview;

/**
 * Created by Xinux on 10/21/14.
 */
public class DeleteSubString {
	public static void main(String[] args) {
		String str = "Hello World! Xinux";
		String deleteStr = "Xinux";
		System.out.println(str.substring(0, 5));  // print the subString, from [0,5)
		System.out.println(deleteSubString(str, deleteStr));


	}

	public static String deleteSubString(String str, String toDeleteStr) {
		if (str.startsWith(toDeleteStr)) {
			return str.substring(toDeleteStr.length());
		} else if (str.endsWith(toDeleteStr)) {
			return str.substring(0, str.length() - toDeleteStr.length());
		} else {
			int index = str.indexOf(toDeleteStr);
			if (index != -1) {
				StringBuffer sb = new StringBuffer();
				sb.append(str.substring(0, index));
				sb.append((str.substring(index+toDeleteStr.length())));
				return sb.toString();
			} else {
				System.out.println("String: " + toDeleteStr + " not found!");
				return null;
			}
		}
	}
}
