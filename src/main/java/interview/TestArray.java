package interview;
/**
 * usage 在二维数组中查找一个数
 * @author XINUX
 *
 */
public class TestArray {
	static final int Search_Number = 7; 
	public static void main(String[] args) {
		int[] matrix = {1,2,8,9,2,4,9,12,4,7,10,13,6,8,11,15};
		boolean found = Find(matrix, 4, 4, Search_Number);
		System.out.println(found);
		
	}
	
	public static boolean Find(int[] matrix, int rows, int columns, int number) {
		boolean found = false;
		if (matrix != null && rows > 0 && columns > 0) {
			int row = 0;
			int column = columns - 1;
			while(row < rows && column > 0) {
				if(matrix[row * columns + column] == number) {
					found = true;
				} else if(matrix[row * columns + column] > number) {
					--column;
				} else {
					++row;
				}
			}
		}
		return found;
	}
}
