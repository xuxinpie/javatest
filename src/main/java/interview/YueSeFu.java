package interview;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class YueSeFu {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("请输入总人数：");
		int totalNum = scanner.nextInt();
		System.out.println("请输入报数的大小：");
		int cycleNum = scanner.nextInt();
		System.out.println();
		yuesefu(totalNum, cycleNum);
	}

	private static void yuesefu(int totalNum, int cycleNum) {
		//初始化人数
		List<Integer> cycle = new ArrayList<Integer>();
		for (int i = 0; i < totalNum; i++) {
			cycle.add(i);
		}

		int k = 0;
		while(cycle.size() > 0) {
			k += (cycleNum-1);
			//第m个人的索引位置
			k = k % (cycle.size());

			//判断是否到达队尾
			if (k + 1 == cycle.size()) {
				System.out.println(cycle.get(cycle.size() - 1));
				cycle.remove(cycle.size() - 1);
				k = 0;
			} else {
				System.out.println(cycle.get(k));
				cycle.remove(k);    //执行remove操作之后，后面的元素会顶上原来元素的位置
			}
			
		}
	}
}
