package interview;

import java.util.Collections;
import java.util.List;

/**
 * 给出5张牌，判断是不是顺子，大小王为0，可以看成是任意数字
 * @author XINUX
 *
 */
public class PokeSequence {
	List<Integer> list;

	public PokeSequence(List<Integer> list) {
		this.list = list;
	}

	//判断是否是顺子，即扑克牌连续
	public boolean isContinuous() {
		Collections.sort(list);
		int numberofZero = 0;
		int numberofGap = 0;

		for (int i = 0; i < list.size() - 1; i++) {
			if (list.get(i) == 0) {
				numberofZero++;
			}
		}

		//统计手牌中的间隔数
		int small = numberofZero;
		int big = small + 1;

		while(big < list.size()) {
			if (list.get(small) == list.get(big)) {
				return false;
			}

			numberofGap += list.get(big) - list.get(small);
			small = big;
			big++;
		}


//		if (numberofGap <= numberofZero) {
//			return true;
//		} else {
//			return false;
//		}

		//用三目运算符改写
		return numberofGap <= numberofZero ? true : false;
		
	}
}
