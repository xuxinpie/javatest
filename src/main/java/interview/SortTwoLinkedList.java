package interview;

import java.util.LinkedList;
import java.util.List;

public class SortTwoLinkedList {
	private List<Integer> ll1 = new LinkedList<Integer>();
	private List<Integer> ll2 = new LinkedList<Integer>();
	private List<Integer> ll3 = new LinkedList<Integer>();
	
	public SortTwoLinkedList(LinkedList<Integer> ll1, LinkedList<Integer> ll2) {
		this.ll1 = ll1;
		this.ll2 = ll2;
	}
	
	public List<Integer> merge() {
		int size = ll1.size() + ll2.size();
		int j = 0;
		int k = 0;
		for (int i = 0; i < size; i++) {
			if (!ll1.iterator().hasNext()) {
				ll3.addAll(j,ll2);
			} else if(!ll2.iterator().hasNext()){
				ll3.addAll(k,ll1);
			}
			
			if (ll1.get(j) < ll2.get(k)) {
				ll3.add(ll1.get(j));
				j++;
			} else {
				ll3.get(ll2.get(k));
				k++;
			}
		}
		
		return ll3;
 }


}