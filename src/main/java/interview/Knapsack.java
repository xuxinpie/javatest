package interview;

/**
 * 0-1 背包问题
 * @author XINUX
 *
 */

public class Knapsack {
	public static void main(String[] args) {
		int val[] = {10,40,30,50};
		int wt[] = {5,4,6,3};
		int W = 10;

		System.out.println(knapsack(val, wt, W));
	}

	private static int knapsack(int[] val, int[] wt, int W) {
		int N = wt.length;

		//创建矩阵
		int[][] V = new int[N+1][W+1];

		//将矩阵第一列全置为0
		for (int i = 0; i <= N; i++) {
			V[i][0] = 0;
		}

		//将矩阵第一行全置为0
		for (int i = 0; i <= W; i++) {
			V[0][i] = 0;
		}

		for (int item = 1; item <= N; item++) {
			for (int weight = 1; weight <= W; weight++) {
				if (wt[item-1] <= weight) {
					/**
					 * 计算不放入该物品时该重量的最大值
					 * 计算当前物品的价值 + 可以容纳的剩余重量的价值
					 */
					V[item][weight] = Math.max(V[item-1][weight], val[item-1] + V[item-1][weight-wt[item-1]]);
				} else {
					V[item][weight] = V[item-1][weight];

				}
			}
		}
		for(int[] rows : V) {
			for(int col : rows) {
				System.out.format("%5d", col);
			}
			System.out.println();
		}

		return V[N][W];
	}


}
