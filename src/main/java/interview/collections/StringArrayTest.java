/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package interview.collections;

import java.util.*;

/**
 * 求java数组的并集/交集/差集
 *
 * @author Xinux
 * @version $Id: StringArrayTest.java, v 0.1 2016-07-22 10:30 AM Xinux Exp $$
 */
public class StringArrayTest {

    public static void main(String[] args) {
        //求集合并集
        String[] arr1 = {"abc", "df", "abc","dd","bdfe"};
        String[] arr2 = {"abc", "cc", "df", "d", "abc"};
        String[] result_union = union(arr1, arr2);
        System.out.println("求并集的结果如下：");
        for (String str : result_union) {
            System.out.print(str + " ");
        }

        System.out.println();

        // 求集合交集
        String[] result_insect = intersect(arr1, arr2);
        System.out.println("求交集的结果如下：");
        for (String str : result_insect) {
            System.out.print(str + " ");
        }

        System.out.println();

        // 求集合差集
        String[] result_minus = substract(arr1, arr2);
        System.out.println("求差集的结果如下：");
        for (String str : result_minus) {
            System.out.print(str + " ");
        }

        System.out.println();
    }

    /**
     * 求集合的差集
     *
     * @param arr1
     * @param arr2
     * @return
     */
    private static String[] substract(String[] arr1, String[] arr2) {
        List<String> tempList = new LinkedList<String>();
        for (String str : arr1) {
            if (!tempList.contains(str)) {
                tempList.add(str);
            }
        }

        for (String str : arr2) {
            if (tempList.contains(str)) {
                tempList.remove(str);
            }
        }

        String[] result = {};
        return tempList.toArray(result);

    }

    /**
     * 求集合交集
     *
     * @param arr1
     * @param arr2
     * @return
     */
    private static String[] intersect(String[] arr1, String[] arr2) {
        List<String> tempList = new LinkedList<String>();
        Set<String> set = new HashSet<String>();
        for (String str : arr1) {
            if (!tempList.contains(str)) {
                tempList.add(str);
            }
        }

        for (String str : arr2) {
            if (tempList.contains(str)) {
                set.add(str);
            }
        }

        String[] result = {};
        return set.toArray(result);
    }

    /**
     * 求集合并集
     *
     * @param arr1
     * @param arr2
     * @return
     */
    private static String[] union(String[] arr1, String[] arr2) {

        Set<String> set = new HashSet<String>();
        for (String str : arr1) {
            set.add(str);
        }
        for (String str : arr2) {
            set.add(str);
        }

        String[] result = {};
        return set.toArray(result);
    }

}