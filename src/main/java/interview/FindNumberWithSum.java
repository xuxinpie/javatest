package interview;

import java.util.Collections;
import java.util.List;

/**
 * 在已经排序好的序列里寻找和等于s的两个数
 * @author XINUX
 *
 */
public class FindNumberWithSum {
	List<Integer> array;
	int sum;

	public FindNumberWithSum(List<Integer> data, int sum) {
		this.array = data;
		this.sum = sum;
	}

	public Integer[] findNumbers() {
		Collections.sort(array);
		Integer[] nums = new Integer[2];
		int begin = 0;
		int end = array.size() - 1;

		while(begin < end) {
			if (array.get(begin) + array.get(end) > sum) {
				begin++;
			} else if (array.get(begin) + array.get(end) < sum) {
				end--;
			} else {
				System.out.println("find two numbers: " + begin + " " + end);
				nums[0] = begin;
				nums[1] = end;
				return nums;
			}
		}

		System.out.println("can't find two numbers which sum result is : " + sum);
		return nums;
	}
}
