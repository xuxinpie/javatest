package random;

/**
 *
 * Created by Xinux on 10/13/14.
 */

/*If you want to generate a number from 0 to 100 then your code would look like this:

		(int)(Math.random() * 101);

		To generate a number from 10 to 20 :
		(int)(Math.random() * 11 + 10);

		In the general case:
		(int)(Math.random() * ((upperbound - lowerbound) + 1) + lowerbound);*/

public class MathRandom {
	public static void main(String[] args) {
		for(int i = 0; i < 10; i++) {
			System.out.print(randomWithRange(2,5) + " ");
		}
	}

	public static int randomWithRange(int min, int max) {
		int range = Math.abs(max - min);
		return (int) (Math.random() * range) + (min < max ? min : max);
	}
}
