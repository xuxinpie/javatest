/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package threadpool;

import java.io.Serializable;

/**
 * @author hanlin.xx
 * @version $Id: ThreadPoolTask.java, v 0.1 2015-06-27 18:36 hanlin.xx Exp $$
 */
public class ThreadPoolTask implements Runnable, Serializable {

	private static int consumeTaskSleepTime = 200;

	private Object threadPoolTaskData;

	public ThreadPoolTask(Object task) {
		this.threadPoolTaskData = task;
	}

	public void run() {
		System.out.println(Thread.currentThread().getName());
		System.out.println("start .." + threadPoolTaskData);

		try {
			Thread.sleep(consumeTaskSleepTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			threadPoolTaskData = null;
		}

	}


	public Object getThreadPoolTaskData() {
		return threadPoolTaskData;
	}
}

