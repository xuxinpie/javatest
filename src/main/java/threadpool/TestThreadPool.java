/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package threadpool;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 利用线程池来并行的执行计算
 * @author hanlin.xx
 * @version $Id: TestThreadPool.java, v 0.1 2015-06-27 18:29 hanlin.xx Exp $$
 */
public class TestThreadPool {

    private static int produceTaskSleepTime = 2;
    private static int produceTaskMaxNumber = 10;

    public static void main(String[] args) {
		/**
		 * corePoolSize: 2
		 * maximumPoolSize: 4 (最多三个线程在执行)
		 * keepAliveTime: 3s
		 */
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(2, 4, 3, TimeUnit.SECONDS,
            new ArrayBlockingQueue<Runnable>(3), new ThreadPoolExecutor.DiscardOldestPolicy());

        for (int i = 0; i <= produceTaskMaxNumber; i++) {
            try {
                String task = "task@ " + i;
                System.out.println("put " + task);
                threadPool.execute(new ThreadPoolTask(task));

                Thread.sleep(produceTaskSleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
