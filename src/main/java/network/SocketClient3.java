package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketClient3 extends Socket {
	private static final String SERVER_IP = "127.0.0.1";
	private static final int SERVER_PORT = 2014;
	
	private Socket client;
	private PrintWriter out;
	private BufferedReader in;
	
	
	/**
	 * 与服务器连接，并输出发送消息
	 * @throws IOException
	 * @throws UnknownHostException
	 *
	 */

	public SocketClient3() throws UnknownHostException, IOException {
		super(SERVER_IP, SERVER_PORT);
		client = this;
		out = new PrintWriter(this.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(this.getInputStream()));

		//开启客户端线程
		new ReadLineThread();

		while(true) {
			in = new BufferedReader(new InputStreamReader(System.in));
			String input = in.readLine();
			out.println(input);
		}
	}

	/**
	 * 用于监听服务端向客户端发送消息线程类
	 * @author XINUX
	 *
	 */
	class ReadLineThread extends Thread {
		private BufferedReader buff;

		public ReadLineThread() {
			try {
				buff = new BufferedReader(new InputStreamReader(client.getInputStream()));
				start();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		@Override
		public void run() {
			try {
				while(true) {
					String result = buff.readLine();
					if("byeClient".equals(result)) {
						break;
					} else {
						System.out.println(result);   //打印出服务端发送过来的消息
					}
				}

				in.close();
				out.close();
				client.close();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	public static void main(String[] args) {
		try {
			new SocketClient3();   //启动客户端
		} catch (Exception e) {
			
		}
	}
	
}
