package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServer2 extends ServerSocket {
	private static final int SERVER_PORT = 2014;
	
	public SocketServer2() throws IOException {
		super(SERVER_PORT);
		
		try {
			while(true) {
				Socket socket = accept();
				new CreateServerThread(socket);  //当有请求时，启一个线程处理
				
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	class CreateServerThread extends Thread {
		private Socket client;
		private BufferedReader bufferedReader;
		private PrintWriter printWriter;
		
		public CreateServerThread(Socket s) throws IOException {
			client = s;
			
			bufferedReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
			printWriter = new PrintWriter(client.getOutputStream(), true);
			System.out.println("Client(" + getName() + ") come in...");
			
			start();
			
		}
		
		@Override
		public void run() {
			try {
				String line = bufferedReader.readLine();
				
				while(!line.equals("bye")) {
					printWriter.println("continue, Client(" + getName() + ") !");
					line = bufferedReader.readLine();
					System.err.println("Client(" + getName() + ") says: " + line);
				}
				
				printWriter.println("bye, Client(" + getName() + ") !");
				System.out.println("Client(" + getName() + ") exit!");
				
				printWriter.close();
				bufferedReader.close();
				client.close();
				
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		new SocketServer2();
	}
}
