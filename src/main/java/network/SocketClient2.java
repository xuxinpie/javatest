package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketClient2 {
	public static void main(String[] args) {
		try {
			Socket socket = new Socket("127.0.0.1", 2014);
			socket.setSoTimeout(60000);  //���ó�ʱʱ��60s
			
			PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader buff = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
			String result = "";
			while(result.indexOf("bye") == -1) {
				BufferedReader buffsys = new BufferedReader(new InputStreamReader(System.in));
				printWriter.println(buffsys.readLine());
				printWriter.flush();
				
				result = buff.readLine();
				System.out.println("Server says: " + result);
			}
			
			printWriter.close();
			buff.close();
			socket.close();

			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
