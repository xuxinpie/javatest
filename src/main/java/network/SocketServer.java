package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.ClientInfoStatus;

public class SocketServer {
	public static void main(String[] args) {
		try {
			ServerSocket serverSocket = new ServerSocket(2014);
			//一直等待socket client的连接，请求到来则产生一个socket对象，并继续执行
			while(true) {
				Socket socket = serverSocket.accept();
				//获取客户端传来的消息
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				String result = bufferedReader.readLine();
				System.out.println("Client says: " + result);

				//发送服务端消息给客户端
				PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
				printWriter.print("hello Client, I'm a Server!");
				printWriter.flush();

				//关闭socket
				printWriter.close();
				bufferedReader.close();
				socket.close();
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			//ServerSocket.close();
		}
		
	}
}
