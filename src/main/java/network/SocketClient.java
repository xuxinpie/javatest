package network;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketClient {
	public static void main(String[] args) {
		try {
			Socket socket = new Socket("127.0.0.1", 2014);
			//60s timeout
			socket.setSoTimeout(60000);
			
			//由socket对象得到输出流，并构造PrintWriter对象
			PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
			//由控制台输入得到的信息输入到server
			BufferedReader sysBuffer = new BufferedReader(new InputStreamReader(System.in));
			printWriter.println(sysBuffer.readLine());
			//刷新输出流，是server马上收到该字符
			printWriter.flush();

			//获得服务端传输来的消息
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

			//打印出消息
			String result = bufferedReader.readLine();
			System.out.println("Server says: " + result);

			//关闭socket
			printWriter.close();
			bufferedReader.close();
			socket.close();
		
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
