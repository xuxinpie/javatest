/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package multiThread;

/**
 * 定义并启动一个线程
 *
 * 继承Thread类。Thread类自身已实现了Runnable接口，
 * 但它的run()方法中并没有定义任何代码。应用程序可以继承与Thread类，并复写run()方法。
 * @author Xinux
 * @version $Id: HelloThread.java, v 0.1 2015-07-11 12:00 PM Xinux Exp $$
 */
public class HelloThread extends Thread {

    public void run() {
        System.out.println("Say Hello from a thread");
    }

    public static void main(String[] args) {
        (new HelloThread()).start();
    }

}