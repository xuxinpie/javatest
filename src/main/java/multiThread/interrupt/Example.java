/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package multiThread.interrupt;

/**
 * 使用thread.interrupt()中断非阻塞状态线程
 * @author hanlin.xx
 * @version $Id: Example.java, v 0.1 2015-07-23 15:42 hanlin.xx Exp $$
 */
public class Example extends Thread {

	public static void main(String[] args) throws InterruptedException {
		Example thread = new Example();
		System.out.println("Starting thread...");

		thread.start();
		Thread.sleep(1000);
		System.out.println("asking thread to stop");

		//send interrupt request
		thread.interrupt();
		Thread.sleep(3000);
		System.out.println("Stopping application...");
	}

	public void run() {
		//check if interrupt identify was setted per second
		while (!Thread.currentThread().isInterrupted()) {
			System.out.println("Thread is running...");
			long time = System.currentTimeMillis();

			while ((System.currentTimeMillis() - time < 1000)) {

			}
		}

		System.out.println("Thread exiting under request...");
	}

}

