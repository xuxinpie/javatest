/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package multiThread.join;

/**
 * This java thread join method is used to wait for the thread on which it’s called to be dead or wait for specified milliseconds.
 * Since thread execution depends on OS implementation,
 * it doesn’t guarantee that the current thread will wait only for given time.
 *
 * 线程实例的方法join()方法可以使得一个线程在另一个线程结束后再执行。
 * 如果join()方法在一个线程实例上调用，当前运行着的线程将阻塞直到这个线程实例完成了执行。
 *
 * @author Xinux
 * @version $Id: ThreadJoinExample.java, v 0.1 2016-06-22 2:13 PM Xinux Exp $$
 */
public class ThreadJoinExample {

    public static void main(String[] args) {
        Thread thread1 = new Thread(new MyRunnable(), "t1");
        Thread thread2 = new Thread(new MyRunnable(), "t2");
        Thread thread3 = new Thread(new MyRunnable(), "t3");

        thread1.start();

        /**
         * wait()的作用是让“当前线程”等待，而这里的“当前线程”是指当前在CPU上运行的线程。
         * 所以，虽然是调用子线程的wait()方法，但是它是通过“主线程”去调用的；所以，休眠的是主线程，而不是“子线程”！
         */

        //start second thread after waiting for 2 seconds or if it's dead
        try {
            thread1.join(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread2.start();

        //start third thread only when first thread is dead
        try {
            thread1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        thread3.start();

        //let all threads finish execution before finishing main thread
        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println("All threads are dead, exiting main thread");
    }

}