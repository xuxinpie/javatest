/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package multiThread.join;

/**
 * @author Xinux
 * @version $Id: MyRunnable.java, v 0.1 2016-06-22 2:17 PM Xinux Exp $$
 */
class MyRunnable implements Runnable {

    public void run() {
        System.out.println("Thread started:::" + Thread.currentThread().getName());
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread ended:::" + Thread.currentThread().getName());
    }
}