/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package multiThread.yeild;

/**
 * Simple Producer
 *
 * @author Xinux
 * @version $Id: Producer.java, v 0.1 2016-06-22 2:24 PM Xinux Exp $$
 */
public class Producer implements Runnable {
    public void run()
    {
        for (int i = 0; i < 5; i++)
        {
            System.out.println("I am Producer : Produced Item " + i);
            Thread.yield();
        }
    }
}