/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package multiThread.yeild;

/**
 * Yield是一个静态的原生(native)方法
 * Yield告诉当前正在执行的线程把运行机会交给线程池中拥有相同优先级的线程。
 * Yield不能保证使得当前正在运行的线程迅速转换到可运行的状态
 * 它仅能使一个线程从运行状态转到可运行状态，而不是等待或阻塞状态
 *
 * @author Xinux
 * @version $Id: ThreadYeildExample.java, v 0.1 2016-06-22 2:23 PM Xinux Exp $$
 */
public class ThreadYeildExample {

    public static void main(String[] args) {
        // 创建了名为生产者和消费者的两个线程
        Thread producer = new Thread(new Producer(), "producer");
        Thread consumer = new Thread(new Consumer(), "consumer");

        // 生产者设定为最小优先级，消费者设定为最高优先级
        producer.setPriority(Thread.MIN_PRIORITY);
        consumer.setPriority(Thread.MAX_PRIORITY);

        /**
         * 在Thread.yield()注释和非注释的情况下我将分别运行该程序。
         *
         * 1.没有调用yield()方法时，虽然输出有时改变，但是通常消费者行先打印出来，然后事生产者。
         * 2.调用yield()方法时，两个线程依次打印，然后将执行机会交给对方，一直这样进行下去。
         */

        producer.start();
        consumer.start();

    }

}