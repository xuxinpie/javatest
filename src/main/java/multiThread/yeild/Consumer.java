/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package multiThread.yeild;

/**
 * Simple Consumer
 *
 * @author Xinux
 * @version $Id: Consumer.java, v 0.1 2016-06-22 2:25 PM Xinux Exp $$
 */
public class Consumer implements Runnable {
    public void run()
    {
        for (int i = 0; i < 5; i++)
        {
            System.out.println("I am Consumer : Consume Item " + i);
            Thread.yield();
        }
    }
}