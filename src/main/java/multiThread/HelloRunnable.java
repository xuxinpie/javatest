/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package multiThread;

/**
 * 定义并启动一个线程
 * 提供一个Runnable对象。Runnable对象仅包含一个run()方法，
 * 在这个方法中定义的代码将在会线程中执行。将Runnable对象传递给Thread类的构造函数即可
 *
 * @author Xinux
 * @version $Id: HelloRunnable.java, v 0.1 2015-07-11 11:54 AM Xinux Exp $$
 */
public class HelloRunnable implements Runnable {
    public void run() {
        System.out.println("Say Hello from a thread");
    }

    public static void main(String[] args) {
        new Thread(new HelloRunnable()).start();
    }
}