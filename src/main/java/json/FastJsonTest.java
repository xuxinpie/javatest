/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package json;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.TypeReference;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.util.*;

/**
 *
 * 测试fastJson
 *
 * @author Xinux
 * @version $Id: FastJsonTest.java, v 0.1 2015-12-28 10:01 AM Xinux Exp $$
 */
public class FastJsonTest {

    /**
     * 备注:
     * JSONObject，JSONArray是JSON的两个子类。
     * JSONObject相当于Map<String, Object>，
     * JSONArray相当于List<Object>。
     * @param args
     */
    public static void main(String[] args) {

        String jsonListStr = initListJsonStr();
        System.out.println("jsonStr: " + jsonListStr);

//        test1();
        test2(jsonListStr);

        String jsonStr = test7();
        test8(jsonStr);


    }

    /**
     * 初始化一个jsonString,一个list<Map> 包含两个map,每个map中有两个k-v
     * @return JSONString
     */
    private static String initListJsonStr() {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("key1", "One");
        map.put("key2", "Two");

        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("key1", "Three");
        map2.put("key2", "Four");

        list.add(map);
        list.add(map2);

        //list<Map> 转Json
        return JSON.toJSONString(list);
    }

    /**
     * 输出带null值的map
     */
    public static void test1() {
        Map<String,Object> map = new HashMap<String, Object>();
        String a = null;
        Integer b = 1;
        map.put("a", a);
        map.put("b", b);
//        默认fastJson的toJSONString()不会输出值为null的key-vaule
//        String jsonStr = JSON.toJSONString(map);
//        需要使用SerializerFeature.WriteMapNullValue 的标识
        String jsonStr = JSON.toJSONString(map, SerializerFeature.WriteMapNullValue);
        System.out.println(jsonStr);
    }

    /**
     * 通过在parser中加入对应的类型进行反序列化
     * @param listJson
     */
    public static void test2(String listJson) {
        List<Map> list = JSON.parseArray(listJson, Map.class);
        for (Map<String, Object> map : list) {
            System.out.println(map.get("key1"));
            System.out.println(map.get("key2"));
        }

    }

    /**
     * 将jsonString转为jsonArray
     * 遍历jsonArray
     * 同时将jsonArray反序列化
     */
    public static void test3() {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("key1", "One");
        map.put("key2", "Two");

        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("key1", "Three");
        map2.put("key2", "Four");

        list.add(map);
        list.add(map2);

        System.out.println(JSON.toJSONString(list));

        JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(list));

        for(int i=0; i<jsonArray.size(); i++){
            System.out.println(jsonArray.get(i));
            //泛型的反序列化,使用TypeReference传入类型信息
            Map<String, Object> map1 = JSON.parseObject(jsonArray.get(i).toString(), new TypeReference<Map<String, Object>>(){});
            System.out.println(map1.get("key1"));
            System.out.println(map1.get("key2"));
        }
    }

    /**
     * 输出指定的日期格式
     */
    public void test4() {
//        使用SerializerFeature特性格式化日期
//        String dateJson = JSON.toJSONString(new Date(), SerializerFeature.WriteDateUseDateFormat);

//        指定输出日期格式
        String dateJson = JSON.toJSONStringWithDateFormat(new Date(), "yyyy-MM-dd HH:mm:ss.SSS");
        System.out.println(dateJson);
    }

    /**
     * String listJson = JSON.toJSONString(list, SerializerFeature.PrettyFormat);
     * boolean PrettyFormat 为true会对JsonString进行格式化输出, false或者不带参数不进行格式化
     */
    public void test5() {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("key1", "One");
        map.put("key2", "Two");

        Map<String, Object> map2 = new HashMap<String, Object>();
        map2.put("key1", "Three");
        map2.put("key2", "Four");

        list.add(map);
        list.add(map2);
        String listJson = JSON.toJSONString(list, true);
        System.out.print(listJson);
    }

    /**
     * map转json
     */
    public static void test6() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("key1", "One");
        map.put("key2", "Two");

        String mapJson = JSON.toJSONString(map);
        System.out.println(mapJson);
    }

    /**
     * javaBean to JsonString
     * 将一个对象进行json序列化
     */
    public static String test7() {
        User user = new User();
        user.setUserName("李四");
        user.setAge(24);

        String userJson = JSON.toJSONString(user);
        System.out.println(userJson);
        return userJson;
    }

    /**
     * json反序列化
     * @param userJson
     */
    public static void test8(String userJson) {
        User user = JSON.parseObject(userJson, User.class);
        System.out.println(user.getUserName());
    }

    /**
     * 内部类
     */
    private static class User {

        String userName;

        int age;

        /**
         * Getter method for property userName.
         *
         * @return property value of userName
         */
        public String getUserName() {
            return userName;
        }

        /**
         * Setter method for property userName.
         *
         * @param userName value to be assigned to property userName
         */
        public void setUserName(String userName) {
            this.userName = userName;
        }

        /**
         * Getter method for property age.
         *
         * @return property value of age
         */
        public int getAge() {
            return age;
        }

        /**
         * Setter method for property age.
         *
         * @param age value to be assigned to property age
         */
        public void setAge(int age) {
            this.age = age;
        }
    }

}