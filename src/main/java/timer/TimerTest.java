package timer;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 连环炸弹  2s 后爆炸一次， 3s后又爆炸一次
 * @author XINUX
 *
 */

public class TimerTest {
	public static void main(String[] args) {
		Timer timer = new Timer();
		timer.schedule(new MyTimerTask2(), 2000);
		while(true) {
			System.out.println(new Date().getSeconds());
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static class MyTimerTask1 extends TimerTask {

		@Override
		public void run() {
			System.out.println("爆炸！！！");
			new Timer().schedule(new MyTimerTask2(), 2000);
		}
	}
	
	private static class MyTimerTask2 extends TimerTask {

		@Override
		public void run() {
			System.out.println("Boom!!!");
			new Timer().schedule(new MyTimerTask1(), 3000);
		}
		
	}
}
