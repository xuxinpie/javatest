/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package exception;

/**
 * @author hanlin.xx
 * @version $Id: MyException.java, v 0.1 2015-07-10 14:33 hanlin.xx Exp $$
 */
public class MyException extends Exception {

	private String message;

	public MyException() {
		super();
	}

	public MyException(String message) {
		super(message);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}

