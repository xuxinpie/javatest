/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package exception;

/**
 * @author hanlin.xx
 * @version $Id: ExceptionTest.java, v 0.1 2015-07-10 14:35 hanlin.xx Exp $$
 */
public class ExceptionTest {

	public void method(String str) throws  MyException {
		if (null == str) {
			throw new MyException("传入的参数不能为NULL");
		} else {
			System.out.println(str);
		}
	}

	public static void main(String[] args) {
		try {
			ExceptionTest test = new ExceptionTest();
			test.method(null);
		} catch (MyException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		} finally {
			System.out.println("程序处理完毕");
		}
	}

}

