/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package exception;

/**
 * 自定义运行时异常
 *
 * @author hanlin.xx
 * @version $Id: MyRuntimeException.java, v 0.1 2015-07-30 15:10 hanlin.xx Exp $$
 */
public class MyRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MyRuntimeException() {
		super();
	}

	public MyRuntimeException(String msg) {
		super(msg);
	}

	public MyRuntimeException(String msg, Throwable t) {
		super(msg, t);
	}

}

