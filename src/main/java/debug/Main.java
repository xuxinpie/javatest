/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package debug;

/**
 * @author Xinux
 * @version $Id: Main.java, v 0.1 2016-10-10 5:29 PM Xinux Exp $$
 */
public class Main {

    public static void main(String[] args) {
        f1();
        f2();
    }


    public static void f1() {
        System.out.println("method f1 be called");
    }

    public static void f2() {
        for (int i = 0; i < 256; i++) {
            if (i == 128) {
                System.out.println("method2 f2 be called");
                f1();
            }
        }
    }

}