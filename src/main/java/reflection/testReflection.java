/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package reflection;

import java.lang.reflect.Field;

/**
 *
 * String 对象是不可改变的对象，但可以利用反射方法修改String对象
 * @author Xinux
 * @version $Id: testReflection.java, v 0.1 2015-06-10 11:02 AM Xinux Exp $$
 */
public class testReflection {

    public static void main(String[] args) throws Exception {
        test();
    }

    public static void test() throws Exception {

        String s = "Hello World";

        System.out.println("s = " + s);

        //获取String的value对象
        Field valueFieldofString = String.class.getDeclaredField("value");

        //改变value的访问属性
        valueFieldofString.setAccessible(true);

        //获得s对象上value属性的值
        char[] value = (char[]) valueFieldofString.get(s);

        //改变value所引用的数组的第5个字符
        value[5] = '_';

        System.out.println("s = " + s);

    }

}