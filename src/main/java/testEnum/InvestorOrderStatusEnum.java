/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package testEnum;


import org.apache.commons.lang.StringUtils;

/**
 * 投资人还款单状态枚举
 * 
 * @author dizhou.fys
 * @version $Id: InvestorOrderStatusEnum.class, v 0.1 2015年04月07日 下午15:38 dizhou.fys Exp $
 */
public enum InvestorOrderStatusEnum {

    /** 初始化状态 */
    INIT("INIT", "初始化"),
    
    /** 理赔待申请 */
    WAIT_INSURANCE_CLAIM("WAIT_INSURANCE_CLAIM","理赔待申请"),

    /** 理赔已申请 */
    INSURANCE_CLAIMED("INSURANCE_CLAIMED","理赔已申请"),

    /** 理赔已确认 */
    INSURANCE_CLAIM_CONFIRMED("INSURANCE_CLAIM_CONFIRMED","理赔已确认"),
    
    /** 成功状态 */
    SUCCESS("SUCCESS", "成功");

    /** code值 */
    private String code;

    /** 描述 */
    private String desc;

    /**
     * Private Constructor
     * 
     * @param code code值
     * @param desc 描述
     */
    private InvestorOrderStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 根据code 值获取枚举
     * 
     * @param code 枚举Code值
     * @return 枚举
     */
    public static InvestorOrderStatusEnum getEnumByCode(String code) {
        for (InvestorOrderStatusEnum investorOrderStatusEnum : InvestorOrderStatusEnum.values()) {
            if (StringUtils.equals(investorOrderStatusEnum.getCode(), code)) {
                return investorOrderStatusEnum;
            }
        }
        return null;
    }

    /**
     * Getter method for property <tt>code</tt>.
     * 
     * @return property value of code
     */
    public String getCode() {
        return code;
    }

    /**
     * Setter method for property <tt>code</tt>.
     * 
     * @param code value to be assigned to property code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Getter method for property <tt>desc</tt>.
     * 
     * @return property value of desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * Setter method for property <tt>desc</tt>.
     * 
     * @param desc value to be assigned to property desc
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

}
