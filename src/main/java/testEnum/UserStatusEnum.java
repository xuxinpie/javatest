/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package testEnum;

import org.apache.commons.lang.StringUtils;

/**
 * @author hanlin.xx
 * @version $Id: UserStatusEnum.java, v 0.1 2015-07-21 19:02 hanlin.xx Exp $$
 */
public enum UserStatusEnum {

	/**
	 * 空闲
	 */
	FREE(0, "空闲"),

	/**
	 * 忙碌
	 */
	BUSY(1, "忙碌"),

	/**
	 * 等待
	 */
	WAIT(2, "等待"),

	/**
	 * 离线
	 */
	OFFLINE(3, "离线");

	/**枚举Code值*/
	private int code;

	/**枚举值描述*/
	private String desc;

	/**
	 * Private consutructor
	 * @param code code值
	 * @param desc 描述
	 */
	UserStatusEnum(int code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	/**
	 * 根据code值获取枚举
	 *
	 * @param code 枚举Code值
	 * @return 枚举值
	 */
	public static UserStatusEnum getEnumByCode(int code) {
		for (UserStatusEnum userStatusEnum : UserStatusEnum.values()) {
			if (code == userStatusEnum.getCode()) {
				return userStatusEnum;
			}
		}

		return null;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}

