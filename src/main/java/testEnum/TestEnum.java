package testEnum;


public class TestEnum {

    public static void main(String[] args) {
        for (ConsumeRecordStatusEnum statusEnum : ConsumeRecordStatusEnum.values()) {
            System.out.println(statusEnum);
        }

        new TestEnum().judge(ConsumeRecordStatusEnum.ALL);
    }

    public void judge(ConsumeRecordStatusEnum statusEnum) {
        switch (statusEnum.value) {
            case 0:
                System.out.println("所有状态");
                break;
            case 1:
                System.out.println("进行中");
                break;
            case 2:
                System.out.println("等待付款");
                break;
            case 3:
                System.out.println("等待发货");
                break;
        }
    }
}
