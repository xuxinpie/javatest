package testEnum;

public enum ConsumeRecordStatusEnum {
    /**
     * 所有状态
     */
    ALL(0),

    /**
     * 进行中
     */
    PROCESS(1),

    /**
     * 等待付款
     */
    WAITPAYMENT(2),

    /**
     * 等待发货
     */
    WAITDELIVERY(3);

    public final int value;

    private ConsumeRecordStatusEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
