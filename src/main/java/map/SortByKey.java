/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package map;

import java.util.*;

/**
 * 对TreeMap按照key值进行排序
 *
 * @author Xinux
 * @version $Id: SortByKey.java, v 0.1 2016-03-23 3:45 PM Xinux Exp $$
 */
public class SortByKey {

    public static void main(String[] args) {
        // 实现降序排序通过实现comparator来实现
//        Map<String, String> map = new TreeMap<String, String>(new Comparator<String>() {
//            public int compare(String obj1, String obj2) {
//                // 降序排序
//                return obj2.compareTo(obj1);
//            }
//        });

        // Tree Map中会按key值做升序排序
        Map<String, String> map = new TreeMap<String, String>();
        map.put("level1", "ccccc");
        map.put("level2", "aaaaa");
        map.put("default", "bbbbb");
        map.put("level3", "ddddd");

        Set<String> keySet = map.keySet();
        Iterator<String> iter = keySet.iterator();
        while (iter.hasNext()) {
            String key = iter.next();
            System.out.println(key + ":" + map.get(key));
        }
    }

}