/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package map;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Xinux
 * @version $Id: MapOverWrite.java, v 0.1 2016-05-24 4:08 PM Xinux Exp $$
 */
public class MapOverWrite {

    public static void main(String[] args) {
        Map<String, String> recordProperty = new HashMap<String, String>();
        recordProperty.put("IS_FOR_PAY", "Y");
        System.out.println(recordProperty);

        recordProperty.put("IS_FOR_PAY", "N");
        System.out.println(recordProperty);
    }

}