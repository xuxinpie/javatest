/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package map;

import java.util.*;

/**
 *
 * Map提供了一些常用方法，如keySet()、entrySet()等方法，
 * keySet()方法返回值是Map中key值的集合；
 * entrySet()的返回值也是返回一个Set集合，此集合的类型为Map.Entry。
 * Map.Entry是Map声明的一个内部接口，此接口为泛型，定义为Entry<K,V>。它表示Map中的一个实体（一个key-value对）。接口中有getKey(),getValue方法。
 *
 * @author Xinux
 * @version $Id: collectionTest.java, v 0.1 2015-12-10 11:44 AM Xinux Exp $$
 */
public class collectionTest {
    public static void main(String[] args) {
        Map<Integer, Integer> mm = new HashMap<Integer, Integer>();
        for (int i = 0; i < 10; i++) {
            mm.put(i, i);
        }
        for (Map.Entry<Integer, Integer> e : mm.entrySet()) {
            System.out.println("key:" + e.getKey());
            System.out.println("value:" + e.getValue());
        }
        for (Iterator<Map.Entry<Integer, Integer>> i = mm.entrySet().iterator(); i.hasNext();) {
            Map.Entry<Integer, Integer> e = i.next();
            System.out.println("key:" + e.getKey());
            System.out.println("value:" + e.getValue());
        }

        Map map = new HashMap();

        Iterator iterator = map.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry entry = (Map.Entry) iterator.next();
            Object key = entry.getKey();
        }

        Set keySet = map.keySet();

        Iterator iterator2 = keySet.iterator();

        while (iterator2.hasNext()) {
            Object key = iterator2.next();
            Object value = map.get(key);
        }

        //另外，还有一种遍历方法是，单纯的遍历value值，Map有一个values方法，返回的是value的Collection集合。通过遍历collection也可以遍历value,如
        Collection c = map.values();

        Iterator iterator3 = c.iterator();

        while (iterator3.hasNext()) {
            Object value = iterator3.next();
        }

    }
}
