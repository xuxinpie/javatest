/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package map;

import java.util.*;

/**
 * 将HashMap进行排序
 * 借助于Collections的sort(List<T> list, Comparator<? super T> c)方法
 *
 * @author Xinux
 * @version $Id: HashMapSortTest.java, v 0.1 2016-03-23 4:13 PM Xinux Exp $$
 */
public class HashMapSortTest {

    public static void main(String[] args) {

        // HashMap的值是没有顺序的，他是按照key的HashCode来实现的
        Map<String, String> map = new HashMap<String, String>();
        map.put("c", "ccccc");
        map.put("a", "aaaaa");
        map.put("b", "bbbbb");
        map.put("d", "ddddd");

        List<Map.Entry<String,String>> list = new ArrayList<Map.Entry<String,String>>(map.entrySet());
        Collections.sort(list,new Comparator<Map.Entry<String,String>>() {
            //升序排序
            public int compare(Map.Entry<String, String> o1,
                               Map.Entry<String, String> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }

        });

        for(Map.Entry<String,String> mapping:list){
            System.out.println(mapping.getKey()+":"+mapping.getValue());
        }
    }
}
