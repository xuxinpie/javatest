/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package algorithm;

import java.util.HashMap;

/**
 * Design and implement a data structure for Least Recently Used (LRU) cache.
 * It should support the following operations: get and set. get(key) - Get the value (will always be positive) of the key if the key exists in the cache,
 * otherwise return -1. set(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity,
 * it should invalidate the least recently used item before inserting a new item.
 *
 * 利用hashMap存储key,构造一个双向链表存储数据
 *
 * @author Xinux
 * @version $Id: LRUCache.java, v 0.1 2016-06-24 2:25 PM Xinux Exp $$
 */
public class LRUCache {
    // 容量
    private int capacity;

    // 存储每个Node的key,用于查找节点
    private HashMap<Integer, Node> hs = new HashMap<Integer, Node>();

    // 头节点
    private Node head = new Node(-1, -1);

    // 尾节点
    private Node tail = new Node(-1, -1);

    /**
     * constructor
     * @param capacity
     */
    public LRUCache(int capacity) {
        this.capacity = capacity;
        tail.prev = head;
        head.next = tail;
    }

    public int get(int key) {
        if(!hs.containsKey(key)) {
            return -1;
        }

        // remove current
        Node current = hs.get(key);
        current.prev.next = current.next;
        current.next.prev = current.prev;

        // move current to tail
        move_to_tail(current);

        return hs.get(key).value;
    }

    public void set(int key, int value) {
        // 缓存命中
        if( get(key) != -1) {
            hs.get(key).value = value;
            return;
        }

        // 未命中,需要插入新的节点
        // 已达设计的缓存容量上限,删除头结点
        if (hs.size() == capacity) {
            hs.remove(head.next.key);
            head.next = head.next.next;
            head.next.prev = head;
        }

        // 新建节点,并插入到链表尾部
        Node insert = new Node(key, value);
        hs.put(key, insert);
        move_to_tail(insert);
    }

    private void move_to_tail(Node current) {
        current.prev = tail.prev;
        tail.prev = current;
        current.prev.next = current;
        current.next = tail;
    }

    // 构造一个节点
    private class Node{
        Node prev;
        Node next;
        int key;
        int value;

        public Node(int key, int value) {
            this.key = key;
            this.value = value;
            this.prev = null;
            this.next = null;
        }
    }
}