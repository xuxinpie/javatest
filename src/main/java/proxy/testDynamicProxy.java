/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package proxy;

import java.lang.reflect.Proxy;

/**
 * ���Զ�̬����
 * @author hanlin.xx
 * @version $Id: testDynamicProxy.java, v 0.1 2015-06-21 22:24 hanlin.xx Exp $$
 */
public class testDynamicProxy {
    public static void main(String[] args) {
        Calculator calculator = new CalculatorImpl();
        ProxyLogHandler logHandler = new ProxyLogHandler(calculator);

        Calculator proxy = (Calculator) Proxy.newProxyInstance(calculator.getClass()
            .getClassLoader(), calculator.getClass().getInterfaces(), logHandler);
		Integer param1 = 1;
		Integer param2 = 1;
        int result = proxy.add(param1, param2);
        System.out.println("The result is: " + result);
    }

}
