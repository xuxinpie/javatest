package proxy.dynamicProxy;

/**
 * 客户端
 */
public class Client {
    public static void main(String[] args) {
        LogHandler logHandler = new LogHandler();
        UserManager userManager = (UserManager) logHandler.newProxyInstance(new UserManagerImpl());
        userManager.addUser("001", "xinux");
    }
}
