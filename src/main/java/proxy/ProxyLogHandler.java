/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package proxy;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author hanlin.xx
 * @version $Id: ProxyLogHandler.java, v 0.1 2015-06-21 22:29 hanlin.xx Exp $$
 */
public class ProxyLogHandler implements InvocationHandler {

	Object object;

	ProxyLogHandler(Object object) {
		this.object = object;
	}

	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		this.doBefore();
		Integer result = (Integer)method.invoke(object, args);
		this.doAfter();
		return object;
	}

	public void doBefore() {
		System.out.println("do this before");
	}

	public void doAfter() {
		System.out.println("do this after");
	}
}

