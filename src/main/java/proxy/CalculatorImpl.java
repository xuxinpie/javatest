/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package proxy;

/**
 * @author hanlin.xx
 * @version $Id: CalculatorImpl.java, v 0.1 2015-06-21 22:24 hanlin.xx Exp $$
 */
public class CalculatorImpl implements  Calculator {
	public Integer add(int num1, int num2) {
		return num1 + num2;
	}
}

