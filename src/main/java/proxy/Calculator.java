/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package proxy;

/**
 * @author hanlin.xx
 * @version $Id: Calculator.java, v 0.1 2015-06-21 22:23 hanlin.xx Exp $$
 */
public interface Calculator {
	Integer add(int num1, int num2);
}

