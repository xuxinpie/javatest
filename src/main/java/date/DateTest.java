/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author hanlin.xx
 * @version $Id: DateTest.java, v 0.1 2015-07-21 16:44 hanlin.xx Exp $$
 */
public class DateTest {

	/** 完整时间 yyyy-MM-dd HH:mm:ss */
	public static final String  SIMPLE   = "yyyy-MM-dd HH:mm:ss";

	/** 年月日 yyyy-MM-dd */
	private static final String DTSIMPLE = "yyyy-MM-dd";

	/** 年月日(无下划线) yyyyMMdd */
	public static final String  DTSHORT  = "yyyyMMdd";

	/**增加的天数*/
	private static final int    ADD_DAYS = 1;

	public static void main(String[] args) {
		Date currentDate = new Date(System.currentTimeMillis());
		Date resultDate_add = addDays(currentDate, ADD_DAYS);
		Date resultDate_decrease = delDays(currentDate, ADD_DAYS);
		System.out.println("CurrentTime add " + ADD_DAYS + " days = " + resultDate_add);
		System.out.println("CurrentTime decrese " + ADD_DAYS + " days = " + resultDate_decrease);
		boolean result = isBetweenNow(resultDate_decrease, resultDate_add);
		System.out.println((result ? "CurrentTime is between startDate and endDate"
				: "CurrentTime is not between startDate and endDate"));

		try {
			Date resultDate = string2Date(dateFormat(currentDate, DTSIMPLE));
			System.out.println(resultDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	public static Date addDays(final Date date, final int days) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		final int hour = 24;
		calendar.add(Calendar.HOUR, hour * days);
		return calendar.getTime();
	}

	public static Date delDays(final Date date, final int days) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		final int hour = 24;
		calendar.add(Calendar.HOUR, -(hour * days));
		return calendar.getTime();
	}

	public static String dateFormat(final Date date, String dateFormat) {
		if (null == date) {
			return "";
		}
		return getFormat(dateFormat).format(date);
	}

	public static final Date string2Date(String stringDate) throws ParseException {
		if (null == stringDate) {
			return null;
		}
		return getFormat(DTSIMPLE).parse(stringDate);
	}

	public static final DateFormat getFormat(String format) {
		return new SimpleDateFormat(format);
	}

	/**
	 * 判断是否在时间范围之内
	 */
	public static boolean isBetweenNow(Date startDate, Date endDate) {
		try {
			if (null == startDate || null == endDate)
				return false;
			DateFormat df1 = new SimpleDateFormat(DTSHORT);
			Date nowdate = df1.parse(df1.format(new Date()));
			return startDate.compareTo(nowdate) <= 0 && endDate.compareTo(nowdate) >= 0;
		} catch (Exception e) {
			throw new RuntimeException("isAfterNow:日期格式转换异常");
		}
	}

}

