/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package date;

/**
 * 交易日/工作日
 *
 * @author Xinux
 * @version $Id: DayType.java, v 0.1 2016-06-21 3:41 PM Xinux Exp $$
 */
public enum DayType {
    Trading("T", "交易日"), Natural("D", "自然日");

    /**
     * 内部编码, 数据库中存储的交易日或者自然日的字符串
     */
    private String code;
    /**
     * 描述信息
     */
    private String description;

    /**
     * 内部构造一个DayType类型
     * @param code          编码
     * @param description   描述
     */
    DayType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    /**
     * 从日类型编码转换为枚举对象
     * @param code      日类型编码
     * @return  日类型对象
     */
    public static DayType fromCode(String code) {
        for (DayType dayType : DayType.values()) {
            if (dayType.getCode().equals(code)) {
                return dayType;
            }
        }
        throw new IllegalArgumentException("Invalid DayType code : " + code);
    }

    /**
     * 从日类型编码转换为枚举对象
     *
     * @param code
     * @param defaultType
     * @return
     */
    public static DayType fromCode(String code, DayType defaultType) {
        for (DayType dayType : DayType.values()) {
            if (dayType.getCode().equals(code)) {
                return dayType;
            }
        }
        return defaultType;
    }

    /**
     * Getter method for property <tt>code</tt>.
     *
     * @return property value of code
     */
    public String getCode() {
        return code;
    }

    /**
     * Getter method for property <tt>description</tt>.
     *
     * @return property value of description
     */
    public String getDescription() {
        return description;
    }

}