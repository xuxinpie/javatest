/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package date;

import org.apache.commons.lang.StringUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * 用于保存T/D的时间点的类
 *
 * @author Xinux
 * @version $Id: TradeTimeOffset.java, v 0.1 2016-06-21 3:39 PM Xinux Exp $$
 */
public class TradeTimeOffset {
    /**
     * 交易日/自然日
     */
    private DayType             dayType;

    /**
     * 日期的偏移量
     */
    private int                 dayOffset;

    /**
     * 时间点
     */
    private final TimeSpan      timeSpan;

    /**
     * T日的表示
     */
    private static final String T_DAY = "T";

    /**
     * D日的表示
     */
    private static final String D_DAY = "D";

    /**
     * 构造一个交易日期
     * @param tradeTime 交易日期的字符串, 格式为: T+n HH:mm:ss
     */
    protected TradeTimeOffset(String tradeTime) {
        if (StringUtils.isBlank(tradeTime)) {
            throw new NullPointerException("tradeTime参数不能为空");
        }

        //trim first
        tradeTime = tradeTime.trim();

        String[] segs = split(tradeTime, " ");

        //segs[0] 为 T+n
        //segs[1] 为 hh:mm:ss
        if (segs.length != 2) {
            throw new IllegalArgumentException("tradeTime的格式不正确, 正确的格式为:T+n HH:mm:ss");
        }

        // 解析T+n 部分
        if (segs[0].startsWith(T_DAY)) {
            this.dayType = DayType.Trading;
        } else if (segs[0].startsWith(D_DAY)) {
            this.dayType = DayType.Natural;
        } else {
            throw new IllegalArgumentException("tradeTime只支持T日或者D日");
        }

        this.dayOffset = 0;

        DecimalFormat df = new DecimalFormat("+#;-#");

        // 解析 +/-n的部分
        String dayOffsetStr = StringUtils.trim(segs[0].substring(1));
        if (StringUtils.isNotEmpty(dayOffsetStr)) {
            try {
                this.dayOffset = df.parse(dayOffsetStr).intValue();
            } catch (ParseException e) {
                throw new IllegalArgumentException("tradeTime的格式不正确, 正确的格式为:T+n HH:mm:ss");
            }
        }

        String hh_mm_ss = segs[1];

        int hour = 0;
        int minute = 0;
        int second = 0;

        if (StringUtils.isEmpty(hh_mm_ss)) {
            throw new java.lang.IllegalArgumentException("tradeTime的格式不正确, 正确的格式为:T+n HH:mm:ss");
        }

        segs = split(hh_mm_ss, ":");

        if (segs.length > 0) {
            hour = Integer.parseInt(segs[0]);
        }
        if (segs.length > 1) {
            minute = Integer.parseInt(segs[1]);
        }
        if (segs.length > 2) {
            second = Integer.parseInt(segs[2]);
        }

        if (hour < 0 || minute < 0 || second < 0 || hour > 23 || minute > 59 || second > 59) {
            throw new IllegalArgumentException("tradeTime的格式不正确, 正确的格式为:T+n HH:mm:ss");
        }

        this.timeSpan = new TimeSpan(hour, minute, second);
    }

    /**
     * 使用指定的字符分隔字符串, 去掉分隔结果为空的元素
     * @param str 要分隔的字符串
     * @return  返回分隔后的元素
     */
    private String[] split(String str, String regex) {
        String[] segs = str.split(regex);
        List<String> list = new ArrayList<String>();

        for (String seg : segs) {
            if (!StringUtils.isBlank(seg)) {
                list.add(seg);
            }
        }

        return list.toArray(new String[list.size()]);
    }

    /**
     * 解析一个交易时间
     * @param tradeTime 交易日期的字符串, 格式为: T+n HH:mm:ss
     * @return 返回一个TradeTime对象
     */
    public static TradeTimeOffset parse(String tradeTime) {
        return new TradeTimeOffset(tradeTime);
    }

    /**
     * Getter method for property <tt>dayType</tt>.
     *
     * @return property value of dayType
     */
    public DayType getDayType() {
        return dayType;
    }

    /**
     * Getter method for property <tt>dayOffset</tt>.
     *
     * @return property value of dayOffset
     */
    public int getDayOffset() {
        return dayOffset;
    }

    /**
     * Getter method for property <tt>timeSpan</tt>.
     *
     * @return property value of timeSpan
     */
    public TimeSpan getTimeSpan() {
        return timeSpan;
    }

}