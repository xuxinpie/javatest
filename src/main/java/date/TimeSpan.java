/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package date;

import org.apache.commons.lang.StringUtils;

import java.util.Calendar;

/**
 *
 * 存储时间间隔信息的类型, 有效的信息为hour, minute, seconds, milliseconds
 * 
 * @author Xinux
 * @version $Id: TimeSpan.java, v 0.1 2016-06-21 3:35 PM Xinux Exp $$
 */
public class TimeSpan {
    /**
     * hours
     */
    private int hour;
    /**
     * minutes
     */
    private int minute;
    /**
     * seconds
     */
    private int second;

    /**
     * 构造一个TimeSpan对象
     * @param hour      小时
     * @param minute    分钟
     * @param second    秒
     */
    public TimeSpan(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    /**
     * 通过milliseconds构造TimeSpan对象
     * @param millis        milliseconds
     */
    public TimeSpan(long millis) {
        long seconds = millis / 1000;
        this.hour = (int) (seconds / 3600);
        this.minute = (int) ((seconds % 3600) / 60);
        this.second = (int) (seconds % 60);
    }

    /**
     * 根据hh:mm:ss格式的数据实例化
     * @param hh_mm_ss    字符串形式的时间表示: HH:mm:ss
     */
    public TimeSpan(String hh_mm_ss) {
        int hour = 0;
        int minute = 0;
        int second = 0;

        if (StringUtils.isEmpty(hh_mm_ss))
            throw new java.lang.IllegalArgumentException();

        String[] segs = hh_mm_ss.split(":");

        if (segs.length > 0) {
            hour = Integer.parseInt(segs[0]);
        }
        if (segs.length > 1) {
            minute = Integer.parseInt(segs[1]);
        }
        if (segs.length > 2) {
            second = Integer.parseInt(segs[2]);
        }

        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    /**
     * 返回对应TimeSpan的Milliseconds的数值
     */
    public long getTimeMillis() {
        return (hour * 3600 + minute * 60 + second) * 1000;
    }

    /**
     * 返回对应TimeSpan的Seconds的数值
     */
    public int getTimeSeconds() {
        return (hour * 3600 + minute * 60 + second);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        TimeSpan timeSpan = (TimeSpan) o;

        if (hour != timeSpan.hour)
            return false;
        if (minute != timeSpan.minute)
            return false;
        //noinspection RedundantIfStatement
        if (second != timeSpan.second)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = hour;
        result = 31 * result + minute;
        result = 31 * result + second;
        return result;
    }

    /**
     * 返回HH:mm:ss的字符串数据
     */
    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d", hour, minute, second);
    }

    /**
     * 返回hhmmss的字符串数据
     */
    public String toShortString() {
        return String.format("%02d%02d%02d", hour, minute, second);
    }

    /**
     * 在当前的时间上增加n小时
     * @param n         增加的小时数
     */
    public void addHours(int n) {
        this.hour += n;
    }

    /**
     * 在当前的时间上增加n分钟
     * @param n         增加的分钟数
     */
    public void addMinutes(int n) {
        int minutes = this.minute + n;

        // 分钟数为正的计算方法
        if (minutes >= 0) {
            this.hour += minutes / 60; //   超过60分, 转换为小时
            this.minute = minutes % 60; //   剩余的分钟数
        } else {
            this.hour -= (Math.abs(minutes) / 60 + 1); // 计算需要借位的小时数
            this.minute = (minutes % 60 + 60); // 借位之后剩余的分钟数
        }
    }

    /**
     * 返回小时数
     */
    public int getHour() {
        return hour;
    }

    /**
     * 返回分钟数
     */
    public int getMinute() {
        return minute;
    }

    /**
     * 返回秒数
     */
    public int getSecond() {
        return second;
    }

    /**
     * 获取指定字段的数值
     * @param calendarField     Calendar中定义的HOUR_OF_DAY, MINUTE, SECONDS
     * @return  该字段的数据
     */
    public int get(int calendarField) {
        int value;
        switch (calendarField) {
            case Calendar.HOUR_OF_DAY:
                value = getHour();
                break;
            case Calendar.MINUTE:
                value = getMinute();
                break;
            case Calendar.SECOND:
                value = getSecond();
                break;
            default:
                throw new IllegalArgumentException("Invalid field index : " + calendarField);
        }

        return value;
    }

    /**
     * 将字符串解析为TimeSpan类型
     * @param s       时间格式的字符串, 格式为: HH:mm:ss
     * @return 返回一个TimeSpan对象
     */
    public static TimeSpan valueOf(String s) {
        return new TimeSpan(s);
    }
}
