/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package date;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author hanlin.xx
 * @version $Id: DateFormaterTest.java, v 0.1 2015-07-21 16:47 hanlin.xx Exp $$
 */
public class DateFormaterTest {
	public static void main(String[] args) {
		Date time = new Date();
		String createDate1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(time);
		String createDate2 = new SimpleDateFormat("yyyy-MM-dd ").format(time);
		System.out.println(createDate1 + " " + createDate2);

	}
}

