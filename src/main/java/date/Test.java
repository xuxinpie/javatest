/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package date;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import util.DateUtil;

/**
 * @author Xinux
 * @version $Id: Test.java, v 0.1 2016-06-17 11:24 AM Xinux Exp $$
 */
public class Test {
    public static void main(String[] args) throws ParseException {
        String bizDate = "20160617";

        System.out.println(DateUtil.shortstring2Date(bizDate));

        Date now = new Date();

        System.out.println(DateUtils.truncate(now, Calendar.DATE));


    }
}