package date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 将java中的Date类型转为Sql的Date类型
 * Created by Xinux on 10/19/14.
 */
public class JavaDatetoSqlDate {
	public static void main(String[] args) {
		Date javaDate = new Date();
		java.sql.Date sqlDate = new java.sql.Date(javaDate.getTime());
		System.out.println(sqlDate);
		System.out.println(javaDate);

		//将string类型转为date类型
		String dateString = "2014-10-19 11:11:11";
		//常见标准的写法"yyyy-MM-dd HH:mm:ss",注意大小写，
		//时间是24小时制，24小时制转换成12小时制只需将HH改成hh,不需要另外的函数。
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date newDate = format.parse(dateString);
			System.out.println(newDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

}
