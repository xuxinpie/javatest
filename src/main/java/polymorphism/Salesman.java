/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package polymorphism;

/**
 * @author hanlin.xx
 * @version $Id: Salesman.java, v 0.1 2015-09-08 9:54 hanlin.xx Exp $$
 */
public class Salesman extends EmployeeType {
	@Override
	int getTypeCode() {
		return Employee.SALESMAN;
	}

	@Override
	public int payAmount(Employee employee) {
		return employee.getMonthlySalary() + employee.getCommission();
	}
}

