/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package polymorphism;

/**
 * @author hanlin.xx
 * @version $Id: Engineer.java, v 0.1 2015-09-08 9:53 hanlin.xx Exp $$
 */
public class Engineer extends EmployeeType {
	@Override
	int getTypeCode() {
		return Employee.ENGINEER;
	}

	@Override
	public int payAmount(Employee employee) {
		return employee.getMonthlySalary();
	}
}

