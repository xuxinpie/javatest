/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package polymorphism;

/**
 * 抽象类
 *
 * @author hanlin.xx
 * @version $Id: EmployeeType.java, v 0.1 2015-09-08 9:50 hanlin.xx Exp $$
 */
public abstract class EmployeeType {

	abstract int getTypeCode();

	public abstract int payAmount(Employee employee);
}

