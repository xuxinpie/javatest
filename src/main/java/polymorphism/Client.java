/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package polymorphism;

/**
 * 多态测试类 -- 本例使用策略模式
 *
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-09-08 10:02 hanlin.xx Exp $$
 */
public class Client {

	public static void main(String[] args) {
		Engineer engineer = new Engineer();

		Employee employee = new Employee(engineer);

		System.out.println(employee.payAmount());
	}




}

