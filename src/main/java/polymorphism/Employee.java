/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package polymorphism;

/**
 * @author hanlin.xx
 * @version $Id: Employee.java, v 0.1 2015-09-08 9:55 hanlin.xx Exp $$
 */
public class Employee {

	static final int ENGINEER = 1;

	static final int SALESMAN = 2;

	static final int MANANGER = 3;

    private EmployeeType _type;

    int          monthlySalary = 10000;

    int          commission    = 3000;

    int          bonus         = 20000;

	public Employee(EmployeeType _type) {
		this._type = _type;
	}

    private int getType() {
        return _type.getTypeCode();
    }

    int payAmount() {
        return _type.payAmount(this);
    }

    /**
     * Getter method for property monthlySalary.
     *
     * @return property value of monthlySalary
     */
    public int getMonthlySalary() {
        return monthlySalary;
    }

    /**
     * Setter method for property monthlySalary.
     *
     * @param monthlySalary value to be assigned to property monthlySalary
     */
    public void setMonthlySalary(int monthlySalary) {
        this.monthlySalary = monthlySalary;
    }

    /**
     * Getter method for property commission.
     *
     * @return property value of commission
     */
    public int getCommission() {
        return commission;
    }

    /**
     * Setter method for property commission.
     *
     * @param commission value to be assigned to property commission
     */
    public void setCommission(int commission) {
        this.commission = commission;
    }

    /**
     * Getter method for property bonus.
     *
     * @return property value of bonus
     */
    public int getBonus() {
        return bonus;
    }

    /**
     * Setter method for property bonus.
     *
     * @param bonus value to be assigned to property bonus
     */
    public void setBonus(int bonus) {
        this.bonus = bonus;
    }

    /**
     * Setter method for property _type.
     *
     * @param _type value to be assigned to property _type
     */
    public void set_type(EmployeeType _type) {
        this._type = _type;
    }
}
