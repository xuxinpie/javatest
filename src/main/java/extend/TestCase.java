/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package extend;

/**
 *
 * @author Xinux
 * @version $Id: TestCase.java, v 0.1 2016-05-23 5:46 PM Xinux Exp $$
 */
public class TestCase {

    public static void main(String[] args) {
        Children children = new Children();
        children.doAction();
    }
}