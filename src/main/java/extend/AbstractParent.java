/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package extend;

/**
 * @author Xinux
 * @version $Id: AbstractParent.java, v 0.1 2016-05-23 5:44 PM Xinux Exp $$
 */
public abstract class AbstractParent {

    public void doAction() {
        sayHello();
    }

    /**
     * 抽象方法
     * 回调函数-调用子类的实现
     */
    protected void sayHello() {

    }
}