/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package extend;

/**
 * @author Xinux
 * @version $Id: Children.java, v 0.1 2016-05-23 5:45 PM Xinux Exp $$
 */
public class Children extends AbstractParent {

    @Override
    protected void sayHello() {
        System.out.println("Hello");
    }

}