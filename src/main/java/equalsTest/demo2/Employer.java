package equalsTest.demo2;

public class Employer extends Person {
	
	private int id;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public Employer(String name, int id) {
		super(name);
		this.id = id;
	}

	public boolean equals(Object object) {
		if (object instanceof Employer) {
			Employer employer = (Employer) object;
			return super.equals(object) && employer.getId() == id;
		} else {
			return false;
		}
	}
}
