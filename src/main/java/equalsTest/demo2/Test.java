package equalsTest.demo2;

public class Test {
	
	public static void main(String[] args) {
		Employer employer1 = new Employer("yuhui", 23);
		Employer employer2 = new Employer("yuhui", 24);
		
		Person person = new Person("yuhui");
		
		System.out.println(person.equals(employer1));
		System.out.println(person.equals(employer2));
		System.out.println(employer1.equals(employer2));
	}
	
	
	
}
