/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package equalsTest;

/**
 *
 * == 和equals的比较
 *
 * 1. 操作符”==”用来比较两个操作元是否相等，这两个操作元既可以是基本类型，也可以是引用类型
 *
 * 2. 当操作符“==”两边都是引用类型变量时，这两个引用变量必须都引用同一个对象，结果才为true,否则为false
 *
 * 3. 当“==”用于比较引用类型变量时，“==”两边的变量被显式声明的类型必须是同种类型或有继承关系
 *
 * 4. Object类的equals()方法比较规则为：当参数obj引用的对象与当前对象为同一个对象时，
 * 	  就返回true,否则返回false.两者的区别是“==”比较的是变量的值（也即为内存地址），而equals()比较的是变量的内容。
 * 5. 只要两个对象都是同一个类的对象，并且它们变量属性相同，则结果为true,否则为false
 *
 * @author Xinux
 * @version $Id: TestInteger.java, v 0.1 2015-06-15 12:11 AM Xinux Exp $$
 */
public class TestInteger {

    private String name;

    TestInteger(String name) {
        this.name = name;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof TestInteger)) {
            return false;
        }
        final TestInteger other = (TestInteger) o;
        if (this.name.equals(other.name)) {
            return true;
        } else {
            return false;
        }
    }
}