/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package equalsTest;

/**
 * @author Xinux
 * @version $Id: TestIntegerOne.java, v 0.1 2015-06-15 12:19 AM Xinux Exp $$
 */
public class TestIntegerOne {

    public static void main(String[] args) {
        TestInteger person1 = new TestInteger("Xinux");
        TestInteger person2 = new TestInteger("Xinux");

        System.out.println(person1 == person2);
        System.out.println(person1.equals(person2));

    }

}