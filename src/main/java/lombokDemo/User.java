/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package lombokDemo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @author hanlin.xx
 * @version $Id: User.java, v 0.1 2015-06-16 13:41 hanlin.xx Exp $$
 */
@Data
public class User {
	@Setter
	@Getter
	private String name;

	private int userId;

}

