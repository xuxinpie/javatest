/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package regex;

import java.util.regex.Pattern;

/**
 * @author Xinux
 * @version $Id: Test.java, v 0.1 2016-06-07 7:45 PM Xinux Exp $$
 */
public class Test {

    public static void main(String[] args) {
        String[] regexs = {".*_\\d{4}_.*"};
        String fileName1 = "20160606_0009_THFUND_00.txt";
        String fileName2 = "20160606_index_THFUND_00.txt";
        String fileName3 = "thfund_user_profit_currency_20160614_0000";

        String[] regexs2 = {"thfund_user_profit_currency_\\d{8}_\\d{4}"};

        System.out.println(filenameAccepted(fileName1, regexs));
        System.out.println(filenameAccepted(fileName2, regexs));
        System.out.println(filenameAccepted(fileName3, regexs2));
    }

    /**
     * 校验文件名是否满足
     *
     * @param fileName
     * @param regexs
     * @return
     */
    private static boolean filenameAccepted(String fileName, String[] regexs) {
        if (regexs != null) {
            for (String regex : regexs) {
                Pattern pattern = Pattern.compile(regex);
                if (pattern.matcher(fileName).matches()) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

}