/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package regex;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式Demo
 * @author hanlin.xx
 * @version $Id: RegexDemo2.java, v 0.1 2015-07-13 14:42 hanlin.xx Exp $$
 */
public class RegexDemo2 {

	public static void main(String[] args) {
		String str = "我我...我我...我要..要要...要要...学学学....学学...编编编...编程..程.程程...程...程";
		test1(str);

		String mail = "xuxinpie@gmail.com";
		checkMail(mail);

		try {
			getMails();
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

	/**去除字符串中重复的单词*/
	public static void test1(String str) {
		//去除字符串中的"."
		str = str.replaceAll("\\.+", "");
		System.out.println(str);

		//去除重复的字
		str = str.replaceAll("(.)\\1+", "$1");
		System.out.println(str);
	}

	/** 邮箱校验 */
	public static void checkMail(String mail_Str) {
		String regex = "[a-zA-Z0-9_]+@[a-zA-Z0-9]+(\\.[a-zA-Z]+)+";
		System.out.println(mail_Str.matches(regex));
	}

	/** 获取指定文档中的邮件地址 */
	public static void getMails() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("D:\\Myproject\\gitRepository\\javaTest\\src\\main\\java\\regex\\mail.txt"));

		String line = null;
		String mailRegex = "^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$";
//		String mailRegex = "\\w+@\\w+(\\.\\w+)+";

		Pattern pattern = Pattern.compile(mailRegex);
		while (null != (line = br.readLine())) {
			Matcher matcher = pattern.matcher(line);
			while (matcher.find()) {
				System.out.println(matcher.group());
			}
		}

	}

	/**网页爬虫*/
	public static void getWebMails(String url_str) throws IOException {
		URL url = new URL(url_str);
		URLConnection conn = url.openConnection();

		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

		String line = null;

		String mailRegex = "^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$";

		Pattern pattern = Pattern.compile(mailRegex);

		while (null != (line = br.readLine())) {
			Matcher matcher = pattern.matcher(line);
			while (matcher.find()) {
				System.out.println(matcher.group());
			}
		}

	}


}

