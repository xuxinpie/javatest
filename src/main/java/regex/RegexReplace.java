package regex;

/**
 * Created by Xinux on 10/23/14.
 */
public class RegexReplace {
	public static void main(String[] args) {
		String str1 = "hello, my telephone num is 18501685428";
		String strTarget = str1.replaceAll("[^\\d]", "");   //排除型字符组， 与中括号一起使用
		System.out.println(strTarget);
	}
}
