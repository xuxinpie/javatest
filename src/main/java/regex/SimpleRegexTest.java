/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author hanlin.xx
 * @version $Id: SimpleRegexTest.java, v 0.1 2015-07-13 10:22 hanlin.xx Exp $$
 */
public class SimpleRegexTest {

	public static void main(String[] args) {
		/*String str = "55555562334444577777756666666666";
		String regex = "\\d{5,}";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);

		String s = "";

		while(matcher.find()) {
			s = matcher.group();
			s.replace(regex, "#");
		}

		System.out.println(s);*/

		String str = "Hello World";
		String regex = " +";
		String[] array = str.split(regex);

		for (String s : array) {
			System.out.println(s);
		}


	}

}

