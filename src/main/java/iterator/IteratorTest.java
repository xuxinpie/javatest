/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package iterator;

import util.StringUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Xinux
 * @version $Id: iterator.IteratorTest.java, v 0.1 2016-06-21 5:20 PM Xinux Exp $$
 */
public class IteratorTest {
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        list.add("a");
        list.add("b");
        list.add("c");

        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            if (StringUtil.equals("a", iterator.next())) {
                iterator.remove();
            }
        }

        for (String str : list) {
            System.out.println(str);
        }
    }
}