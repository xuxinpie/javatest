/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package jsonTest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * 使用org.json包进行json转换
 *
 * @author Xinux
 * @version $Id: JSONTest.java, v 0.1 2015-06-16 7:24 PM Xinux Exp $$
 */
public class JSONTest {

	public static void main(String[] args) {
		//JsonObject
		String jsonMessage1 = "{\"语文\":\"88\",\"数学\":\"78\",\"计算机\":\"99\"}";
		String value = "";
		try {
			JSONObject jsonObject = new JSONObject(jsonMessage1);
			value = jsonObject.getString("数学");
			System.out.println("value : " + value);
		} catch (JSONException e) {
			e.printStackTrace();
		}

		System.out.println("--------华丽丽的分割线---------");

		//JsonArray
		String jsonMessage2 = "[{'num':'成绩', '外语':88, '历史':65, '地理':99, 'object':{'aaa':'11a11','bbb':'2222','cccc':'3333'}}," +
				"{'num':'兴趣', '外语':28, '历史':45, '地理':19, 'object':{'aaa':'11b11','bbb':'2222','cccc':'3333'}}," +
				"{'num':'爱好', '外语':48, '历史':62, '地理':39, 'object':{'aaa':'11c11','bbb':'2222','cccc':'3333'}}]";

		try {
			JSONArray jsonArray = new JSONArray(jsonMessage2);
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				//获取每一个对象中的值
				String numString = jsonObject.getString("num");
				int englishScore = jsonObject.getInt("外语");
				int historyScore = jsonObject.getInt("历史");
				int geographyScore = jsonObject.getInt("地理");
				//获取数组中对象的对象
				JSONObject jsonObject2 = jsonObject.getJSONObject("object");
				String aaaString = jsonObject2.getString("aaa");
				System.out.println("aaaString="+aaaString);
				System.out.println("numString="+numString);
				System.out.println("englishScore="+englishScore);
				System.out.println("historyScore="+historyScore);
				System.out.println("geographyScore="+geographyScore);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

}