/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package exchanger;

import sun.plugin.javascript.navig.LinkArray;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Exchanger;

/**
 * Exchanger可以在两个线程之间交换数据，只能是2个线程，不支持更多的线程之间互换数据
 * @author hanlin.xx
 * @version $Id: TestExchanger.java, v 0.1 2015-06-21 0:18 hanlin.xx Exp $$
 */
public class TestExchanger {

    public static void main(String[] args) {
		final Exchanger<List<Integer>> exchanger = new Exchanger<List<Integer>>();

		/**
		 * 当线程A调用Exchange对象的exchange()方法后，他会陷入阻塞状态，
		 * 直到线程B也调用了exchange()方法，然后以线程安全的方式交换数据，之后线程A和B继续运行
		 */
		new Thread() {
			@Override
			public void run() {
				List<Integer> list = new ArrayList<Integer>(2);
				list.add(1);
				list.add(2);
				try {
					list = exchanger.exchange(list);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("Thread1 " + list);
			}
		}.start();

		new Thread() {
			@Override
			public void run() {
				List<Integer> list = new ArrayList<Integer>(2);
				list.add(3);
				list.add(4);
				try {
					list = exchanger.exchange(list);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("Thread2 " + list);
			}
		}.start();


	}
}
