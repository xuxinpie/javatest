/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package hash;

import java.util.Map;

/**
 * 当一个类有自己特有的“逻辑相等”概念, 需要重写equals方法
 * 改写equals()的时候，总是要改写hashCode()
 *
 * 使用HashMap，如果key是自定义的类，就必须重写hashcode()和equals()。
 *
 * @author Xinux
 * @version $Id: CacheKey.java, v 0.1 2016-10-11 10:48 AM Xinux Exp $$
 */
public class CacheKey {
    private TaskType            taskType;
    private Map<String, Object> pathExpress;

    public CacheKey(TaskType taskType, Map<String, Object> pathExpress) {
        this.taskType = taskType;
        this.pathExpress = pathExpress;
    }

    /**
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((pathExpress == null) ? 0 : pathExpress.hashCode());
        result = prime * result + ((taskType == null) ? 0 : taskType.hashCode());
        return result;
    }

    /**
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CacheKey other = (CacheKey) obj;
        if (pathExpress == null) {
            if (other.pathExpress != null)
                return false;
        } else if (!pathExpress.equals(other.pathExpress))
            return false;
        if (taskType != other.taskType)
            return false;
        return true;
    }
}