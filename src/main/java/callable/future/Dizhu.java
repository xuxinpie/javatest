/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package callable.future;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * ����һ�����ڵ������๤�ͳ����Ĺ���
 * ���������߳�
 * �๤��FutureTask
 * ������Callable
 * @author hanlin.xx
 * @version $Id: Dizhu.java, v 0.1 2015-06-30 11:50 hanlin.xx Exp $$
 */
public class Dizhu {

	public static void main(String[] args) {
		Changgong changgong = new Changgong();
		//������Ҫʵ��Callable�ӿ�
		FutureTask<Integer> jianggong = new FutureTask<Integer>(changgong);
		new Thread(jianggong).start();
		while (!jianggong.isDone()) {
			try {
				System.out.println("加紧干活");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		int amount = 0;
		try {
			amount = jianggong.get();
			System.out.println("地主获得收入: " + amount);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

}

