/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package callable.future;

import java.util.concurrent.Callable;

/**
 *
 * @author hanlin.xx
 * @version $Id: Changgong.java, v 0.1 2015-06-30 11:46 hanlin.xx Exp $$
 */
public class Changgong implements Callable<Integer> {

	private int workingHours = 12;

	private int productAmount = 0;

	public Integer call() throws Exception {
		while (workingHours > 0) {
			System.out.println("I'm working");
			productAmount += 5;
			workingHours--;
			Thread.sleep(1000);
		}
		return productAmount;
	}
}

