/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package callable;

import java.util.concurrent.*;

/**
 * 使用Future对象
 * Callable接口类似于Runnable，从名字就可以看出来了，但是Runnable不会返回结果，并且无法抛出返回结果的异常，
 * 而Callable功能更强大一些，被线程执行后，可以返回值，这个返回值可以被Future拿到
 * 也就是说，Future可以拿到异步执行任务的返回值
 * @author hanlin.xx
 * @version $Id: TestCallable.java, v 0.1 2015-06-30 10:58 hanlin.xx Exp $$
 */
public class TestCallable {
    public static void main(String[] args) {
        //ExecuteService继承了Executor，Executor也是一个接口，里面只有一个方法
        ExecutorService excutor = Executors.newCachedThreadPool();
        Task task = new Task();
        Future<Integer> result = excutor.submit(task);
        //并不是终止线程的运行，而是禁止在这个Executor中添加新的任务
        excutor.shutdown();

        System.out.println("主线程正在执行");

        try {
            System.out.println("task的运行结果 " + result.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        System.out.println("所用的任务执行完毕");

    }

    public static class Task implements Callable<Integer> {

        public Integer call() throws Exception {
            System.out.println("子线程正在计算");
            System.out.println("sleep 3000ms");
            Thread.sleep(3000);
            int sum = 0;
            for (int i = 0; i < 100; i++) {
                sum += i;
            }
            return sum;
        }
    }
}
