/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package callable;

import java.util.concurrent.*;

/**
 * 使用FutureTask对象
 *
 * FutureTask则是一个RunnableFuture<V>，即实现了Runnbale又实现了Futrue<V>这两个接口，
 * 另外它还可以包装Runnable(实际上会转换为Callable)和Callable
 * <V>，所以一般来讲是一个符合体了，它可以通过Thread包装来直接执行，也可以提交给ExecuteService来执行
 * 并且还可以通过v get()返回执行结果，在线程体没有执行完成的时候，主线程一直阻塞等待，执行完则直接返回结果。
 *
 * @author hanlin.xx
 * @version $Id: TestCallable.java, v 0.1 2015-06-30 10:58 hanlin.xx Exp $$
 */
public class TestCallable2 {
    public static void main(String[] args) {
        ExecutorService excutor = Executors.newCachedThreadPool();
        Task task = new Task();
        /**
         * FutureTask implements RunnableFuture
         * RunnableFuture extends Future and Runnable
         * 既可以作为Runnable被线程执行，又可以作为Future得到Callable的返回值
         */
        FutureTask<Integer> futureTask = new FutureTask<Integer>(task);
        excutor.submit(futureTask);
        excutor.shutdown();

        /*try {
             Thread.sleep(1000);
         } catch (InterruptedException e) {
             e.printStackTrace();
         }*/

        System.out.println("主线程正在执行");

        try {
            //调用fureTask.get()时如果计算没有完成会发生阻塞
            System.out.println("task的" + futureTask.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        System.out.println("所用的任务执行完毕");

    }

    public static class Task implements Callable<Integer> {

        public Integer call() throws Exception {
            System.out.println("子线程正在计算");
            Thread.sleep(3000);
            int sum = 0;
            for (int i = 0; i < 100; i++) {
                sum += i;
            }
            return sum;
        }
    }
}
