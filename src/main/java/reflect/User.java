/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package reflect;

/**
 * @author hanlin.xx
 * @version $Id: User.java, v 0.1 2015-07-08 14:58 hanlin.xx Exp $$
 */
public class User {

	private String name;
	private String sex;
	private int age;

	public User(String name, String sex, int age) {
		this.name = name;
		this.sex = sex;
		this.age = age;
	}

	public User() {
		super();
	}

	private int calWeight(Integer weight) {
		return weight + 1;
	}

	private void outInfo() {
		System.out.println("测试java反射的测试类");
	}
}

