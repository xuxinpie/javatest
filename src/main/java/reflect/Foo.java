/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package reflect;

/**
 * @author hanlin.xx
 * @version $Id: Foo.java, v 0.1 2015-07-08 15:12 hanlin.xx Exp $$
 */
public class Foo {

	private String msg;

	public Foo(String msg) {
		this.msg = msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	private void outInfo() {
		System.out.println("测试java反射的测试类");
	}

}

