/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author hanlin.xx
 * @version $Id: TestRef.java, v 0.1 2015-07-08 15:15 hanlin.xx Exp $$
 */
public class TestRef {
	public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
		Foo foo = new Foo("这是一个Foo对象");
		Class clazz = foo.getClass();
		Method method1 = clazz.getDeclaredMethod("outInfo");
		Method method2 = clazz.getDeclaredMethod("setMsg", String.class);
		Method method3 = clazz.getDeclaredMethod("getMsg");

		method1.setAccessible(true);
		method1.invoke(foo);
		method2.invoke(foo, "重新设置Msg信息");
		String msg = (String) method3.invoke(foo);
		System.out.println(msg);
	}
}

