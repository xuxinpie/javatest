/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package reflect.filedTest;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 测试类
 * @author hanlin.xx
 * @version $Id: test.java, v 0.1 2015-07-09 14:27 hanlin.xx Exp $$
 */
public class test {

    public static void main(String[] args) {
        testRefect();
    }

    private static void testRefect() {
        try {
            //返回类中的任何可见性的属性(不包括基类)
            System.out.println("Declared fileds: ");
            Field[] fields = Dog.class.getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                System.out.println(fields[i].getName());
            }

            //获得类中指定的public属性（包括基类）
            System.out.println("public fields: ");
            fields = Dog.class.getFields();
            for (int i = 0; i < fields.length; i++) {
                System.out.println(fields[i].getName());
            }

            //getMethod()只能调用共有方法，不能反射调用私有方法
            Dog dog = new Dog();
            dog.setAge(1);
            Method method1 = dog.getClass().getMethod("getAge", null);
            Object value = method1.invoke(dog);
            System.out.println("age: " + value);

            //调用基类的private类型方法

            /**先实例化一个Animal的对象 */
            Animal animal = new Animal();
            Class a = animal.getClass();
            //Animal中getHeight方法是私有方法，只能使用getDeclaredMethod
            Method method2 = a.getDeclaredMethod("getHeight", null);
            method2.setAccessible(true);

            //java反射无法传入基本类型变量，可以通过如下形式
            int param = 12;
            Class[] argsClass = new Class[] { int.class };
            //Animal中getHeight方法是共有方法，可以使用getMethod
            Method method3 = a.getMethod("setHeight", argsClass);
            method3.invoke(animal, param);

            //Animal中height变量声明为static变量，这样在重新实例化一个Animal对象后调用getHeight()，还可以读到height的值
            int height = (Integer) method2.invoke(animal);
            System.out.println("height: " + height);

            /**不用先实例化一个Animal,直接通过反射来获得animal的class对象*/
            Class anotherAnimal = Class.forName("reflect.filedTest.Animal");
            //Animal中getHeight方法是私有方法，只能使用getDeclaredMethod
            Method method4 = anotherAnimal.getDeclaredMethod("getHeight", null);
            method4.setAccessible(true);

            //java反射无法传入基本类型变量，可以通过如下形式
            int param2 = 15;
            Class[] argsClass2 = new Class[] { int.class };
            //Animal中getHeight方法是共有方法，可以使用getMethod
            Method method5 = anotherAnimal.getMethod("setHeight", argsClass2);
            method5.invoke(anotherAnimal.newInstance(), param2);

            //Animal中height变量必须声明为static变量，这样在重新实例化一个Animal对象后调用getHeight()才能读到height的值
            // 否则重新实例化一个新的Animal对象，读到的值为初始值
            int height2 = (Integer) method4.invoke(anotherAnimal.newInstance());
            System.out.println("height:" + height2);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

}
