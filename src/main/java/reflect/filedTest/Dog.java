/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package reflect.filedTest;

/**
 * 子类 —— Dog
 * @author hanlin.xx
 * @version $Id: Dog.java, v 0.1 2015-07-09 14:23 hanlin.xx Exp $$
 */
public class Dog extends Animal {

	public Dog() {

	}

	public String color;

	protected String name;

	private String type;

	String fur;

}

