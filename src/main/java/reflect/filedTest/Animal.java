/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package reflect.filedTest;

/**
 * 基类Animal
 * @author hanlin.xx
 * @version $Id: Animal.java, v 0.1 2015-07-09 14:22 hanlin.xx Exp $$
 */
public class Animal {

	public Animal() {

	}

	public int location;

	protected int age;

	private int height;

	int length;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	private int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

}

