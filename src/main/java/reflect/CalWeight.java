/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author hanlin.xx
 * @version $Id: CalWeight.java, v 0.1 2015-07-08 15:01 hanlin.xx Exp $$
 */
public class CalWeight {
	public static void main(String[] args) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
		Class user = Class.forName("reflect.User");
		/*User u = new User("Xinux", "male", 25);
		Class user = u.getClass();*/
		Method method1 = user.getDeclaredMethod("calWeight", Integer.class);
		Method method2 = user.getDeclaredMethod("outInfo");
		method1.setAccessible(true);
		method2.setAccessible(true);

		method2.invoke(user.newInstance());
		//使用user.newInstance()或者u都是可以的，但要注意的是使用user.newInstance()的时候一定要无参的构造函数
//		method2.invoke(u);

		int result = (Integer)method1.invoke(user.newInstance(), new Integer(5));
		System.out.println(result);
	}
}

