package reflect.print;

import java.lang.reflect.Array;
import java.lang.reflect.Method;

/**
 * 循环打印出该对象所在类的类名和方法名
 * Created by Xinux on 10/23/14.
 */
public class PirntClassandMethod {
	public static void test(Object obj) {
		Class clazz = obj.getClass();
		Method[] ms = clazz.getDeclaredMethods();
		long length = Array.getLength(ms);
		for(int i = 0; i < length; i++) {
			System.out.println("类名: " + clazz.getName() + " 方法名：" + ms[i].getName());
		}
	}

	class A {
		public void b() {}
		public void c() {}
		public void d() {}
		public void e() {}
	}

	public static void main(String[] args) {
		PirntClassandMethod Test = new PirntClassandMethod();
		PirntClassandMethod.A a = Test.new A();
		//结果会出现重排
		test(a);
	}
}
