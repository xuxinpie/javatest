/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import java.util.*;

/**
 * json格式转换
 *
 * @author Xinux
 * @version $Id: JsonConvertUtils.java, v 0.1 2016-07-08 10:54 AM Xinux Exp $$
 */
public class JsonConvertUtils {

    /**
     * 将Map转换为json格式的字符串
     *
     * @param map
     * @return
     */
    public static String mapToStr(Map<String, String> map) {
        Assert.notNull(map, "map对象不能为空！");
        JSONObject jsonObject = JSONObject.fromObject(map);
        return jsonObject.toString();
    }

    /**
     * 转换为Map
     *
     * @param str
     * @return
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static Map<String, String> strToMap(String str) {
        if (StringUtils.isBlank(str)) {
            return new HashMap<String, String>();
        }

        JSONObject jsonObject = JSONObject.fromObject(str);
        Map<String, String> resultMap = (Map<String, String>) JSONObject.toBean(jsonObject,
                Map.class);
        if (resultMap == null || resultMap.size() <= 0) {
            return resultMap;
        }
        Iterator iter = resultMap.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String key = (String) entry.getKey();
            Object val = entry.getValue();
            if (val instanceof JSONObject) {
                JSONObject tempJsonObject = (JSONObject) val;
                if (tempJsonObject.isNullObject()) {
                    resultMap.put(key, null);
                }
            }
        }
        return resultMap;
    }

    /**
     * 将对象转换成字符串
     *
     * @param obj
     * @return
     */
    public static String objectToString(Object obj) {
        if (obj == null) {
            return "";
        }
        return "[" + JSONObject.fromObject(obj).toString() + "]";
    }

    /**
     * 将字符串转换成相应对象
     *
     * @param string
     * @return
     */
    @SuppressWarnings({ "static-access", "rawtypes" })
    public static Object stringToObject(String string, Class beanClass) {
        if (StringUtils.isBlank(string) || beanClass == null) {
            return null;
        }
        JSONArray jsonarray = JSONArray.fromObject(string);
        JSONObject jsonObject = null;
        for (int i = 0; i < jsonarray.size(); i++) {
            jsonObject = jsonarray.getJSONObject(i);
        }
        return jsonObject.toBean(jsonObject, beanClass);
    }

    /**
     * json转换为列表
     * @param json
     * @param beanClass
     * @return
     */
    public static <T> List<T> strToList(String json, Class<T> beanClass) {
        if (StringUtils.isBlank(json) || beanClass == null) {
            return null;
        }

        List<T> jsonList = new ArrayList<T>();

        JSONArray jsonarray = JSONArray.fromObject(json);
        for (int i = 0; i < jsonarray.size(); i++) {
            @SuppressWarnings("unchecked")
            T bean = (T) JSONObject.toBean(jsonarray.getJSONObject(i), beanClass);
            jsonList.add(bean);
        }

        return jsonList;
    }
}