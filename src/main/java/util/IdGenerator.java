/**
 * Bilibili.com Inc.
 * Copyright (c) 2009-2017 All Rights Reserved.
 */
package util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.RandomStringUtils;

/**
 * 序列号生成器
 *
 * @author Xinux
 * @version $Id: IdGenerator.java, v 0.1 2017-01-19 3:08 PM Xinux Exp $$
 */
public class IdGenerator {

private final static String        str62    = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

private final static int           pixLen   = 36;

private static volatile int        pixOne   = 0;

private static volatile int        pixTwo   = 0;

private static volatile int        pixThree = 0;

private static volatile int        pixFour  = 0;

/**
 * 生成短时间内不会重复的长度为24位的字符串。<br/>
 * 生成策略为获取自当前时间的精确到毫秒数的字符串，该字符串长度为23位<br/>
 * 并追加四位"0-z"的自增字符串.<br/>
 * 本方法至少可以保证在系统返回的一个毫秒数内生成36的4次方 * 1000 个（1679616000）ID不重复。<br/>
 *
 *
 * @return 23位短时间不会重复的字符串。<br/>
 */
public static final synchronized String genId() {
    StringBuilder sb = new StringBuilder(24);
    // 去当前时间戳,精确到毫秒 共17个字符
    String currentTimeStamp = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());

    // 随机四位: 36 * 36 * 36 * 36 共1679616中组合
    pixFour++;
    if (pixFour == pixLen) {
        pixFour = 0;
        pixThree++;
        if (pixThree == pixLen) {
            pixThree = 0;
            pixTwo++;
            if (pixTwo == pixLen) {
                pixTwo = 0;
                pixOne++;
                if (pixOne == pixLen) {
                    pixOne = 0;
                }
            }
        }
    }
    String shardingRandomKey = RandomStringUtils.random(3, "0123456789");
    return sb.append(currentTimeStamp).append(str62.charAt(pixOne)).append(str62.charAt(pixTwo))
            .append(str62.charAt(pixThree)).append(str62.charAt(pixFour)).append(shardingRandomKey).toString();
}

}