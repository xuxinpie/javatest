/**
 * Bilibili.com Inc.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package util.thirdParty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;


/**
 * 微信支付工具类
 *
 * @author Xinux
 * @version $Id: WechatUtils.java, v 0.1 2016-11-17 2:35 PM Xinux Exp $$
 */
public class WechatUtils {

/**
 * @param map
 * @return xml字符串
 * @description 将map转为xml字符串
 */
public static String map2XmlString(Map<String, String> map) {

    String xmlResult = "";
    StringBuffer sb = new StringBuffer();
    sb.append("<xml>");
    for (String key : map.keySet()) {
        sb.append("<").append(key).append(">").append(map.get(key)).append("</").append(key).append(">");
    }
    sb.append("</xml>");
    xmlResult = sb.toString();

    return xmlResult;
}

/**
 * @param xml
 * @return Map key-value对
 * @description 将xml字符串转换成map
 */
public static Map<String, String> xmlString2Map(String xml) throws DocumentException {
    Map<String, String> map = new HashMap<String, String>();
    Document doc = null;
    // 将字符串转为XML
    doc = DocumentHelper.parseText(xml);
    // 获取根节点
    Element rootElement = doc.getRootElement();
    // 获取根节点下所有节点
    @SuppressWarnings("unchecked")
    List<Element> elements = rootElement.elements();
    for (Element element : elements) { // 遍历节点
        // 节点的name为map的key，text为map的value
        String key = element.getName();
        StringBuilder sb = new StringBuilder();
        sb.append("<").append(key).append("><!\\[CDATA\\[(.*)\\]\\]></").append(key).append(">");
        Pattern regexPattern = Pattern.compile(sb.toString());
        Matcher matcher = regexPattern.matcher(element.getText());
        if (matcher.find()) {
            map.put(element.getName(), matcher.group(1));
            ;
        } else {
            map.put(element.getName(), element.getText());
        }

    }
    return map;
}


}