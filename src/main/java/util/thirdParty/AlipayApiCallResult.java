/**
 * Bilibili.com Inc.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package util.thirdParty;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Xinux
 * @version $Id: AlipayApiCallResult.java, v 0.1 2016-11-17 6:36 PM Xinux Exp $$
 */
public class AlipayApiCallResult {

/**
 * 请求是否成功
 */
private boolean success;

/**
 * 查询交易信息失败的错误代码
 */
private String error;

/**
 * request Map
 */
private Map<String, String> requestMap = new HashMap<String, String>();

/**
 * response Map
 */
private Map<String, String> responseMap = new HashMap<String, String>();

/**
 * 签名
 */
private String sign;

/**
 * 签名方式
 */
private String signType;

/**
 * Setter method for property isSuccess.
 *
 * @param success value to be assigned to property success
 */
public void setSuccess(boolean success) {
    this.success = success;
}

/**
 * Getter method for property success.
 *
 * @return property value of success
 */
public boolean isSuccess() {
    return success;
}

/**
 * Getter method for property error.
 *
 * @return property value of error
 */
public String getError() {
    return error;
}

/**
 * Setter method for property error.
 *
 * @param error value to be assigned to property error
 */
public void setError(String error) {
    this.error = error;
}

/**
 * Getter method for property sign.
 *
 * @return property value of sign
 */
public String getSign() {
    return sign;
}

/**
 * Setter method for property sign.
 *
 * @param sign value to be assigned to property sign
 */
public void setSign(String sign) {
    this.sign = sign;
}

/**
 * Getter method for property signType.
 *
 * @return property value of signType
 */
public String getSignType() {
    return signType;
}

/**
 * Setter method for property signType.
 *
 * @param signType value to be assigned to property signType
 */
public void setSignType(String signType) {
    this.signType = signType;
}

/**
 * Getter method for property requestMap.
 *
 * @return property value of requestMap
 */
public Map<String, String> getRequestMap() {
    return requestMap;
}

/**
 * Getter method for property responseMap.
 *
 * @return property value of responseMap
 */
public Map<String, String> getResponseMap() {
    return responseMap;
}

/**
 * 添加key-value到RequestMap
 *
 * @param key
 * @param value
 */
public void addRequestData(String key, String value) {
    requestMap.put(key, value);
}

/**
 * 添加key-value到ResponseMap
 *
 * @param key
 * @param value
 */
public void addResponseData(String key, String value) {
    responseMap.put(key, value);
}

/**
 * 根据Key获取RequestMap中的值
 *
 * @param key
 */
public String getRequestData(String key) {
    return requestMap.get(key);
}

/**
 * 根据Key获取RequestMap中的值
 *
 * @param key
 */
public String getResponseData(String key) {
    return responseMap.get(key);
}

}