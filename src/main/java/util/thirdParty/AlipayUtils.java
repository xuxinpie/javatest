/**
 * Bilibili.com Inc.
 * Copyright (c) 2009-2016 All Rights Reserved.
 */
package util.thirdParty;

import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StreamUtils;

import util.StringUtil;

/**
 * @author Xinux
 * @version $Id: AlipayUtils.java, v 0.1 2016-11-17 5:11 PM Xinux Exp $$
 */
public class AlipayUtils {

/**
 * 将参数的key-value值组装成url参数形式
 *
 * @param paramsMap
 * @return url参数
 * @author Xinux
 */
public static String map2UrlParams(Map<String, String> paramsMap, boolean isSort,
                                   String rsaPrivateKeyFile) {
    List<String> keys = new ArrayList<String>(paramsMap.keySet());
    // 需要排序的情况下进行排序
    if (isSort) {
        Collections.sort(keys);
    }

    StringBuilder paramsUrl = new StringBuilder();

    // 前n-1个参数拼接
    for (int i = 0; i < keys.size() - 1; i++) {
        paramsUrl.append(String.format("%s=%s&", keys.get(i), paramsMap.get(keys.get(i))));
    }

    // 第n个参数拼接
    paramsUrl.append(String.format("%s=%s%s%s", keys.get(keys.size() - 1),
            paramsMap.get(keys.get(keys.size() - 1))));

    String urlParams = paramsUrl.toString();

    // 将拼接的字符创和支付宝私钥一起进行加密
    String sign = URLEncoder.encode(getAliPayRsaSign(urlParams, rsaPrivateKeyFile));

    return String.format("%s&sign=%s&sign_type=RSA", urlParams, sign);

}

/**
 * 将支付宝同步查单接口返回的url中的参数提取成key-value并保存在map中
 *
 * @param xmlStr 支付宝返回结果
 * @return 参数key-value形式
 * @author Xinux
 */
public static AlipayApiCallResult orderQueryURLParams2Map(String xmlStr) throws DocumentException {
    AlipayApiCallResult callResult = new AlipayApiCallResult();

    // 将字符串转为Dom
    Document doc = DocumentHelper.parseText(xmlStr);
    // 获取根节点
    Element rootElt = doc.getRootElement();

    // 请求是否成功
    callResult.setSuccess(StringUtil.equals(rootElt.element("is_success").getTextTrim(), "T"));
    // 设置错误码(请求成功情况下为null)
    if (null != rootElt.element("error")) {
        callResult.setError(rootElt.element("error").getTextTrim());
    }
    callResult.setSign(rootElt.element("sign").getTextTrim());
    callResult.setSignType(rootElt.element("sign_type").getTextTrim());

    // 遍历获得请求的参数
    Element requestElement = rootElt.element("request");
    List<Element> requestDataElements = requestElement.elements();
    for (Element element : requestDataElements) { // 遍历节点
        // 节点的name为map的key，text为map的value
        List<Attribute> listAttr = element.attributes();
        callResult.addRequestData(listAttr.get(0).getValue(), element.getTextTrim());
    }

    // 遍历获得返回的参数
    Element responseElement = rootElt.element("response").element("trade");
    List<Element> responseDataElements = responseElement.elements();
    for (Element element : responseDataElements) { // 遍历节点
        // 节点的name为map的key，text为map的value
        callResult.addResponseData(element.getName(), element.getTextTrim());
    }

    return callResult;
}

public static String getAliPayRsaSign(String content, String privateKeyFileUrl) {
    String charset = "UTF-8";
    try {
        Resource resource = new ClassPathResource(privateKeyFileUrl);
        String privateKey = StreamUtils.copyToString(
                resource.getInputStream(), Charset.defaultCharset());
        PKCS8EncodedKeySpec priPKCS8 = new PKCS8EncodedKeySpec(
                AliPayCommonHelper.decode(privateKey));
        KeyFactory keyf = KeyFactory.getInstance("RSA");
        PrivateKey priKey = keyf.generatePrivate(priPKCS8);

        java.security.Signature signature =
                java.security.Signature.getInstance("SHA1WithRSA");

        signature.initSign(priKey);
        signature.update(content.getBytes(charset));

        byte[] signed = signature.sign();

        return AliPayCommonHelper.encode(signed);
    } catch (Exception e) {
        e.printStackTrace();
    }
    return null;
}

}