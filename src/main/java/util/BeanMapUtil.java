/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package util;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.springframework.util.Assert;

/**
 * javabean map 互转
 *
 * @author Xinux
 * @version $Id: BeanMapUtil.java, v 0.1 2016-08-24 2:38 PM Xinux Exp $$
 */
public class BeanMapUtil {
    /**
     * Map转换成bean
     *
     * @param map
     * @param type
     * @return
     */
    public static Object convertToBean(Map<String, Object> map, Class<?> type) {
        if (null == map) {
            return null;
        }

        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(type); // 获取类属性
            Object obj = type.newInstance(); // 创建 JavaBean 对象

            // 给 JavaBean 对象的属性赋值
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (int i = 0; i < propertyDescriptors.length; i++) {
                PropertyDescriptor descriptor = propertyDescriptors[i];
                String propertyName = descriptor.getName();

                if (map.containsKey(propertyName)) {
                    Object value = map.get(propertyName);
                    Object[] args = new Object[1];
                    args[0] = value;
                    Method writeMethod = descriptor.getWriteMethod();
                    if (writeMethod != null) {
                        writeMethod.invoke(obj, args);
                    }
                }
            }

            return obj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * bean转换成map
     *
     * @param bean
     * @return
     */
    public static Map<String, Object> convertToMap(Object bean) {
        Assert.notNull(bean);

        try {
            Class<?> type = bean.getClass();
            Map<String, Object> returnMap = new HashMap<String, Object>();
            BeanInfo beanInfo = Introspector.getBeanInfo(type);

            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (int i = 0; i < propertyDescriptors.length; i++) {
                PropertyDescriptor descriptor = propertyDescriptors[i];
                String propertyName = descriptor.getName();
                if (!propertyName.equals("class")) {
                    Method readMethod = descriptor.getReadMethod();
                    if (readMethod != null) {
                        Object result = readMethod.invoke(bean, new Object[0]);
                        if (result != null) {
                            returnMap.put(propertyName, result);
                        }
                    }
                }
            }
            return returnMap;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}