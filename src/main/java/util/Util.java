/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package util;

import java.util.Collections;
import java.util.Map;

/**
 * 集成常用的工具类
 *
 * @author Xinux
 * @version $Id: Util.java, v 0.1 2016-06-21 6:02 PM Xinux Exp $$
 */
public class Util {
    private static final ArrayUtil  ARRAY_UTIL  = new ArrayUtil();
    private static final ClassUtil  CLASS_UTIL  = new ClassUtil();
    private static final ObjectUtil OBJECT_UTIL = new ObjectUtil();
    private static final StringUtil STRING_UTIL = new StringUtil();
    private static final Map        ALL_UTILS   = Collections.unmodifiableMap(ArrayUtil
                                                    .toMap(new Object[][] {
            { "arrayUtil", ARRAY_UTIL }, { "classUtil", CLASS_UTIL },
            { "objectUtil", OBJECT_UTIL }, { "stringUtil", STRING_UTIL }, }));

    /**
     * 取得<code>ArrayUtil</code>。
     *
     * @return <code>ArrayUtil</code>实例
     */
    public static ArrayUtil getArrayUtil() {
        return ARRAY_UTIL;
    }

    /**
     * 取得<code>ClassUtil</code>。
     *
     * @return <code>ClassUtil</code>实例
     */
    public static ClassUtil getClassUtil() {
        return CLASS_UTIL;
    }

    /**
     * 取得<code>ObjectUtil</code>。
     *
     * @return <code>ObjectUtil</code>实例
     */
    public static ObjectUtil getObjectUtil() {
        return OBJECT_UTIL;
    }

    /**
     * 取得<code>StringUtil</code>。
     *
     * @return <code>StringUtil</code>实例
     */
    public static StringUtil getStringUtil() {
        return STRING_UTIL;
    }

    /**
     * 取得包含所有utils的map
     *
     * @return utils map
     */
    public static Map getUtils() {
        return ALL_UTILS;
    }
}
