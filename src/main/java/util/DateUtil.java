package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <pre>
 *     日期处理工具类
 * </pre>
 * @author Xinux
 * @version $Id: DateUtil.java, v 0.1 2016-06-17 11:16 AM Xinux Exp $$
 */
public class DateUtil {

    private static final Log   LOG                                = LogFactory
                                                                      .getLog(DateUtil.class);
    /**
     * 完整时间 yyyy-MM-dd HH:mm:ss
     */
    public static final String SIMPLE                             = "yyyy-MM-dd HH:mm:ss";

    /**
     * 完整时间 yyyy-MM-dd HH:mm:ss,SSS
     */
    public static final String SIMPLEMS                           = "yyyy-MM-dd HH:mm:ss,SSS";

    /**
     * 年月日 yyyy-MM-dd
     */
    public static final String DTSIMPLE                           = "yyyy-MM-dd";

    /**
     * 年月日(中文) yyyy年MM月dd日
     */
    public static final String DTSIMPLECHINESE                    = "yyyy年MM月dd日";

    /**
     * 年月日(无下划线) yyyyMMdd
     */
    public static final String DTMONTH                            = "yyyyMM";

    /**
     * 年月日(无下划线) yyyyMMdd
     */
    public static final String DTSHORT                            = "yyyyMMdd";

    /**
     * 年月日时(无下划线) yyyyMMddHH
     */
    public static final String DTHOUR                             = "yyyyMMddHH";

    /**
     * 年月日时分(无下划线) yyyyMMddHHmm
     */
    public static final String DTMIDDLE                           = "yyyyMMddHHmm";

    /**
     * 年月日时分秒(无下划线) yyyyMMddHHmmss
     */
    public static final String DTLONG                             = "yyyyMMddHHmmss";

    /**
     * 时分秒 HH:mm:ss
     */
    public static final String HMSFORMAT                          = "HH:mm:ss";

    /**
     * 时分 HHmm
     */
    public static final String HMFORMAT                           = "HHmm";

    /**
     * 时分 HH:mm
     */
    public static final String HMFORMAT2                          = "HH:mm";

    /**
     * 年.月.日 小时:分钟 yyyy.MM.dd HH:mm
     */
    public static final String ZXSIMPLEFORMAT                     = "yyyy.MM.dd HH:mm";

    /**
     * 年-月-日 小时:分钟 yyyy-MM-dd HH:mm
     */
    public static final String SIMPLEFORMAT                       = "yyyy-MM-dd HH:mm";

    /**
     * 年月日(中文) yyyy年MM月dd日 HH:mm
     */
    public static final String DTSIMPLECHINESEWITHTIME            = "yyyy年MM月dd日 HH:mm";

    /**
     * 年月日(中文) yyyy年MM月dd日 HH:mm:ss
     */
    public static final String DTSIMPLECHINESEWITHTIMESECOND      = "yyyy年MM月dd日 HH:mm:ss";

    /**
     * 月日(中文) MM月dd日
     */
    public static final String DTSIMPLECHINESEWITHOUTYEAR         = "MM月dd日";

    /**
     * 月日 MM-dd
     */
    public static final String DTSIMPLEWITHOUTYEAR                = "MM-dd";

    /**
     * 月日(中文) MM月dd日 HH:mm:ss
     */
    public static final String DTSIMPLECHINESEWITHOUTYEARWITHTIME = "MM月dd日 HH:mm:ss";

    /**
     * 24点日期后缀
     */
    public static final String DATE_SUFFIX                        = " 24:00";

    /**
     * 被Date.toString() 转换过的日期（格式形如Fri Aug 15 08:02:00 CST 2016），再转Date时使用
     */
    public final static String DATE_TO_STRING_FORMAT              = "EEE MMM dd HH:mm:ss zzz yyyy";

    /**
     * 将Date.toString() 转换过的日期（格式形如Fri Aug 15 08:02:00 CST 2014）转回Date
     *
     * @param dateStr
     * @return Date
     */
    public static Date parseToStringDate(String dateStr) {
        if (StringUtil.isBlank(dateStr)) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_TO_STRING_FORMAT, Locale.UK);
        try {
            return simpleDateFormat.parse(dateStr);
        } catch (ParseException e) {
            LOG.warn("日期转化失败,date=" + dateStr, e);
        }
        return null;
    }

    /**
     * 返回下一天,截断到0点
     */
    public static Date getNextShortDate(Date date) {
        String smpDateStr = DateUtil.shortDate(date);

        try {
            Date today = DateUtil.shortstring2Date(smpDateStr);
            return DateUtil.addDays(today, 1);
        } catch (ParseException e) {
            LOG.warn("日期转化失败,date=" + date, e);
        }
        return null;
    }

    /**
     * 获取格式
     *
     * @param format
     * @return
     */
    public static final DateFormat getFormat(String format) {
        return new SimpleDateFormat(format);
    }

    /**
     * yyyy-MM-dd HH:mm:ss,SSS
     *
     * @param date
     * @return
     */
    public static final String simplemsFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getFormat(SIMPLEMS).format(date);
    }

    /**
     * yyyy-MM-dd HH:mm:ss
     *
     * @param date
     * @return
     */
    public static final String simpleFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getFormat(SIMPLE).format(date);
    }

    /**
     * yyyy.MM.dd HH:mm
     *
     * @param date
     * @return
     */
    public static final String zxSimpleFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getFormat(ZXSIMPLEFORMAT).format(date);
    }

    /**
     * yyyy-MM-dd
     *
     * @param date
     * @return
     */
    public static final String dtSimpleFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getFormat(DTSIMPLE).format(date);
    }

    /**
     * yyyyMMdd
     *
     * @param date
     * @return
     */
    public static final String dtShortSimpleFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getFormat(DTSHORT).format(date);
    }

    /**
     * <pre>
     *  yyyyMMddHHmm
     * </pre>
     *
     * @param date
     * @return
     */
    public static final String dtMiddleSimpleFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getFormat(DTMIDDLE).format(date);
    }

    /**
     * <pre>
     *  yyyyMMddHHmmss
     * </pre>
     *
     * @param date
     * @return
     */
    public static final String dtLongSimpleFormat(Date date) {
        if (date == null) {
            return "";
        }
        return getFormat(DTLONG).format(date);
    }

    /**
     * <pre>
     *  yyyyMMddHHmmss格式字符串转化成日期
     * </pre>
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public static final Date longDtSimpleFormat(String date) throws ParseException {
        if (date == null) {
            return null;
        }
        return getFormat(DTLONG).parse(date);
    }

    /**
     * @param strDate yyyyMMdd
     * @return yyyy-MM-dd
     */
    public static final String dtFromShortToSimpleStr(String strDate) {
        if (null != strDate) {
            Date date;
            try {
                date = shortstring2Date(strDate);
            } catch (ParseException e) {
                date = null;
            }
            if (null != date) {
                return dtSimpleFormat(date);
            }
        }
        return "";
    }

    /**
     * yyyy-mm-dd 日期格式转换为日期
     *
     * @param strDate
     * @return
     */
    public static final Date strToDtSimpleFormat(String strDate) {
        if (strDate == null) {
            return null;
        }

        try {
            return getFormat(DTSIMPLE).parse(strDate);
        } catch (Exception e) {
            LOG.error("date format ", e);
        }

        return null;
    }

    /**
     * yyyy年MM月dd日
     *
     * @param date
     * @return
     */
    public static final String dtSimpleChineseFormat(Date date) {
        if (date == null) {
            return "";
        }

        return getFormat(DTSIMPLECHINESE).format(date);
    }

    /**
     * MM月dd日
     *
     * @param date
     * @return
     */
    public static final String dtSimpleChineseFormatWithoutYear(Date date) {
        if (date == null) {
            return "";
        }

        return getFormat(DTSIMPLECHINESEWITHOUTYEAR).format(date);
    }

    /**
     * MM月dd日 HH:mm:ss
     *
     * @param date
     * @return
     */
    public static final String dtSimpleChineseFormatWithoutYearWithTime(Date date) {
        if (date == null) {
            return "";
        }

        return getFormat(DTSIMPLECHINESEWITHOUTYEARWITHTIME).format(date);
    }

    /**
     * yyyy-MM-dd到 yyyy年MM月dd日 转换
     *
     * @param date
     * @return
     */
    public static final String dtSimpleChineseFormatStr(String date) throws ParseException {
        if (date == null) {
            return "";
        }

        return getFormat(DTSIMPLECHINESE).format(string2Date(date));
    }

    /**
     * yyyy-MM-dd 日期字符转换为时间
     *
     * @param stringDate
     * @return
     * @throws ParseException
     */
    public static final Date string2Date(String stringDate) throws ParseException {
        if (stringDate == null) {
            return null;
        }

        return getFormat(DTSIMPLE).parse(stringDate);
    }

    /**
     * 返回日期时间（Add by Sunzy）
     *
     * @param stringDate "yyyy-MM-dd HH:mm:ss"
     * @return
     * @throws ParseException
     */
    public static final Date string2DateTime(String stringDate) throws ParseException {
        if (stringDate == null) {
            return null;
        }

        return getFormat(SIMPLE).parse(stringDate);
    }

    /**
     * 返回日期时间（Add by Gonglei）
     *
     * @param stringDate (yyyyMMdd)
     * @return
     * @throws ParseException
     */
    public static final Date shortstring2Date(String stringDate) throws ParseException {
        if (stringDate == null) {
            return null;
        }

        return getFormat(DTSHORT).parse(stringDate);
    }

    /**
     * 返回短日期格式（yyyyMMdd格式）
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public static final String shortDate(Date date) {
        if (date == null) {
            return null;
        }

        return getFormat(DTSHORT).format(date);
    }

    /**
     * 返回长日期格式（yyyyMMddHHmmss格式）
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public static final String longDate(Date date) {
        if (date == null) {
            return null;
        }

        return getFormat(DTLONG).format(date);
    }

    /**
     * 计算当前时间几小时之后的时间
     *
     * @param date
     * @param hours
     * @return
     */
    public static Date addHours(Date date, long hours) {
        return addMinutes(date, hours * 60);
    }

    /**
     * 计算当前时间几分钟之后的时间
     *
     * @param date
     * @param minutes
     * @return
     */
    public static Date addMinutes(Date date, long minutes) {
        return addSeconds(date, minutes * 60);
    }

    /**
     * @param date1
     * @param secs
     * @return
     */

    public static Date addSeconds(Date date1, long secs) {
        return new Date(date1.getTime() + (secs * 1000));
    }

    /**
     * 获取下月
     *
     * @param StringDate
     * @return
     */
    public static String getNextMon(String StringDate) throws ParseException {
        Date tempDate = DateUtil.shortstring2Date(StringDate);
        Calendar cad = Calendar.getInstance();

        cad.setTime(tempDate);
        cad.add(Calendar.MONTH, 1);
        return DateUtil.shortDate(cad.getTime());
    }

    /**
     * add by daizhixia 20050808 获取下一天
     *
     * @param StringDate
     * @return
     * @throws ParseException
     */
    public static String getNextDay(String StringDate) throws ParseException {
        Date tempDate = DateUtil.string2Date(StringDate);
        Calendar cad = Calendar.getInstance();

        cad.setTime(tempDate);
        cad.add(Calendar.DATE, 1);
        return DateUtil.dtSimpleFormat(cad.getTime());
    }

    /**
     * 返回日期相差天数，向下取整数
     *
     * @param dateStart 一般前者小于后者dateEnd
     * @param dateEnd
     * @return
     */
    public static int countDays(Date dateStart, Date dateEnd) {
        if ((dateStart == null) || (dateEnd == null)) {
            return -1;
        }

        return (int) ((dateEnd.getTime() - dateStart.getTime()) / (1000 * 60 * 60 * 24));
    }

    /**
     * 将字符串按format格式转换为date类型
     *
     * @param str
     * @param format
     * @return
     */
    public static Date string2Date(String str, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);

        try {
            return sdf.parse(str);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 加减天数
     *
     * @param date
     * @return Date
     * @author shencb 2006-12 add
     */
    public static final Date increaseDate(Date date, int days) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, days);
        return cal.getTime();
    }

    /**
     * 年月日(中文) yyyy年MM月dd日 HH:mm:ss
     *
     * @param date
     * @return
     */
    public static final String dtSimpleChineseFormatWithTimeSecond(Date date) {
        if (date == null) {
            return "";
        }
        return getFormat(DTSIMPLECHINESEWITHTIMESECOND).format(date);
    }

    /**
     * 计算当前时间后几个天的时间.
     *
     * @param date 当前时间.
     * @param days 天数.
     * @return 当前时间后几个天的时间.
     * @author wb-xiaocl
     */
    public static Date addDays(final Date date, final int days) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        //化整为零，化一天为24小时
        final int hour = 24;
        calendar.add(Calendar.HOUR, days * hour);
        return calendar.getTime();
    }

    /**
     * 计算当前时间后几个年的时间.
     *
     * @param date  当前时间.
     * @param years 年数.
     * @return 当前时间后几个年的时间.
     * @author wb-xiaocl
     */
    public static Date addYears(final Date date, final int years) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, years);
        return calendar.getTime();
    }

    /**
     * default：据出生日期计算年龄（"yyyy-MM-dd"）.
     *
     * @param strBirthDay 字符串格式的出生日期.
     * @return 年龄.
     * @throws Exception 系统异常.
     */
    public static int getAgeByBirthday(final String strBirthDay) throws Exception {
        return getAgeByBirthday(strBirthDay, DTSIMPLE);
    }

    /**
     * 根据出生日期计算年龄.
     *
     * @param strBirthDay 字符串型日期.
     * @param format      日期格式.
     * @return 未来日期返回0.
     * @throws Exception 系统异常.
     * @author wb-xiaocl
     */
    public static int getAgeByBirthday(final String strBirthDay, final String format)
                                                                                     throws Exception {
        DateFormat dateFormat = new SimpleDateFormat(format);
        Date birthDay = dateFormat.parse(strBirthDay);
        return getAgeByBirthday(birthDay);
    }

    /**
     * 根据出生日期计算年龄.
     *
     * @param birthDay 出生日期.
     * @return 年龄（未来日期返回0）.
     * @throws Exception 系统异常.
     * @author wb-xiaocl
     */
    public static int getAgeByBirthday(final Date birthDay) throws Exception {

        Calendar cal = Calendar.getInstance();

        if (cal.before(birthDay)) {
            return 0;
        }

        int yearNow = cal.get(Calendar.YEAR);
        int monthNow = cal.get(Calendar.MONTH);
        int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);
        cal.setTime(birthDay);

        int yearBirth = cal.get(Calendar.YEAR);
        int monthBirth = cal.get(Calendar.MONTH);
        int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);

        int age = yearNow - yearBirth;

        if (monthNow <= monthBirth) {
            if (monthNow == monthBirth) {
                if (dayOfMonthNow < dayOfMonthBirth) {
                    age--;
                }
            } else {
                age--;
            }
        }

        return age;
    }

    /**
     * 转换成有效时间描述.
     *
     * @param range
     * @return
     */
    public static String genAvaibleRangDesc(final int range) {
        int lefRange = 0;
        int year = range / 365;
        lefRange = range % 365;
        int month = lefRange / 30;
        StringBuilder builder = new StringBuilder();
        if (year > 0) {
            builder.append(year).append("年");
        }
        if (month > 0) {
            builder.append(month).append("月");
        }
        return builder.toString();
    }

    /**
     * 计算当前时间前几个月或者后几个月的时间, 按Calendar.MONTH进行增减计算
     *
     * @param date
     * @param month
     * @return
     */
    public static Date addMonths(Date date, int month) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, month);
        return calendar.getTime();
    }

    /**
     * 判断一个日期是否属于昨天的日期s
     *
     * @param date
     * @return
     */
    public static boolean isYesterday(Date date) {

        Calendar c = Calendar.getInstance();
        c.set(Calendar.DATE, c.get(Calendar.DATE) - 1);
        Date today = c.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        return format.format(today).equals(format.format(date));

    }

    /**
     * 判断一个日期是否为今天
     *
     * @param date
     * @return
     */
    public static boolean isToday(Date date) {

        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        return format.format(today).equals(format.format(date));

    }

    /**
     * 判断一个日期是否为当月
     *
     * @param date
     * @return
     */
    public static boolean isInCurrentMonth(Date date) {

        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");

        return format.format(today).equals(format.format(date));

    }

    /**
     * 判断传入的时间是否是当天0点之前
     *
     * @param date
     * @return
     */
    public static boolean isBeforeToday(Date date) {
        if (date == null) {
            return false;
        }
        try {
            //判断传入时间是否为昨天
            return shortstring2Date(shortDate(date))
                .before(shortstring2Date(shortDate(new Date())));
        } catch (ParseException e) {
            return false;
        }
    }

    /**
     * 获取当前日期是星期几
     *
     * @param date
     * @return
     */
    public static String getWeekOfDate(Date date) {
        SimpleDateFormat dateFm = new SimpleDateFormat("EEEE");
        return dateFm.format(date);
    }

    /**
     * 获取指定格式日期
     * yyyy-mm-dd HH:mm
     *
     * @param date
     * @param format
     * @return
     */
    public static String zxSimpleFormat(Date date, String format) {
        if (date == null) {
            return "";
        }
        return new SimpleDateFormat(format).format(date);
    }

    /**
     * 获取第二天日期，字符串返回
     *
     * @param tempDate
     * @return
     * @throws ParseException
     */
    public static String getNextDayWith24Format(Date tempDate) {
        Calendar cad = Calendar.getInstance();

        cad.setTime(tempDate);
        cad.add(Calendar.DATE, 1);
        return zxSimpleFormat(cad.getTime(), DTSIMPLE) + DATE_SUFFIX;
    }

    /**
     * 获取第二天日期，日期格式
     *
     * @param tempDate
     * @return
     */
    public static Date getNextDay(Date tempDate) {
        Calendar cad = Calendar.getInstance();

        cad.setTime(tempDate);
        cad.add(Calendar.DATE, 1);
        return cad.getTime();
    }

    /**
     * 根据指定格式对日期进行格式化
     *
     * @param date   日期
     * @param format 格式
     * @return
     */
    public static String format(Date date, String format) {
        if (date == null) {
            return null;
        }

        return new SimpleDateFormat(format).format(date);
    }

    /**
     * 校验时分格式
     *
     * @param time
     * @return
     */
    public static boolean checkTime(String time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(HMFORMAT2);
        try {
            dateFormat.parse(time);
        } catch (Exception ex) {
            return false;
        }
        String[] temp = time.split(":");
        if ((temp[0].length() == 2 || temp[0].length() == 1) && temp[1].length() == 2) {
            int h, m;
            try {
                h = Integer.parseInt(temp[0]);
                m = Integer.parseInt(temp[1]);
            } catch (NumberFormatException e) {
                return false;
            }
            if (h >= 0 && h < 24 && m < 60 && m >= 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断两时间，获取较小的那个值
     *
     * @param date1 日期1
     * @param date2 日期2
     * @return
     */
    public static Date getMinDate(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            return null;
        }

        return date1.getTime() - date2.getTime() < 0 ? date1 : date2;
    }
}