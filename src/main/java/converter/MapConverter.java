/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Map 转为List或者String[]
 *
 * @author Xinux
 * @version $Id: MapConverter.java, v 0.1 2016-09-21 11:16 AM Xinux Exp $$
 */
public class MapConverter {

    public static void main(String[] args) {

        Map<String, String> map = new HashMap<String, String>();

        map.put("1", "AA");
        map.put("2", "BB");
        map.put("3", "CC");
        map.put("4", "DD");

        Collection<String> valueCollection = map.values();
        final int size = valueCollection.size();

        List<String> valueList = new ArrayList<String>(valueCollection);

        String[] valueArray = new String[size];

        map.values().toArray(valueArray);


    }



}