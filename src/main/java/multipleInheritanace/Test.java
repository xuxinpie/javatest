package multipleInheritanace;

/**
 * In this test case we implements multiple inheritance with java
 * @author XINUX
 *
 */
public class Test {
	public static void main(String[] args) {
		Son son = new Son();
		System.out.println("Son " + son.getStrong());
		System.out.println("Son " + son.getKind());
	}
}
