package multipleInheritanace;

/**
 * 利用内部类实现多继承
 */
public class Son {
	class Father_1 extends Father {
		public String strong() {
			return super.strong() + "er";
		}
	}
	
	class Mother_1 extends Mother {
		public String kind() {
			return super.kind() + "less";
		}
	}
	
	public String getStrong() {
		return new Father_1().strong();
	}
	
	public String getKind() {
		return new Mother_1().kind();
	}
}
