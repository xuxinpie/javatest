/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package threadlocalDemo;

/**
 * ThreadLocal 测试类
 *
 * @author hanlin.xx
 * @version $Id: TestLocalThread.java, v 0.1 2015-06-09 17:37 hanlin.xx Exp $$
 */
public class TestLocalThread {

    ThreadLocal<Long>   longLocal   = new ThreadLocal<Long>() {
                                        protected Long initialValue() {
                                            return Thread.currentThread().getId();
                                        }
                                    };

    ThreadLocal<String> stringLocal = new ThreadLocal<String>() {
                                        protected String initialValue() {
                                            return Thread.currentThread().getName();
                                        }
                                    };

    public void set() {
        longLocal.set(Thread.currentThread().getId());
        stringLocal.set(Thread.currentThread().getName());
    }

    public long getLong() {
        return longLocal.get();
    }

    public String getString() {
        return stringLocal.get();
    }

    public static void main(String[] args) throws InterruptedException {
        final TestLocalThread test = new TestLocalThread();
        /**
         * 需要重写的initialValue方法，这样调用get()方法之前可以不使用set()方法
         * 否则会报NP异常
         */
        //test.set();

        System.out.println(test.getLong());
        System.out.println(test.getString());

        Thread thread1 = new Thread() {
            public void run() {
                /**
                 * 需要重写的initialValue方法，这样调用get()方法之前可以不使用set()方法
                 * 否则会报NP异常
                 */
                //test.set();
                System.out.println(test.getLong());
                System.out.println(test.getString());
            }
        };

        thread1.start();
        //wait thread1 to be finished
        // join() 方法主要是让调用改方法的thread完成run方法里面的东西后，在执行join()方法后面的代码
        thread1.join();

        System.out.println(test.getLong());
        System.out.println(test.getString());
    }

}
