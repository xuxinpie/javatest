/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package leetCode;

import java.util.LinkedList;
import java.util.List;

/**
 * leecode求两个集合的交集
 * 求两个集合的交集
 * 
 * @author Xinux
 * @version $Id: CollectionInterSection.java, v 0.1 2016-07-22 2:15 PM Xinux Exp $$
 */
public class CollectionInterSection {

    public static void main(String[] args) {
        int[] num1 = {1,2,2,1};
        int[] num2 = {2,2};

        int[] result = intersection(num1, num2);

        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + " ");
        }
    }

    /**
     * @param nums1 an integer array
     * @param nums2 an integer array
     * @return an integer array
     */
    public static int[] intersection(int[] nums1, int[] nums2) {
        // Write your code here
        List<Integer> tempList = new LinkedList<Integer>();
        for (int i = 0; i < nums1.length; i++) {
            Integer item = nums1[i];
            tempList.add(item);
        }

        List<Integer> resultList = new LinkedList<Integer>();

        for (int i = 0; i < nums2.length; i++) {
            if (tempList.contains(nums2[i])) {
                resultList.add(nums2[i]);
            }
        }

        Integer[] result = new Integer[resultList.size()];
        resultList.toArray(result);
        int[] result2 = new int[result.length];
        for (int i = 0; i < result.length; i++) {
            result2[i] = result[i].intValue();
        }

        return result2;

    }
}