/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 将两个字符数组合并
 * @author hanlin.xx
 * @version $Id: ArrayMerge.java, v 0.1 2015-07-13 16:10 hanlin.xx Exp $$
 */
public class ArrayMerge {

    public static void main(String[] args) {

    }

    public static String[] getArray1() {
        String[] a = { "0", "1", "2" };
        String[] b = { "0", "1", "2" };
        String[] c = new String[a.length + b.length];
        for (int i = 0; i < a.length; i++) {
            c[i] = a[i];
        }
        for (int j = 0; j < b.length; j++) {
            c[j] = b[j];
        }

        return c;
    }

    public static String[] getArray2() {
        String[] a = { "0", "1", "2" };
        String[] b = { "0", "1", "2" };

        List al = Arrays.asList(a);
        List bl = Arrays.asList(b);

        List resultList = new ArrayList();
        resultList.addAll(al);
        resultList.addAll(bl);

        String[] result = (String[]) resultList.toArray();

        return result;
    }

    public static String[] getArray3() {
        String[] a = { "0", "1", "2" };
        String[] b = { "0", "1", "2" };

        String[] c = new String[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);

        return c;
    }

    /**字符数组和整型数组合并*/
    public static String[] getStringArray(int[] al, String[] bl) {
        String[] temp = new String[al.length];

        for (int i = 0; i < al.length; i++) {
            temp[i] = String.valueOf(al[i]);
        }

        String[] c = new String[al.length + bl.length];
        System.arraycopy(temp, 0, c, 0, temp.length);
        System.arraycopy(bl, 0, c, temp.length, bl.length);

        return c;
    }

}
