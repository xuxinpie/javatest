/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package array.listRemoveElement;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hanlin.xx
 * @version $Id: Example.java, v 0.1 2015-07-23 19:50 hanlin.xx Exp $$
 */
public class Example {

	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();

		for (int i = 0; i < 10; i++) {
			list.add(i);
		}

		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i) + " ");
		}

		System.out.println("\n" + "-------------华丽丽的分割线-------------");

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i) % 2 ==0) {
				list.remove(i);
			}
		}
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i) + " ");
		}

	}

}

