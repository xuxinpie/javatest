/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package array.listRemoveElement;


import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author Xinux
 * @version $Id: IteratorRemove.java, v 0.1 2015-12-08 10:55 AM Xinux Exp $$
 */
public class IteratorRemove {
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            list.add(i);
        }

        Iterator<Integer> iterator = list.iterator();
        while(iterator.hasNext()) {
            Integer integer = iterator.next();
            if (integer % 2 == 0) {
                iterator.remove();
            }
        }

        for (Integer integer : list) {
            System.out.println(integer);
        }
    }
}