/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package reference;

/**
 * 存放图片地址的引用
 *
 * @author Xinux
 * @version $Id: Bitmap.java, v 0.1 2016-05-19 10:08 AM Xinux Exp $$
 */
public class Bitmap {

    private String path;

    /**
     * Getter method for property path.
     *
     * @return property value of path
     */
    public String getPath() {
        return path;
    }

    /**
     * Setter method for property path.
     *
     * @param path value to be assigned to property path
     */
    public void setPath(String path) {
        this.path = path;
    }
}