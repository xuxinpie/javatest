/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package reference;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * 软引用和弱引用
 *
 * @author Xinux
 * @version $Id: ReferenceDemo.java, v 0.1 2016-05-19 9:47 AM Xinux Exp $$
 */
public class ReferenceDemo {

    public static void main(String[] args) {
        // 软引用关联着的对象，只有在内存不足的时候JVM才会回收该对象
        SoftReference<String> sr = new SoftReference<String>(new String("Hello"));
        System.out.println(sr.get());

        // 弱引用
        // 当JVM进行垃圾回收时，无论内存是否充足，都会回收被弱引用关联的对象
        WeakReference<String> wr = new WeakReference<String>(new String("World"));
        System.out.println(wr.get());
        System.gc();
        System.out.println(wr.get());

        // 虚引用(用的很少)
        ReferenceQueue<String> queue = new ReferenceQueue<String>();
        PhantomReference<String> pr = new PhantomReference<String>(new String("hello"), queue);
        System.out.println(pr.get());


    }

    /**
     * 假如有一个应用需要读取大量的本地图片，如果每次读取图片都从硬盘读取，
     * 则会严重影响性能，但是如果全部加载到内存当中，又有可能造成内存溢出，
     * 此时使用软引用可以解决这个问题。
     *
     * 设计思路是：用一个HashMap来保存图片的路径 和 相应图片对象关联的软引用之间的映射关系，
     * 在内存不足时，JVM会自动回收这些缓存图片对象所占用的空间，从而有效地避免了OOM的问题
     */
    private Map<String, SoftReference<Bitmap>> imageCache = new HashMap<String, SoftReference<Bitmap>>();

    public void addBitmapToCache(String path) {

        // 强引用的Bitmap对象

        Bitmap bitmap = BitmapFactory.decodeFile(path);

        // 软引用的Bitmap对象

        SoftReference<Bitmap> softBitmap = new SoftReference<Bitmap>(bitmap);

        // 添加该对象到Map中使其缓存

        imageCache.put(path, softBitmap);

    }

    public Bitmap getBitmapByPath(String path) {

        // 从缓存中取软引用的Bitmap对象

        SoftReference<Bitmap> softBitmap = imageCache.get(path);

        // 判断是否存在软引用

        if (softBitmap == null) {

            return null;

        }

        // 取出Bitmap对象，如果由于内存不足Bitmap被回收，将取得空

        Bitmap bitmap = softBitmap.get();

        return bitmap;

    }
}