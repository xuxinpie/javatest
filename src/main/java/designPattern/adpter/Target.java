/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.adpter;

/**
 * 适配接口
 *
 * @author hanlin.xx
 * @version $Id: Target.java, v 0.1 2015-07-16 16:18 hanlin.xx Exp $$
 */
public interface Target {

	public void option();

}

