/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.adpter;

/**
 * 适配器模式
 *
 * @author hanlin.xx
 * @version $Id: Adaptee.java, v 0.1 2015-07-16 15:42 hanlin.xx Exp $$
 */
public class Adaptee {

	public void adapteeOperation() {
		System.out.println("this is a adpatee operation!!");
	}

}

