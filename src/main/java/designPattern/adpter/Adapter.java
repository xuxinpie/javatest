/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.adpter;

/**
 * 适配类
 *
 * @author hanlin.xx
 * @version $Id: Adapter.java, v 0.1 2015-07-16 16:21 hanlin.xx Exp $$
 */
public class Adapter extends Adaptee implements Target {

	//适配的方法
	public void option() {
		this.adapteeOperation();
	}


}

