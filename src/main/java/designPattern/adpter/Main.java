/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.adpter;

/**
 * 适配器模式——运行类
 *
 * @author hanlin.xx
 * @version $Id: Main.java, v 0.1 2015-07-16 16:22 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] ags) {
		Target target = new Adapter();
		target.option();
	}

}

