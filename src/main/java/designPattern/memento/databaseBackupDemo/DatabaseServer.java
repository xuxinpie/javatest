/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.memento.databaseBackupDemo;


/**
 * 数据库系统(发起人角色)
 * @author hanlin.xx
 * @version $Id: DatabaseServer.java, v 0.1 2015-07-17 16:06 hanlin.xx Exp $$
 */
public class DatabaseServer {

	private boolean isUseable;

	/**创建备忘录*/
	public Memento createMemento() {
		return new Memo(isUseable);
	}

	/**备份还原*/
	public boolean restoreMemo(Memento memo) {
		Memo m = (Memo) memo;
		setUseable(m.isUseable);
		return this.isUseable;

	}

	public boolean isUseable() {
		return isUseable;
	}

	public void setUseable(boolean isUseable) {
		this.isUseable = isUseable;
		System.out.println("DB state useable is: " + isUseable);
	}

	/**内部类*/
	private class Memo implements Memento {
		private boolean isUseable;

		public Memo(boolean isUseable) {
			super();
			this.isUseable = isUseable;
		}

		public boolean isUseable() {
			return isUseable;
		}

		public void setUseable(boolean isUseable) {
			this.isUseable = isUseable;
		}

	}

}

