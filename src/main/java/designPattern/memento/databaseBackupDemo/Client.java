/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.memento.databaseBackupDemo;

/**
 * 备忘录模式 —— 测试类
 *
 * 备忘录模式优点是简化了发起人（Originator）类，
 * 它不在需要管理负责备份一个副本在自己内部，当发起人内部状态失效时，可以用外部状态来还原。
 *
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-17 16:30 hanlin.xx Exp $$
 */
public class Client {

	private static DatabaseServer dbServer = new DatabaseServer();

	private static BackupServer backupServer = new BackupServer(dbServer);

	public static void main(String[] args) throws InterruptedException {
		//数据库系统设置于为可用状态
		dbServer.setUseable(true);
		//备份
		backupServer.createMemento();

		//1秒钟备份一次
		Thread.sleep(1000);

		dbServer.setUseable(true);
		backupServer.createMemento();

		Thread.sleep(1000);
		dbServer.setUseable(false);

		System.out.println("-------System Error --------");

		backupServer.retireveMemento();

	}

}

