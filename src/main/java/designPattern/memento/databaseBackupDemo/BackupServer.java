/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.memento.databaseBackupDemo;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author hanlin.xx
 * @version $Id: BackupServer.java, v 0.1 2015-07-17 16:20 hanlin.xx Exp $$
 */
public class BackupServer {

	//增强管理者的功能，把发起人的操作放在这里
	private DatabaseServer dbServer;

	//用Map来设置多点备份
	private Map<Long, Memento> mementos;

	public BackupServer(DatabaseServer databaseServer) {
		super();
		this.dbServer = databaseServer;
		mementos = new ConcurrentHashMap<Long, Memento>();
	}

	/**还原*/
	public void retireveMemento() {
		Iterator<Long> iterator = mementos.keySet().iterator();
		//还原到上一个可用的状态
		while (iterator.hasNext()) {
			Long key = iterator.next();
			Memento memento = mementos.get(key);
			boolean isUseable = dbServer.restoreMemo(memento);
			if (isUseable) {
				break;
			}
		}
	}

	/**备份*/
	public void createMemento() {
		Memento memento = dbServer.createMemento();
		this.mementos.put(new Date().getTime(), memento);
	}



}

