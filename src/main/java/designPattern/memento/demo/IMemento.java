/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.memento.demo;

/**
 * @author hanlin.xx
 * @version $Id: IMemento.java, v 0.1 2015-07-17 16:47 hanlin.xx Exp $$
 */
public interface IMemento {
}

