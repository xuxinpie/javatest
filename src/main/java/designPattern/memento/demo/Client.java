/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.memento.demo;

/**
 * 备忘录模式 —— 测试类
 *
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-17 16:52 hanlin.xx Exp $$
 */
public class Client {

    private static Originator o = new Originator();
    private static Caretaker  c = new Caretaker();

    public static void main(String[] args) {
        //改变发起人的状态
        o.setState("on");
        //创建备忘录对象，并保持于管理保持
        c.saveMemento(o.createMemento());

        //改变发起人的状态
        o.setState("off");
        //还原状态
        o.restoreMemento(c.retrieveMemento());
    }

}
