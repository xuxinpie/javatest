/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.memento.demo;

/**
 * 发起者
 * @author hanlin.xx
 * @version $Id: Originator.java, v 0.1 2015-07-17 16:48 hanlin.xx Exp $$
 */
public class Originator {
    private String state;

    /**
     *描述：创建备忘录
     */
    public IMemento createMemento() {
        return (IMemento) new Memento(state);
    }

    /**
     *描述：还原
     */
    public void restoreMemento(IMemento memento) {
        Memento m = (Memento) memento;
        setState(m.getState());
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
        System.out.println("current state:" + state);
    }

    /**
     *描述：内部类 （设置为private，防止外部对内部类进行操作）
     */
    private class Memento implements IMemento {

        private String state;

        public Memento(String state) {
            this.state = state;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }
    }
}
