/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.memento.demo;

/**
 * 管理者
 * @author hanlin.xx
 * @version $Id: Caretaker.java, v 0.1 2015-07-17 16:50 hanlin.xx Exp $$
 */
public class Caretaker {

    private IMemento memento;

    /**
     *描述：取值 
     */
    public IMemento retrieveMemento() {
        return memento;
    }

    /**
     *描述：设值 
     */
    public void saveMemento(IMemento memento) {
        this.memento = memento;
    }
}
