/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * ������
 * @author hanlin.xx
 * @version $Id: Subject.java, v 0.1 2015-07-14 17:37 hanlin.xx Exp $$
 */
public abstract class Subject {

	List<Observer> observers = new ArrayList<Observer>();

	//add an observer
	public void addObserver(Observer observer) {
		observers.add(observer);
	}

	//remove an observer
	public void removeObserver(Observer observer) {
		observers.remove(observer);
	}

	//通知观察者更新自己的状态
	public void notify(WeatherInfo weatherInfo) {
		for (Observer observer : observers) {
			observer.update(weatherInfo);
		}
	}
}

