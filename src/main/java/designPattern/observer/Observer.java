/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.observer;

/**
 * @author hanlin.xx
 * @version $Id: Observer.java, v 0.1 2015-07-14 17:39 hanlin.xx Exp $$
 */
public abstract class Observer {

	//观察者的回调方法
	abstract void update(WeatherInfo weatherInfo);

}

