/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.observer;

/**
 * �۲���ģʽ
 *
 * ����̨ÿ��������������������Ϣ,
 * С�׵���������ʹ��ӵ��������������������̨��������Ϣ��
 * ������������˱仯��������������ἰʱ����������Ϣ���ù۲���ģʽʵ��
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-14 17:37 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] args) {
		Observer miui = new ConcreteObserver("小米天气");
		Observer hammer = new ConcreteObserver("锤子天气");

		ConcreteSubject subject = new ConcreteSubject();
		subject.addObserver(miui);
		subject.addObserver(hammer);

		//set weather info
		WeatherInfo weatherInfo = new WeatherInfo(38, 204);

		//set weather status and notify observer to update
		subject.setUpdateStatus("1");
		subject.notify(weatherInfo);

		//remove an observer
		subject.removeObserver(miui);

		//change weather info
		weatherInfo.setTemperature(30);
		subject.notify(weatherInfo);

	}


}

