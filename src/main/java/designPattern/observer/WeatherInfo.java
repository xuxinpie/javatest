/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.observer;

/**
 * @author hanlin.xx
 * @version $Id: WeatherInfo.java, v 0.1 2015-07-14 17:39 hanlin.xx Exp $$
 */
public class WeatherInfo {

	//�¶�
	private double temperature;

	//ʪ��
	private double humidity;

	public WeatherInfo(double temperature, double humidity) {
		this.temperature = temperature;
		this.humidity = humidity;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public double getHumidity() {
		return humidity;
	}

	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}

}

