/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.observer;

/**
 * @author hanlin.xx
 * @version $Id: ConcreteObserver.java, v 0.1 2015-07-14 17:56 hanlin.xx Exp $$
 */
public class ConcreteObserver extends Observer {

    private String observerName;

    public ConcreteObserver(String observerName) {
        this.observerName = observerName;
    }

    @Override
    void update(WeatherInfo weatherInfo) {
        System.out.println("观察者:" + observerName + "目前的天气情况是:");
        System.out.println("温度:" + weatherInfo.getTemperature());
        System.out.println("湿度:" + weatherInfo.getHumidity());
    }
}
