/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.observer;

/**
 * @author hanlin.xx
 * @version $Id: ConcreteSubject.java, v 0.1 2015-07-14 17:49 hanlin.xx Exp $$
 */
public class ConcreteSubject extends Subject {

	//状态发生改变的标志位
	private String updateStatus;

	public String getUpdateStatus() {
		return updateStatus;
	}

	public void setUpdateStatus(String updateStatus) {
		this.updateStatus = updateStatus;
	}

	@Override
	public void notify(WeatherInfo weatherInfo) {
		if ("1".equals(updateStatus)) {
			super.notify(weatherInfo);
		}
	}
}

