/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.media;

/**
 * @author hanlin.xx
 * @version $Id: AbstractColleague.java, v 0.1 2015-07-20 11:39 hanlin.xx Exp $$
 */
public abstract class AbstractColleague {

	protected int number;

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public abstract void setNum(int number, AbstractMediator am);
}

