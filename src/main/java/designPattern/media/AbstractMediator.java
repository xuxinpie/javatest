/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.media;

/**
 * @author hanlin.xx
 * @version $Id: AbstractMediator.java, v 0.1 2015-07-20 11:40 hanlin.xx Exp $$
 */
public abstract class AbstractMediator {

    protected AbstractColleague A;

    protected AbstractColleague B;

    public AbstractMediator(AbstractColleague a, AbstractColleague b) {
        this.A = a;
        this.B = b;
    }

    public abstract void AaffectB();

    public abstract void BaffectA();

}
