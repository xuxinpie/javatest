/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.media;

/**
 * @author hanlin.xx
 * @version $Id: ColleagueA.java, v 0.1 2015-07-20 11:43 hanlin.xx Exp $$
 */
public class ColleagueB extends AbstractColleague {


	@Override
	public void setNum(int number, AbstractMediator am) {
		this.number = number;
		am.BaffectA();
	}
}

