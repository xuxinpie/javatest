/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.media;

/**
 * @author hanlin.xx
 * @version $Id: Media.java, v 0.1 2015-07-20 11:45 hanlin.xx Exp $$
 */
public class Media extends AbstractMediator {

	public Media(AbstractColleague a, AbstractColleague b) {
		super(a, b);
	}

	@Override
	public void AaffectB() {
		int number = A.getNumber();
		B.setNumber(number * 100);
	}

	@Override
	public void BaffectA() {
		int number = B.getNumber();
		A.setNumber(number / 100);
	}
}

