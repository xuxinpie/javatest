/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.media;

/**
 * 中介者模式 —— 运行类
 *
 * 将一对的同事类之间的操作封装到一个中介类中，通过中介类来处理对象间的关系
 * 将网状结构转变为星状结构
 *
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-20 11:34 hanlin.xx Exp $$
 */
public class Client {

	public static void main(String[] args) {
		AbstractColleague collA = new ColleagueA();
		AbstractColleague collB = new ColleagueB();

		AbstractMediator am = new Media(collA, collB);

		System.out.println("通过设置A影响B");
		collA.setNum(100, am);

		System.out.println("collA的number值为："+collA.getNumber());
		System.out.println("collB的number值为A的10倍："+collB.getNumber());

		System.out.println("通过设置B影响A");
		collB.setNum(100, am);
		System.out.println("collB的number值为："+collB.getNumber());
		System.out.println("collA的number值为B的0.1倍："+collA.getNumber());
	}

}

