/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.state;

/**
 * @author hanlin.xx
 * @version $Id: YellowState.java, v 0.1 2015-07-17 17:09 hanlin.xx Exp $$
 */
public class YellowState implements State {

	private static final long SLEEP_TIME = 500L;

	public void change(Light light) {
		System.out.println("现在是黄灯，警示");

		//黄灯亮0.5s
		try {
			Thread.sleep(SLEEP_TIME);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		light.setState(new RedState());
	}
}

