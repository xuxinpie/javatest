/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.state;

/**
 * 状态模式 —— 运行类
 *
 * 状态模式将与特定状态相关的行为局部化，并且将不同状态的行为分割开来；
 * 所有状态相关的代码都存在于某个ConcereteState中，所以通过定义新的子类很容易地增加新的状态和转换；
 * 状态模式通过把各种状态转移逻辑分不到State的子类之间，来减少相互间的依赖。
 *
 * 使用条件：
 * 如果你需要管理状态和状态转移，但不想使用大量嵌套的条件语句，那么就是它了
 *
 * @author hanlin.xx
 * @version $Id: Main.java, v 0.1 2015-07-17 17:05 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] args) {
		State state = new GreenState();
		Light light = new Light(state);

		light.work();
	}

}

