/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.state;

/**
 * 抽象状态类
 *
 * @author hanlin.xx
 * @version $Id: State.java, v 0.1 2015-07-17 17:07 hanlin.xx Exp $$
 */
public interface State {

	public void change(Light light);

}

