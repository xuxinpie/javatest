/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.state;

/**
 * @author hanlin.xx
 * @version $Id: Light.java, v 0.1 2015-07-17 17:08 hanlin.xx Exp $$
 */
public class Light {

	private State state;

	public Light(State state) {
		this.state = state;
	}

	public void changeState() {
		state.change(this);
	}

	public void work() {
		while (true) {
			changeState();
		}
	}


	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
}

