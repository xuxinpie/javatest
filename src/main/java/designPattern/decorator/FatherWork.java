/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.decorator;

/**
 * @author hanlin.xx
 * @version $Id: MotherWork.java, v 0.1 2015-07-15 15:26 hanlin.xx Exp $$
 */
public class FatherWork implements Work {

	private Work work;

	public FatherWork(Work work) {
		this.work = work;
	}

	public void paint() {
		System.out.println("爸爸需要为画进行装订");
		work.paint();
		System.out.println("爸爸将画装订好");
	}
}

