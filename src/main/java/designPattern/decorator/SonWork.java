/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.decorator;

/**
 * @author hanlin.xx
 * @version $Id: SonWork.java, v 0.1 2015-07-15 15:25 hanlin.xx Exp $$
 */
public class SonWork implements Work {
	public void paint() {
		System.out.println("儿子画了一幅铅笔画");
	}
}

