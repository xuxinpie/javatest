/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.decorator;

/**
 * @author hanlin.xx
 * @version $Id: Work.java, v 0.1 2015-07-15 15:23 hanlin.xx Exp $$
 */
public interface Work {

	public void paint();

}

