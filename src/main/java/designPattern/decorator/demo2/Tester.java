/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.decorator.demo2;

/**
 * @author hanlin.xx
 * @version $Id: PM.java, v 0.1 2015-07-20 15:10 hanlin.xx Exp $$
 */
public class Tester extends Decorator {

	private Person person;

	public Tester(Person person) {
		this.person = person;
	}

	@Override
	public void work() {
		person.work();
		doFollowWork();

	}

	public void doFollowWork() {
		System.out.println("阿尔法测试");
		System.out.println("贝塔测试");
		System.out.println("预发测试");

	}
}

