/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.decorator.demo2;

/**
 * @author hanlin.xx
 * @version $Id: Person.java, v 0.1 2015-07-20 15:07 hanlin.xx Exp $$
 */
public interface Person {

	public void work();

}

