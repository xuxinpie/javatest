/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.decorator.demo2;

/**
 * 装饰者模式
 *
 * @author hanlin.xx
 * @version $Id: Main.java, v 0.1 2015-07-20 15:15 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] args) {
		Person employee = new Programmer();
		//给程序员赋予产品经理的职责
		employee = new PM(employee);
		//给程序员赋予测试的职责
		employee = new Tester(employee);

		employee.work();
	}

}

