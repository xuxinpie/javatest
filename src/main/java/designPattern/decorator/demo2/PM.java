/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.decorator.demo2;

/**
 * @author hanlin.xx
 * @version $Id: PM.java, v 0.1 2015-07-20 15:10 hanlin.xx Exp $$
 */
public class PM extends Decorator {

	private Person person;

	public PM(Person person) {
		this.person = person;
	}

	@Override
	public void work() {
		doEarlyWork();
		person.work();
	}

	public void doEarlyWork() {
		System.out.println("市场需求分析");
		System.out.println("产品需求分析");

	}
}

