/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.decorator;

/**
 * 装饰者模式 —— 测试类
 *
 * 使用范围：
 * 1.在不影响其他对象的情况下，以动态、透明的方式给单个对象添加职责
 * 2.处理那些可以撤销的职责
 * 3.当不能采用生成子类的方式进行扩充时
 *
 * 实际中用到的场景：java I/O
 *
 * @author hanlin.xx
 * @version $Id: DecoratorTest.java, v 0.1 2015-07-15 15:30 hanlin.xx Exp $$
 */
public class DecoratorTest {

	public static void main(String[] args) {
		Work work = new SonWork();
		//儿子画画
		work.paint();
		System.out.println("\n");

		work = new MotherWork(work);
		work.paint();
		System.out.println("\n");

		work = new FatherWork(work);
		work.paint();
		System.out.println("\n");
	}
}

