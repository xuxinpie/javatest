/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.decorator;

/**
 * @author hanlin.xx
 * @version $Id: MotherWork.java, v 0.1 2015-07-15 15:26 hanlin.xx Exp $$
 */
public class MotherWork implements Work {

	private Work work;

	public MotherWork(Work work) {
		this.work = work;
	}

	public void paint() {
		System.out.println("妈妈需要为画上色");
		work.paint();
		System.out.println("妈妈将画上上颜色");
	}
}

