/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.facade;

/**
 * 门面模式 —— 子系统角色（负责提供业务逻辑）
 *
 * 学生办
 * @author hanlin.xx
 * @version $Id: EnrollmentOffice.java, v 0.1 2015-07-16 19:27 hanlin.xx Exp $$
 */
public class StudentAffairsOffice {

	public void getGoods() {
		System.out.println("领取生活用品");
	}
}

