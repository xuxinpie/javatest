/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.facade;

/**
 * 门面模式 —— 门面角色（委派任务到相应的子系统中去）
 * @author hanlin.xx
 * @version $Id: Facade.java, v 0.1 2015-07-16 19:27 hanlin.xx Exp $$
 */
public class Facade {

	private EnrollmentOffice enrollmentOffice;

	private FinaceSection finaceSection;

	private StudentAffairsOffice studentAffairsOffice;

	//帮助新同学
	public void helpNewComer() {
		enrollmentOffice.register();
		finaceSection.payment();
		studentAffairsOffice.getGoods();
	}

}

