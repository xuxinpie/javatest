/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.facade;

/**
 * 门面模式 —— 运行类
 *
 * 比如：一个CLASS A 还有一个 CLASS B ，你创建一个 CLASS C ，这个CLASS C里 创建的实体
 * 有 CLASS A 和 CLASS B的实体 ，就是创建一个更高一层的实体 来操作 A 和 B
 * @author hanlin.xx
 * @version $Id: Main.java, v 0.1 2015-07-16 19:36 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] args) {
		Facade facade = new Facade();
		facade.helpNewComer();
	}

}

