/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.facade;

/**
 * 门面模式 —— 子系统角色（负责提供业务逻辑）
 *
 * 财务处
 * @author hanlin.xx
 * @version $Id: EnrollmentOffice.java, v 0.1 2015-07-16 19:27 hanlin.xx Exp $$
 */
public class FinaceSection {

	public void payment() {
		System.out.println("交学费");
	}
}

