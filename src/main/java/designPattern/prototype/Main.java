/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.prototype;

/**
 *
 * 原型模式——运行类
 * @author Xinux
 * @version $Id: Main.java, v 0.1 2015-07-16 3:08 PM Xinux Exp $$
 */
public class Main {

	public static void main(String[] args) throws CloneNotSupportedException {
//		//实例化一个张三的对象
//		Person zhangsan = new Person();
//		zhangsan.setName("张三");
//		zhangsan.setAge(11);
//		zhangsan.setGender('男');
//		//通过张三复制出来一个李四的对象
//		Person lisi  = (Person) zhangsan.clone();
//		//修改一下名字
//		lisi.setName("李四");
//		//李四读过一本叫心理学的书
//		lisi.setReadBooks("心理学");
//		//张三的自我介绍
//		zhangsan.sayHello();
//		System.out.println("-----------------");
//		//李四的自我介绍
//		lisi.sayHello();
	}

}