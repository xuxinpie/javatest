/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.prototype;

import java.util.ArrayList;

/**
 * 原型模式 —— 深拷贝
 * @author hanlin.xx
 * @version $Id: deepCopy.java, v 0.1 2015-07-16 16:56 hanlin.xx Exp $$
 */
public class DeepCopy implements Cloneable {

	private ArrayList<String> arrayList;

	public ArrayList<String> getArrayList() {
		return arrayList;
	}

	public DeepCopy() {
		arrayList = new ArrayList<String>();
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		DeepCopy deepCopy = (DeepCopy) super.clone();
		deepCopy.arrayList = (ArrayList<String>) this.arrayList.clone();
		return deepCopy;
	}


	public static void main(String[] args) throws CloneNotSupportedException {
		DeepCopy deepCopy1 = new DeepCopy();
		deepCopy1.getArrayList().add("1");
		DeepCopy deepCopy2 = (DeepCopy) deepCopy1.clone();

		deepCopy2.getArrayList().add("2");
		System.out.println("deepCopy1" + deepCopy1.getArrayList());
		System.out.println("deepCopy2" + deepCopy2.getArrayList());
	}

}

