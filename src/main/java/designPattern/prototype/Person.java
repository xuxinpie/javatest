/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.prototype;

import java.util.ArrayList;

/**
 * @author Xinux
 * @version $Id: Person.java, v 0.1 2015-07-16 3:05 PM Xinux Exp $$
 */
public class Person {

	private String name;

	private int age;

	private char gender;

	private double height;

	private double weight;

	private ArrayList<String> readBooks = new ArrayList<String>();//读过的书

	public Person() {
		System.out.println("构造Person开始");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public ArrayList<String> getReadBooks() {
		return readBooks;
	}

	public void setReadBooks(String readBooks) {
		this.readBooks.add(readBooks);
	}

	public void sayHello() {
		System.out.println("my name :" + name + "--my age: " + age
				+ "--my gender: " + gender + "--读过的书：" + this.readBooks);
	}
	//执行浅拷贝
	@Override
	public Object clone() throws CloneNotSupportedException {
		Person person = null;
		person = (Person) super.clone();
		return person;
	}

	/*public Object clone() throws CloneNotSupportedException {
		Person person = null;
		person = (Person) super.clone();
		person.readBooks = (ArrayList<String>) this.readBooks.clone();//深度克隆
		return person;
	}*/

}