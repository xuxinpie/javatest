/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.prototype;

import java.util.ArrayList;

/**
 * 原型模式 —— 浅拷贝
 * @author hanlin.xx
 * @version $Id: ShallowCopy.java, v 0.1 2015-07-16 16:56 hanlin.xx Exp $$
 */
public class ShallowCopy implements Cloneable {

	private ArrayList<String> arrayList;

	public ArrayList<String> getArrayList() {
		return arrayList;
	}

	public ShallowCopy() {
		arrayList = new ArrayList<String>();
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}


	public static void main(String[] args) throws CloneNotSupportedException {
		ShallowCopy shallowCopy1 = new ShallowCopy();
		shallowCopy1.getArrayList().add("1");
		ShallowCopy shallowCopy2 = (ShallowCopy) shallowCopy1.clone();

		shallowCopy2.getArrayList().add("2");
		System.out.println("shallowCopy1" + shallowCopy1.getArrayList());
		System.out.println("shallowCopy2" + shallowCopy2.getArrayList());
	}

}

