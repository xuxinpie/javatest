/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.command;

/**
 * 抽象的命令角色： 申明执行操作的接口
 * @author hanlin.xx
 * @version $Id: Command.java, v 0.1 2015-07-15 14:19 hanlin.xx Exp $$
 */
public interface Command {

	public void execute(String operation);


}

