/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.command;

import java.util.Scanner;

/**
 * 命令模式 —— 运行类
 * 又称Action模式或者Transaction模式。它属于对象的行为模式。命令模式把一个请求或者操作封装到一个对象中。
 * @author hanlin.xx
 * @version $Id: Main.java, v 0.1 2015-07-15 14:19 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("请输入指令:");

		String operation = scanner.next();
		//申明执行者
		Receiver receiver = new Receiver();
		//申明命令的接收者
		Command command = new ConcreteCommand(receiver);

		//申明调用者
		Invoker invoker = new Invoker();
		invoker.setCommand(command);

		//调用者发出命令
		invoker.runCommand(operation);

	}

}

