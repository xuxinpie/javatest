/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.command;

/**
 * 具体的命令角色
 * 将一个接收者对象绑定到一个动作上。调用接收者相应的操作，以实现Execute方法
 * @author hanlin.xx
 * @version $Id: ConcreteCommand.java, v 0.1 2015-07-15 14:21 hanlin.xx Exp $$
 */
public class ConcreteCommand implements Command {

	private Receiver receiver;

	public ConcreteCommand(Receiver receiver) {
		this.receiver = receiver;
	}

	public void execute(String operation) {
		receiver.action(operation);
	}

	public Receiver getReceiver() {
		return receiver;
	}

	public void setReceiver(Receiver receiver) {
		this.receiver = receiver;
	}
}

