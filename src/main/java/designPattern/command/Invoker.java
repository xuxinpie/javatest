/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.command;

/**
 * 请求者角色
 * 负责调用命令对象的执行请求
 * @author hanlin.xx
 * @version $Id: Invoker.java, v 0.1 2015-07-15 14:29 hanlin.xx Exp $$
 */
public class Invoker {

	private Command command;

	public void runCommand(String operation) {
		command.execute(operation);
	}

	public void setCommand(Command command) {
		this.command = command;
	}
}

