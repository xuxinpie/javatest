/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.command;

/**
 * 接受者角色
 * @author hanlin.xx
 * @version $Id: Receiver.java, v 0.1 2015-07-15 14:22 hanlin.xx Exp $$
 */
public class Receiver {

	//接收者需要执行的动作
	public void action(String operation) {
		//执行不同的业务逻辑
		if ("cmd".equals(operation)) {
			System.out.println("打开终端");
		} else if ("shutdown".equals(operation)) {
			System.out.println("电脑正在关机");
		} else {
			System.out.println("无法识别指令");
		}
	}

}

