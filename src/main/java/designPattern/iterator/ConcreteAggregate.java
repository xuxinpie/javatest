/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * 聚集实现类
 *
 * @author Xinux
 * @version $Id: ConcreteAggregate.java, v 0.1 2015-07-19 9:14 PM Xinux Exp $$
 */
public class ConcreteAggregate<T> implements Aggregate<T> {

	private List<T> list = new ArrayList<T>();

	public void add(T obj) {
		list.add(obj);
	}

	public void remove(T obj) {
		list.remove(obj);
	}

	public Iterator iterator() {
		return new ConcreteIterator<T>(list);
	}
}