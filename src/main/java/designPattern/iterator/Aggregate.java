/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.iterator;

/**
 * 抽象聚集类
 *
 * @author Xinux
 * @version $Id: Aggregate.java, v 0.1 2015-07-19 9:13 PM Xinux Exp $$
 */
public interface Aggregate<T> {

	void add(T obj);

	void remove(T obj);

	Iterator<T> iterator();

}