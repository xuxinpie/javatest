/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * 构造器实现类
 *
 * @author Xinux
 * @version $Id: ConcreteIterator.java, v 0.1 2015-07-19 9:00 PM Xinux Exp $$
 */
public class ConcreteIterator<T> implements Iterator<T> {

	public List<T> list = new ArrayList<T>();

	public int cursor = 0;

	public ConcreteIterator(List<T> list) {
		this.list = list;
	}

	public T next() {
		T obj = null;
		if (this.hasNext()) {
			obj = this.list.get(cursor++);
		}
		return obj;
	}

	public boolean hasNext() {
		if (cursor == list.size()) {
			return false;
		}

		return true;

	}
}