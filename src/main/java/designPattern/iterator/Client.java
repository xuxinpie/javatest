/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.iterator;

/**
 * 迭代器模式 —— 测试类
 *
 * @author Xinux
 * @version $Id: Client.java, v 0.1 2015-07-19 9:19 PM Xinux Exp $$
 */
public class Client {

	public static void main(String[] args) {
		Aggregate<String> aggregate = new ConcreteAggregate<String>();
		aggregate.add("Hello");
		aggregate.add(" ");
		aggregate.add("Xinux");

		Iterator<String> it = aggregate.iterator();

		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}

}