/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.iterator;

/**
 * 抽象构造器
 *
 * @author Xinux
 * @version $Id: Iterator.java, v 0.1 2015-07-19 8:59 PM Xinux Exp $$
 */
public interface Iterator<T> {

	public T next();

	public boolean hasNext();

}