/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.compisite;

/**
 * 定义一个抽象树干类(抽象构建角色)
 * @author hanlin.xx
 * @version $Id: Component.java, v 0.1 2015-07-15 10:49 hanlin.xx Exp $$
 */
public abstract class Component {

	public String name;

	public Component(String name) {
		this.name = name;
	}

	//加入组件
	abstract void add(Component component);

	//删除组件
	abstract void remove(Component component);

	//组件方法
	abstract void operation(Component component);

}

