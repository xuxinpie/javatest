/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.compisite.demo2;

/**
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-20 15:43 hanlin.xx Exp $$
 */
public class Client {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Leader leader1 = new Leader("张三");
        Leader leader2 = new Leader("李四");
        Employee employee1 = new Employee("王五");
        Employee employee2 = new Employee("赵六");
        Employee employee3 = new Employee("陈七");
        Employee employee4 = new Employee("徐八");
        leader1.add(leader2);
        leader1.add(employee1);
        leader1.add(employee2);
        leader2.add(employee3);
        leader2.add(employee4);
        leader1.doSomething();
    }
}
