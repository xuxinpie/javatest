/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.compisite.demo2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author hanlin.xx
 * @version $Id: Employee.java, v 0.1 2015-07-20 15:32 hanlin.xx Exp $$
 */
public class Leader implements Worker {

    private String       name;

    private List<Worker> workers = new ArrayList<Worker>();

    public Leader(String name) {
        super();
        this.name = name;
    }

    public void doSomething() {
        System.out.println(toString());
        Iterator<Worker> it = workers.iterator();
        while (it.hasNext()) {
            it.next().doSomething();
        }
    }

    public void add(Worker worker) {
        workers.add(worker);
    }

    public void remove(Worker worker) {
        workers.remove(worker);
    }

    public Worker getChild(int i) {
        return workers.get(i);
    }

    public String toString() {
        return "My name is " + name + "(领导) " + "有 " + workers.size() + " 个下属";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
