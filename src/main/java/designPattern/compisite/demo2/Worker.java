/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.compisite.demo2;

/**
 * @author hanlin.xx
 * @version $Id: Worker.java, v 0.1 2015-07-20 15:32 hanlin.xx Exp $$
 */
public interface Worker {

	public void doSomething();

}

