/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.compisite.demo2;

/**
 * @author hanlin.xx
 * @version $Id: Employee.java, v 0.1 2015-07-20 15:32 hanlin.xx Exp $$
 */
public class Employee implements Worker {

	private String name;

	public Employee(String name) {
		super();
		this.name = name;
	}

	public void doSomething() {
		System.out.println(toString());
	}


	public String toString() {
		return "My name is " + name + "(普通员工)";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

