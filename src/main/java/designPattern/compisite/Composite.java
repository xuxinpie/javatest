/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.compisite;

import java.util.ArrayList;
import java.util.List;

/**
 * 树枝构建角色——表示分支节点对象
 * @author hanlin.xx
 * @version $Id: Composite.java, v 0.1 2015-07-15 10:53 hanlin.xx Exp $$
 */
public class Composite extends Component {

	private List<Component> components = null;

	public Composite(String name) {
		super(name);
		components = new ArrayList<Component>();
	}

	@Override
	void add(Component component) {
		components.add(component);
	}

	@Override
	void remove(Component component) {
		components.remove(component);
	}

	//实现一个打印功能
	@Override
	void operation(Component component) {
		System.out.println(this.name);
		for (Component c : components) {
			//递归输出每个节点
			System.out.print("\t");
			c.operation(c);
		}
	}
}

