/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.compisite;

/**
 * @author hanlin.xx
 * @version $Id: Leaf.java, v 0.1 2015-07-15 11:08 hanlin.xx Exp $$
 */
public class Leaf extends Component {

	public Leaf(String name) {
		super(name);
	}

	@Override
	void add(Component component) {
		throw new UnsupportedOperationException("叶子角色类不支持这个角色");
	}

	@Override
	void remove(Component component) {
		throw new UnsupportedClassVersionError("叶子角色类不支持这个角色");
	}

	@Override
	void operation(Component component) {
		System.out.println("\t" + this.name);
	}
}

