/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.compisite;

/**
 * 某网刊有体育模块和国内新闻模块，体育模块下有CBA和NBA网站;国内新闻模块有上海地区新闻和江苏地区新闻
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-15 10:43 hanlin.xx Exp $$
 */
public class Client {

    public static void main(String[] args) {

        Component newsRoot = new Composite("新闻");

        Component sportNews = new Composite("体育新闻");

        Component nationalNews = new Composite("国内新闻");

        Component nbaNews = new Leaf("NBA新闻");

        Component cbaNews = new Leaf("CBA新闻");

        Component shanghaiNews = new Leaf("上海");

        Component jiangsuNews = new Leaf("江苏");

        sportNews.add(nbaNews);
        sportNews.add(cbaNews);

        nationalNews.add(shanghaiNews);
        nationalNews.add(jiangsuNews);

        newsRoot.add(sportNews);
        newsRoot.add(nationalNews);

        newsRoot.operation(newsRoot);

    }

}
