/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.singleton;

/**
 * 最简单的单例模式
 * 存在的问题：
 *  1. 不管singleton使不使用都会被创建
 *  2. 尽管构造器被定义为private,但是利用java的反射类仍然是会被实例化
 * @author hanlin.xx
 * @version $Id: HungerSingleton.java, v 0.1 2015-07-13 20:50 hanlin.xx Exp $$
 */
public class HungerSingleton {

    private static final HungerSingleton singleton = new HungerSingleton();

    //构造方法为私有，这样就保证了只能有一个对象存在
    private HungerSingleton() {

    }

    //提供一个外部得到实例的方法
    public static HungerSingleton getInstance() {
        return singleton;
    }

}
