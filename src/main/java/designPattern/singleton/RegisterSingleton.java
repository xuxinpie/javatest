/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.singleton;

import java.util.HashMap;
import java.util.Map;

/**
 * 登记式单例类
 * 类似Spring里面的方法，将类名注册，下次从里面直接获取。
 * @author hanlin.xx
 * @version $Id: RegisterSingleton.java, v 0.1 2015-07-13 21:23 hanlin.xx Exp $$
 */
public class RegisterSingleton {

    private static Map<String, RegisterSingleton> map = new HashMap<String, RegisterSingleton>();

    static {
        RegisterSingleton singleton = new RegisterSingleton();
        map.put(singleton.getClass().getName(), singleton);
    }

    protected RegisterSingleton() {

    }

    //静态工厂方法，返回此类唯一的实例
    public static RegisterSingleton getInstance(String name) {
        if (null == name) {
            name = RegisterSingleton.class.getName();
            System.out.println("name == null " + "---> name = " + name);
        }
        if (null == map.get(name)) {
            try {
                map.put(name, (RegisterSingleton) Class.forName(name).newInstance());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return map.get(name);
    }

}
