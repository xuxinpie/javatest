/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.singleton;

/**
 * 懒汉式单例模式
 * @author hanlin.xx
 * @version $Id: LazySingleton.java, v 0.1 2015-07-13 20:54 hanlin.xx Exp $$
 */
public class LazySingleton {

	//先不进行实例化操作
	private static LazySingleton singleton = null;

	//构造方法声明为私有，这样就保证了只能有一个对象存在
	private LazySingleton() {

	}

	public LazySingleton getInstance() {
		if (null == singleton) {
			singleton = new LazySingleton();
		}

		return singleton;
	}

}

