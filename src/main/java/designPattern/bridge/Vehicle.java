/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.bridge;

/**
 * @author hanlin.xx
 * @version $Id: Vehicle.java, v 0.1 2015-07-17 11:21 hanlin.xx Exp $$
 */
public abstract class Vehicle {

	private Transport transportation;

	//接口和实现相分离，调用Transport的transport()来实现
	public void transport() {
		transportation.transport();
	}

	public Transport getTransportation() {
		return transportation;
	}

	public void setTransportation(Transport transportation) {
		this.transportation = transportation;
	}
}

