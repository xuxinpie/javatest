/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.bridge;

/**
 * @author hanlin.xx
 * @version $Id: Goods.java, v 0.1 2015-07-17 11:31 hanlin.xx Exp $$
 */
public class Guest implements Transport {
	public void transport() {
		System.out.println("运客");
	}
}

