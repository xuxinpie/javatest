/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.bridge;

/**
 * @author hanlin.xx
 * @version $Id: Transport.java, v 0.1 2015-07-17 11:22 hanlin.xx Exp $$
 */
public interface Transport {

	public void transport();

}

