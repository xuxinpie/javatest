/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.bridge;

/**
 *
 * 桥梁模式 —— 运行类
 *
 * 1、分离接口及实现部分  一个实现不必一直绑定在一个接口上；
 * 2、提高可扩充性，使扩展变得简单；
 *
 * @author hanlin.xx
 * @version $Id: Main.java, v 0.1 2015-07-17 11:16 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] args) {
		Car car = new Car();
		//设置不同的实现类
		car.setTransportation(new Goods());
		car.transport();

		car.setTransportation(new Guest());
		car.transport();

	}

}

