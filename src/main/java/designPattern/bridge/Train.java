/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.bridge;

/**
 * @author hanlin.xx
 * @version $Id: Car.java, v 0.1 2015-07-17 11:25 hanlin.xx Exp $$
 */
public class Train extends Vehicle {
	public void transport() {
		System.out.print("火车");
		super.transport();
	}
}

