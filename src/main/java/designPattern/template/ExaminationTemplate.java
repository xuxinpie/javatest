/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.template;

/**
 * 考试模板类
 * @author hanlin.xx
 * @version $Id: ExaminationTemplate.java, v 0.1 2015-07-14 15:37 hanlin.xx Exp $$
 */
public abstract class ExaminationTemplate {

	//钩子
	public boolean hook = false;

	final void templateMethod() {
		if (hook) {
			givePapers();
			teacherSay();
			doPapers();
			handInPaper();
		} else {
			givePapers();
			doPapers();
			handInPaper();
		}
	}

	//抽象方法 延迟到子类中试下

	//提交试卷
	protected abstract void handInPaper();

	//做试卷
	protected abstract void doPapers();

	//老师发话
	protected abstract void teacherSay();

	//分发试卷
	protected abstract void givePapers();

}

