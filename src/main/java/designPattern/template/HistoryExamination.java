/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.template;

/**
 * @author hanlin.xx
 * @version $Id: HistoryExamination.java, v 0.1 2015-07-14 15:42 hanlin.xx Exp $$
 */
public class HistoryExamination extends ExaminationTemplate {
	@Override
	protected void handInPaper() {
		System.out.println("上交历史试卷");
	}

	@Override
	protected void doPapers() {
		System.out.println("做试卷");
	}

	@Override
	protected void teacherSay() {
		System.out.println("老师说：开卷考试");
	}

	@Override
	protected void givePapers() {
		System.out.println("分发历史试卷");
	}
}

