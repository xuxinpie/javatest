/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.template;

/**
 * 模板模式 —— 运行类
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-14 15:49 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] args) {
		//历史试卷模板
		HistoryExamination historyExamination = new HistoryExamination();
		historyExamination.templateMethod();

		//数学试卷模板
		MathExamination mathExamination = new MathExamination();
		//设置钩子，执行不同的逻辑
		mathExamination.hook = true;
		mathExamination.templateMethod();
	}

}

