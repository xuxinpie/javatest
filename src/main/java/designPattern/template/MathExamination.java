/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.template;

/**
 * @author hanlin.xx
 * @version $Id: MathExamination.java, v 0.1 2015-07-14 15:45 hanlin.xx Exp $$
 */
public class MathExamination extends ExaminationTemplate {
    @Override
    protected void handInPaper() {
        System.out.println("上交数学试卷");
    }

    @Override
    protected void doPapers() {
        System.out.println("做数学试卷");
    }

    @Override
    protected void teacherSay() {
        System.out.println("老师说：不许抄袭");
    }

    @Override
    protected void givePapers() {
        System.out.println("分发数学试卷");
    }
}
