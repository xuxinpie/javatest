/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.apperance;

/**
 * 外观模式 —— 运行类
 * 提供了一个统一接口，用来访问子系统中的一群接口。外观定义了一个高层接口，让子系统更容易使用
 *
 * 遵循原则：只和你的密友谈话
 * @author hanlin.xx
 * @version $Id: Main.java, v 0.1 2015-07-16 16:29 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] args) {
		Client client = new Client();
		client.start();
	}

}

