/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.apperance;

/**
 * @author hanlin.xx
 * @version $Id: CPU.java, v 0.1 2015-07-16 16:30 hanlin.xx Exp $$
 */
public class CPU {

	public void execute() {
		System.out.println("CPU is Running");
	}

}

