/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.apperance;

/**
 * @author hanlin.xx
 * @version $Id: KeyBorad.java, v 0.1 2015-07-16 16:38 hanlin.xx Exp $$
 */
public class KeyBorad {

	public void clickButton(String command) {
		if ("start".equals(command)) {
			new CPU().execute();
			new Memory().execute();
			new HardDisk().execute();
		}
	}

}

