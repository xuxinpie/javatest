/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.apperance;

/**
 * 外观模式（又称门面模式）
 *
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-16 16:42 hanlin.xx Exp $$
 */
public class Client {

	private KeyBorad keyBorad = new KeyBorad();

	public void start() {
		keyBorad.clickButton("start");
	}

}

