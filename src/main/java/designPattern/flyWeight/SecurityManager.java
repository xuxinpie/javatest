/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.flyWeight;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Xinux
 * @version $Id: SecurityManager.java, v 0.1 2015-07-19 11:21 PM Xinux Exp $$
 */
public class SecurityManager {

    private static SecurityManager securityManager = new SecurityManager();

    private SecurityManager() {

    }

    public static SecurityManager getInstance() {
        return securityManager;
    }

    /**存放登录人员的权限*/
    private Map<String, Collection<IflyWeight>> map = new HashMap<String, Collection<IflyWeight>>();

    public void logon(String user) {
        Collection<IflyWeight> coll = queryByUser(user);
        map.put(user, coll);
    }

    private Collection<IflyWeight> queryByUser(String user) {
        Collection<IflyWeight> coll = new ArrayList<IflyWeight>();
        for (String s : TestDB.coll) {
            String[] str = s.split(",");
            if (str[0].equals(user)) {
                IflyWeight flyWeight = FlyWeightFactory.getInstance().getFlyWeight(
                    str[1] + "," + str[2]);
                coll.add(flyWeight);
            }
        }

		return coll;
    }

	/**
	 * 判断某个用户对某个安全实体是否有某种权限
	 * @param user
	 * @param securityEntity
	 * @param permit
	 * @return
	 */
	public boolean hasPermit(String user, String securityEntity, String permit) {
		Collection<IflyWeight> coll = map.get(user);
		if (coll == null || coll.isEmpty()) {
			System.out.println(user + " 没有登录或者没有权限");
			return false;
		}

		for (IflyWeight flyWeight : coll) {
			if (flyWeight.match(securityEntity, permit)) {
				return true;
			}
		}

		return false;

	}

}