/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.flyWeight;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * 模拟一个数据库保存员工的信息
 *
 * @author Xinux
 * @version $Id: TestDB.java, v 0.1 2015-07-19 11:05 PM Xinux Exp $$
 */
public class TestDB {

    public static Collection<String> coll = new ArrayList<String>();

    static {
        coll.add("张三,人员列表,查看");
        coll.add("李四,人员列表,查看");
        coll.add("王五,薪资列表,查看");
        coll.add("于六,薪资列表,修改");

        for (short i = 0; i < 3; i++) {
            coll.add("张三" + i + ",人员列表,查看");
        }
    }

}