/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.flyWeight;

/**
 * @author Xinux
 * @version $Id: FlyWeight.java, v 0.1 2015-07-19 11:12 PM Xinux Exp $$
 */
public class FlyWeight implements IflyWeight {

	private String securityEntity;

	private String permit;

	public FlyWeight(String state) {
		String[] strArray = state.split(",");
		this.securityEntity = strArray[0];
		this.permit = strArray[1];
	}

	public boolean match(String securityEntity, String permit) {

		if (this.securityEntity.equals(securityEntity) && this.permit.equals(permit)) {
			return true;
		}

		return false;
	}
}