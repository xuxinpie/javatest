/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.flyWeight;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Xinux
 * @version $Id: FlyWeightFactory.java, v 0.1 2015-07-19 11:15 PM Xinux Exp $$
 */
public class FlyWeightFactory {

	private static FlyWeightFactory factory = new FlyWeightFactory();

	private FlyWeightFactory() {

	}

	public static FlyWeightFactory getInstance() {
		return factory;
	}

	/**存储多个人的IflyWeight对象*/
	private Map<String, IflyWeight> flyMap = new HashMap<String, IflyWeight>();

	/**获取享元单元*/
	public IflyWeight getFlyWeight(String key) {
		IflyWeight flyWeight = flyMap.get(key);

		if (null == flyWeight) {
			flyWeight = new FlyWeight(key);
			flyMap.put(key, flyWeight);
		}

		return flyWeight;
	}

}