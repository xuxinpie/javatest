/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.flyWeight;

/**
 *
 * 享元模式 —— 运行类
 *
 * @author Xinux
 * @version $Id: Main.java, v 0.1 2015-07-19 11:35 PM Xinux Exp $$
 */
public class Main {

    public static void main(String[] agrs) {
        SecurityManager securityManager = SecurityManager.getInstance();

        securityManager.logon("张三");
        securityManager.logon("李四");

        boolean result1 = securityManager.hasPermit("张三", "薪资列表", "查看");
        boolean result2 = securityManager.hasPermit("李四", "薪资列表", "查看");
        boolean result3 = securityManager.hasPermit("张三", "人员列表", "查看");

        System.out.println("result1: " + result1 + " result2: " + result2 + " result3; " + result3);
    }

}