/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.flyWeight;

/**
 * @author Xinux
 * @version $Id: IflyWeight.java, v 0.1 2015-07-19 11:11 PM Xinux Exp $$
 */
public interface IflyWeight {

	public boolean match(String securityEntity, String permit);

}