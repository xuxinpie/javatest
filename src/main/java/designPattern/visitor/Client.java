/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.visitor;

import java.util.ArrayList;
import java.util.List;

/**
 * 访问者模式 —— 运行类
 *
 * 访问者模式(类模式) ：封装一些作用于某种数据结构中的各元素的操作，
 * 它可以在不改变这个数据结构的前提下定义作用于这些元素的新的操作
 *
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-17 19:49 hanlin.xx Exp $$
 */
public class Client {

	public static void main(String[] args) {
		Visitor v1 = new VisitorA();
		List<Visitable> list = new ArrayList<Visitable>();

		list.add(new ConcreteElementA());
		list.add(new ConcreteElementB());

		for (Visitable element : list) {
			element.accept(v1);
		}
	}

}

