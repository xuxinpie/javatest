/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.visitor;

/**
 * 抽象角色元素 —— 申明接受哪一类访问者的访问
 * 申明一个接受操作，接受一个访问者对象作为一个参数
 *
 * @author hanlin.xx
 * @version $Id: Visitable.java, v 0.1 2015-07-17 19:41 hanlin.xx Exp $$
 */
public interface Visitable {

	public void accept(Visitor visitor);

}

