/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.visitor;

/**
 * 元素类 —— 实现抽象元素类所声明的accept方法，通常都是visitor.visit(this)，基本上已经形成一种定式
 * @author hanlin.xx
 * @version $Id: ConcreteElementA.java, v 0.1 2015-07-17 19:43 hanlin.xx Exp $$
 */
public class ConcreteElementA implements Visitable {

	public void accept(Visitor visitor) {
			visitor.visit(this);
	}

	public void operate() {
		System.out.println("ConcreteElementA ...");
	}
}

