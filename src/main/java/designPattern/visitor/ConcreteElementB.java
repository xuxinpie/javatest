/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.visitor;

/**
 * @author hanlin.xx
 * @version $Id: ConcreteElementA.java, v 0.1 2015-07-17 19:43 hanlin.xx Exp $$
 */
public class ConcreteElementB implements Visitable {

	public void accept(Visitor visitor) {
			visitor.visit(this);
	}

	public void operate() {
		System.out.println("ConcreteElementB ...");
	}
}

