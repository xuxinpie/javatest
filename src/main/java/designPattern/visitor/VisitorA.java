/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.visitor;

/**
 * 具体访问者A —— 访问一个类之后该干什么
 * @author hanlin.xx
 * @version $Id: VisitorA.java, v 0.1 2015-07-17 19:48 hanlin.xx Exp $$
 */
public class VisitorA implements Visitor {
	public void visit(ConcreteElementA elementA) {
		elementA.operate();
	}

	public void visit(ConcreteElementB elementB) {
		elementB.operate();
	}
}

