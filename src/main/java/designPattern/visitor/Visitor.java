/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.visitor;


/**
 *
 * 抽象访问者（抽象类或者接口）
 *
 * 声明访问者可以访问哪些元素
 * @author hanlin.xx
 * @version $Id: Visitor.java, v 0.1 2015-07-17 19:39 hanlin.xx Exp $$
 */
public interface Visitor {

	public void visit(ConcreteElementA elementA);

	public void visit(ConcreteElementB elementB);

}

