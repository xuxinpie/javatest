/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.simpleFactory;

/**
 * @author hanlin.xx
 * @version $Id: BMW.java, v 0.1 2015-07-13 21:46 hanlin.xx Exp $$
 */
public class BMW implements Car {
	public void drive() {
		System.out.println("Driving BMW");
	}
}

