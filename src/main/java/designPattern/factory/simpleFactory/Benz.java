/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.simpleFactory;

/**
 * @author hanlin.xx
 * @version $Id: Benz.java, v 0.1 2015-07-13 21:45 hanlin.xx Exp $$
 */
public class Benz implements Car {
	public void drive() {
		System.out.println("Driving Benz");
	}
}

