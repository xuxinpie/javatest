/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.simpleFactory;

/**
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-13 21:44 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] args) {
		CarFactory carFactory = new CarFactory();

		CarStore carStore = new CarStore(carFactory);

		//create a Benz Car
		Car bmw = carStore.orderVehicle("BMW");
		bmw.drive();

		//create a BMW Car
		Car benz = carStore.orderVehicle("Benz");
		benz.drive();
	}

}

