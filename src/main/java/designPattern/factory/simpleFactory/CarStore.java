/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.simpleFactory;

/**
 * 工厂模式 —— 环境类
 * 持用CarFactory
 * @author hanlin.xx
 * @version $Id: CarStore.java, v 0.1 2015-07-13 21:50 hanlin.xx Exp $$
 */
public class CarStore {

	private CarFactory carFactory;

	public CarStore(CarFactory carFactory) {
		this.carFactory = carFactory;
	}

	public Car orderVehicle(String type) {
		return carFactory.createVehicle(type);
	}

}

