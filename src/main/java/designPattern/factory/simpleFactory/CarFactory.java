/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.simpleFactory;

/**
 * @author hanlin.xx
 * @version $Id: CarFactory.java, v 0.1 2015-07-13 21:47 hanlin.xx Exp $$
 */
public class CarFactory {
    public Car createVehicle(String type) {
        Car car = null;
        if ("Benz".equals(type)) {
            car = new Benz();
        } else if ("BMW".equals(type)) {
            car = new BMW();
        }

        return car;
    }
}
