/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.simpleFactory;

/**
 * @author hanlin.xx
 * @version $Id: Car.java, v 0.1 2015-07-13 21:45 hanlin.xx Exp $$
 */
public interface Car {
	public void drive();
}

