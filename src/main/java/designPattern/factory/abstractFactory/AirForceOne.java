/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.abstractFactory;

/**
 * @author hanlin.xx
 * @version $Id: AirForceOne.java, v 0.1 2015-07-14 10:53 hanlin.xx Exp $$
 */
public class AirForceOne implements AirPlane {
	public void fly() {
		System.out.println("AirForce Flying");
	}
}

