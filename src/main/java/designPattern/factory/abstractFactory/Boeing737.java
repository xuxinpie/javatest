/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.abstractFactory;

/**
 * @author hanlin.xx
 * @version $Id: Boeing737.java, v 0.1 2015-07-14 10:56 hanlin.xx Exp $$
 */
public class Boeing737 implements AirPlane {
	public void fly() {
		System.out.println("Boeing737 Flying");
	}
}

