/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.abstractFactory;


/**
 * 抽象工厂
 * 抽象工厂的工厂基类定义了多个虚工厂方法，每个虚工厂方法负责返回一种产品
 * 每个具体工厂生产多个相关基类的各一个派生类
 *
 * @author hanlin.xx
 * @version $Id: AbstractFactory.java, v 0.1 2015-07-14 10:47 hanlin.xx Exp $$
 */
public abstract class AbstractFactory {

	public abstract Car createVehicle();

	public abstract AirPlane createAirPlane();

}

