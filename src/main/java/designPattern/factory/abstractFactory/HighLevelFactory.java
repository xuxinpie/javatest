/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.abstractFactory;

/**
 * @author hanlin.xx
 * @version $Id: HighLevelFactory.java, v 0.1 2015-07-14 11:02 hanlin.xx Exp $$
 */
public class HighLevelFactory extends AbstractFactory {
	@Override
	public Car createVehicle() {
		return new BMW();
	}

	@Override
	public AirPlane createAirPlane() {
		return new AirForceOne();
	}
}

