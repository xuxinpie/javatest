/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.abstractFactory;

/**
 * 工厂模式
 *
 * 通过不同的工厂创建出相应的实例
 *
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-14 11:03 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] args) {
		//实例化一个抽象工厂
		AbstractFactory highlevelFactory = new HighLevelFactory();
		Car bmw = highlevelFactory.createVehicle();
		AirPlane airForceOne = highlevelFactory.createAirPlane();

		bmw.drive();
		airForceOne.fly();

	}

}

