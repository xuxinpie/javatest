/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.abstractFactory;

/**
 * 飞机类接口
 *
 * @author hanlin.xx
 * @version $Id: AirPlane.java, v 0.1 2015-07-14 10:52 hanlin.xx Exp $$
 */
public interface AirPlane {

	public void fly();

}

