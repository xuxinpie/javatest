/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.factory.abstractFactory;

/**
 * @author hanlin.xx
 * @version $Id: LowLevelFactory.java, v 0.1 2015-07-14 10:57 hanlin.xx Exp $$
 */
public class LowLevelFactory extends AbstractFactory {
	@Override
	public Car createVehicle() {
		return new QQ();
	}

	@Override
	public AirPlane createAirPlane() {
		return new Boeing737();
	}
}

