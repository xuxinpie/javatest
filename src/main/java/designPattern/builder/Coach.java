/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.builder;

/**
 * @author Xinux
 * @version $Id: Coach.java, v 0.1 2015-07-16 1:59 PM Xinux Exp $$
 */
public class Coach implements TrafficMachine {

    private Chassis chassis;
    private Tire    tire;
    private Seat    seat;
    private Shell   shell;

    public Chassis getChassis() {
        return chassis;
    }

    public void setChassis(Chassis chassis) {
        this.chassis = chassis;
    }

    public Tire getTire() {
        return tire;
    }

    public void setTire(Tire tire) {
        this.tire = tire;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public Shell getShell() {
        return shell;
    }

    public void setShell(Shell shell) {
        this.shell = shell;
    }
}