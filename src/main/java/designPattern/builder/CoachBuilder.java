/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.builder;

/**
 * @author Xinux
 * @version $Id: CoachBuilder.java, v 0.1 2015-07-16 2:10 PM Xinux Exp $$
 */
public class CoachBuilder implements Builder {

	private Coach coach = new Coach();

	public void buildTire() {
		coach.setTire(new Tire());
	}

	public void buildChassis() {
		coach.setChassis(new Chassis());
	}

	public void buildSeat() {
		coach.setSeat(new Seat());
	}

	public void buildShell() {
		coach.setShell(new Shell());
	}

	public TrafficMachine retrieveResult() {
		return coach;
	}
}