/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.builder;

/**
 * @author Xinux
 * @version $Id: Builder.java, v 0.1 2015-07-16 2:07 PM Xinux Exp $$
 */
public interface Builder {

	/**建造轮胎*/
	public void buildTire();

	/**建造底盘*/
	public void buildChassis();

	/**建造座椅*/
	public void buildSeat();

	/**健在外壳*/
	public void buildShell();

	/**返回产品*/
	public TrafficMachine retrieveResult();

}