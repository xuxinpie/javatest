/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.builder;

/**
 * 建造者模式—— 导演组件
 * 包含一个Builder对象，用来构建对象
 *
 * @author Xinux
 * @version $Id: Director.java, v 0.1 2015-07-16 2:15 PM Xinux Exp $$
 */
public class Director {

	private Builder builder;

	public TrafficMachine construct() {
		builder = new CoachBuilder();

		builder.buildChassis();
		builder.buildSeat();
		builder.buildShell();
		builder.buildTire();

		return builder.retrieveResult();
	}

}