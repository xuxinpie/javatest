/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.proxy.dynamicProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author hanlin.xx
 * @version $Id: ProxyService.java, v 0.1 2015-07-15 16:22 hanlin.xx Exp $$
 */
public class ProxyService implements InvocationHandler {

    private Object target;

    public ProxyService(Object target) {
        this.target = target;
    }


    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        LogUtil.before(target.getClass().getName() + "." + method.getName());

        Object object = method.invoke(target, args);

        LogUtil.after(target.getClass().getName() + "." + method.getName());

        return object;
    }
}
