/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.proxy.dynamicProxy;

import java.lang.reflect.Proxy;

/**
 * @author hanlin.xx
 * @version $Id: Main.java, v 0.1 2015-07-15 16:28 hanlin.xx Exp $$
 */
public class Main {

    public static void main(String[] args) {
        LoginService target = new LoginServiceImpl();

        ProxyService handler = new ProxyService(target);

        LoginService proxy = (LoginService) Proxy.newProxyInstance(target.getClass()
            .getClassLoader(), target.getClass().getInterfaces(), handler);

		proxy.login();
    }
}
