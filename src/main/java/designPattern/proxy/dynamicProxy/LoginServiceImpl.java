/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.proxy.dynamicProxy;

/**
 * @author hanlin.xx
 * @version $Id: LoginServiceImpl.java, v 0.1 2015-07-15 16:21 hanlin.xx Exp $$
 */
public class LoginServiceImpl implements LoginService {
	public void login() {
		System.out.println("校验用户名和密码");
	}
}

