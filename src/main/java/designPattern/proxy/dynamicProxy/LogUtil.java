/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.proxy.dynamicProxy;

/**
 * 日志类
 * @author hanlin.xx
 * @version $Id: LogUtil.java, v 0.1 2015-07-15 16:23 hanlin.xx Exp $$
 */
public class LogUtil {

	//方法被调用之前执行
	public static void before(String methodName) {
		System.out.println("before call: " + methodName);
	}

	//方法被调用之后执行
	public static void after(String methodName) {
		System.out.println("after call: " + methodName);
	}

}

