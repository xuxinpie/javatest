/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.proxy.dynamicProxy;

/**
 * @author hanlin.xx
 * @version $Id: LoginService.java, v 0.1 2015-07-15 16:20 hanlin.xx Exp $$
 */
public interface LoginService {

	public void login();

}

