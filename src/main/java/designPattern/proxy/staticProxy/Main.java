/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.proxy.staticProxy;

/**
 * @author hanlin.xx
 * @version $Id: Main.java, v 0.1 2015-07-15 16:16 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] args) {
		//使用代理类访问被代理类的方法
		Wicket wicket = new ProxyWicket();
		wicket.sell();

	}

}

