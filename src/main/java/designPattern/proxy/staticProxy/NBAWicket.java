/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.proxy.staticProxy;

/**
 * @author hanlin.xx
 * @version $Id: NBAWicket.java, v 0.1 2015-07-15 16:09 hanlin.xx Exp $$
 */
public class NBAWicket implements Wicket {
	public void sell() {
		System.out.println("出售NBA比赛门票");
	}
}

