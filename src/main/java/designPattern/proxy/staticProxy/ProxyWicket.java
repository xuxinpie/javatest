/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.proxy.staticProxy;

/**
 * 代理类
 * @author hanlin.xx
 * @version $Id: ProxyWicket.java, v 0.1 2015-07-15 16:10 hanlin.xx Exp $$
 */
public class ProxyWicket implements Wicket {

	private Wicket wicket;

	/**代理类调用被代理的方法*/
	public void sell() {
		if (null == wicket) {
			wicket = new NBAWicket();
		}

		wicket.sell();
	}

	public void setWicket(Wicket wicket) {
		this.wicket = wicket;
	}
}

