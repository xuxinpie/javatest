/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.interpreter;

import java.util.Stack;

/**
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-20 12:01 hanlin.xx Exp $$
 */
public class Client {

    public static void main(String[] args) {
        String expression = "";
        char[] charArray = expression.toCharArray();
        Context ctx = new Context();
        Stack<Expression> stack = new Stack<Expression>();
        for (int i = 0; i < charArray.length; i++) {
            //进行语法判断，递归调用
        }
        Expression exp = stack.pop();
        exp.interpreter(ctx);
    }

}
