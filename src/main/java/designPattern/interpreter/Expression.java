/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.interpreter;

/**
 * @author hanlin.xx
 * @version $Id: Expression.java, v 0.1 2015-07-20 11:59 hanlin.xx Exp $$
 */
public abstract class Expression {
	public abstract Object interpreter(Context ctx);
}

