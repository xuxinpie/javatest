/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.strategy.demo2;

import java.util.HashMap;
import java.util.Map;

/**
 * 策略工厂 使用单例模式
 *
 * @author hanlin.xx
 * @version $Id: StrategyFactory.java, v 0.1 2015-07-17 14:41 hanlin.xx Exp $$
 */
public class StrategyFactory {

	private static StrategyFactory factory = new StrategyFactory();

	private StrategyFactory() {

	}

	private static Map<Integer, Strategy> strategyMap = new HashMap<Integer, Strategy>();

	static {
		strategyMap.put(RechargeTypeEnum.E_BANK.value(), new EBankStrategy());
		strategyMap.put(RechargeTypeEnum.BUSI_ACCOUNTS.value(), new BusiAcctStrategy());
		strategyMap.put(RechargeTypeEnum.MOBILE.value(), new MobileStrategy());
		strategyMap.put(RechargeTypeEnum.CARD_RECHARGE.value(), new CardStrategy());
	}

	public Strategy creator(Integer type) {
		return strategyMap.get(type);
	}

	public static StrategyFactory getInstance() {
		return factory;
	}



}

