/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.strategy.demo2;

/**
 * @author hanlin.xx
 * @version $Id: EBankStrategy.java, v 0.1 2015-07-17 14:25 hanlin.xx Exp $$
 */
public class EBankStrategy implements Strategy {
	public double calRecharge(Double charge, RechargeTypeEnum type) {
		return charge * 0.85;
	}
}

