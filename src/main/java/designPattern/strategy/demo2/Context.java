/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.strategy.demo2;

/**
 * @author hanlin.xx
 * @version $Id: Context.java, v 0.1 2015-07-17 14:29 hanlin.xx Exp $$
 */
public class Context {

	private Strategy strategy;

	public Double calRecharge(Double charge, Integer type) {
		strategy = StrategyFactory.getInstance().creator(type);
		return strategy.calRecharge(charge, RechargeTypeEnum.valueOf(type));
	}

	public Strategy getStrategy() {
		return strategy;
	}

	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}
}

