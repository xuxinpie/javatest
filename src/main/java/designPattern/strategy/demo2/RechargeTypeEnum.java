/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.strategy.demo2;

/**
 * 支付方式枚举类
 * @author hanlin.xx
 * @version $Id: RechargeTypeEnum.java, v 0.1 2015-07-17 14:16 hanlin.xx Exp $$
 */
public enum RechargeTypeEnum {

	E_BANK(1, "网银"),

	BUSI_ACCOUNTS(2, "商户账号"),

	MOBILE(3,"手机卡充值"),

	CARD_RECHARGE(4,"充值卡");

	//状态值
	private int value;

	//类型描述
	private String description;

	private RechargeTypeEnum(int value, String description) {
		this.value = value;
		this.description = description;
	}

	public int value() {
		return value;
	}

	public String description() {
		return description;
	}

	public static RechargeTypeEnum valueOf(int value) {
		for (RechargeTypeEnum type : RechargeTypeEnum.values()) {
			if (type.value == value) {
				return type;
			}
		}

		return null;
	}



}

