/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.strategy.demo2;

/**
 * 策略模式 —— 运行类
 *
 * 根据请求参数的不同选择不同的算法来执行
 *
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-17 15:07 hanlin.xx Exp $$
 */
public class Client {

	public static void main(String[] args) {
		Context context = new Context();

		//网银充值， 需要付多少
		Double money = context.calRecharge(100D, RechargeTypeEnum.E_BANK.value());
		System.out.println(money);
	}

}

