/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.strategy;

/**
 * 策略模式 —— 环境类
 * 持有一个抽象的策略类
 * @author hanlin.xx
 * @version $Id: Environment.java, v 0.1 2015-07-14 15:09 hanlin.xx Exp $$
 */
public class Environment {

	private Calculate calculate;

	public Environment(Calculate calculate) {
		this.calculate = calculate;
	}

	public void setCalculate(Calculate calculate) {
		this.calculate = calculate;
	}

	//获得一个策略类
	public Calculate getCalculate() {
		return calculate;
	}

	//此处调用不同的算法来执行
	public int calculate(int a, int b) {
		return calculate.calculate(a, b);
	}


}

