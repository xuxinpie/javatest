/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.strategy;

/**
 * 策略模式——一个算法接口
 *
 * @author hanlin.xx
 * @version $Id: Calculate.java, v 0.1 2015-07-14 15:06 hanlin.xx Exp $$
 */
public interface Calculate {

	public int calculate(int a, int b);

}

