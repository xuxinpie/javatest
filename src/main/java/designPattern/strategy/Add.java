/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.strategy;

/**
 * @author hanlin.xx
 * @version $Id: Add.java, v 0.1 2015-07-14 15:07 hanlin.xx Exp $$
 */
public class Add implements Calculate {
	public int calculate(int a, int b) {
		return a + b;
	}
}

