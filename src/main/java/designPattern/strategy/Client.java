/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.strategy;

/**
 * 策略模式把具体的算法封装到了具体策略角色内部，增强了可扩展性，隐蔽了实现细节；
 * 它替代继承来实现，避免了if-else这种不易维护的条件语句
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-14 15:14 hanlin.xx Exp $$
 */
public class Client {

    public static void main(String[] args) {
        Add addOpertion = new Add();
        Environment environment = new Environment(addOpertion);
        System.out.println("the Calculator result is:" + environment.calculate(3, 4));

        Minus minusOperation = new Minus();
        environment.setCalculate(minusOperation);
        System.out.println("the Calculator result is:" + environment.calculate(3, 4));

    }

}
