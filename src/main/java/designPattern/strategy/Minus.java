/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.strategy;

/**
 * @author hanlin.xx
 * @version $Id: Minus.java, v 0.1 2015-07-14 15:08 hanlin.xx Exp $$
 */
public class Minus implements Calculate {
	public int calculate(int a, int b) {
		return a - b;
	}
}

