/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.chainOfReponsibility;

/**
 * 责任链模式——定义一个抽象的处理器
 *
 * @author hanlin.xx
 * @version $Id: Handler.java, v 0.1 2015-07-14 17:11 hanlin.xx Exp $$
 */
public abstract class Handler {

	private Handler nextHandler;

	public Handler getNextHandler() {
		return nextHandler;
	}

	public void setNextHandler(Handler nextHandler) {
		this.nextHandler = nextHandler;
	}

	protected abstract void handleRequest(int bugLevel);
}

