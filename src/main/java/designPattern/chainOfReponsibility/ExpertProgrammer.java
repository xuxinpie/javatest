/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.chainOfReponsibility;

/**
 * @author hanlin.xx
 * @version $Id: JuniorProgrammer.java, v 0.1 2015-07-14 17:16 hanlin.xx Exp $$
 */
public class ExpertProgrammer extends Handler {
    @Override
    protected void handleRequest(int bugLevel) {
        if (3 == bugLevel) {
            System.out.println("bug 等级为高等级，专家程序员可以处理");
        } else {
            if (null != getNextHandler()) {
                getNextHandler().handleRequest(bugLevel);
            } else {
                System.out.println("bug 等级较高，专家程序员也无法处理");
            }

        }
    }
}
