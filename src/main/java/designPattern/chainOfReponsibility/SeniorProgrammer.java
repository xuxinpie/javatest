/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.chainOfReponsibility;

/**
 * @author hanlin.xx
 * @version $Id: JuniorProgrammer.java, v 0.1 2015-07-14 17:16 hanlin.xx Exp $$
 */
public class SeniorProgrammer extends Handler {
	@Override
	protected void handleRequest(int bugLevel) {
		if (2 == bugLevel) {
			System.out.println("bug 等级为中等级，高级程序员可以处理");
		} else {
			System.out.println("bug 等级较高，高级程序员无法处理，交给技术专家");
			getNextHandler().handleRequest(bugLevel);
		}
	}
}

