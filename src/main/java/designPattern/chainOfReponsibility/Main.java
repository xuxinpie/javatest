/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package designPattern.chainOfReponsibility;

/**
 * 责任链模式
 *
 * 测试人员针对于当前测试的系统会提一些bug,当bug等级为低的时候初级程序员就可以修复
 * 等级为中等的时候需要中级程序员来修复，等缓为高级的时候就需要强力大牛来解决，我们
 * 通过责任链来实现这一功能
 * @author hanlin.xx
 * @version $Id: Client.java, v 0.1 2015-07-14 17:15 hanlin.xx Exp $$
 */
public class Main {

	public static void main(String[] args) {
		Handler junior = new JuniorProgrammer();
		Handler senior = new SeniorProgrammer();
		Handler expert = new ExpertProgrammer();

		junior.setNextHandler(senior);
		senior.setNextHandler(expert);

		junior.handleRequest(3);
		junior.handleRequest(5);
	}




}

