/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package redis;

import redis.clients.jedis.Jedis;

import java.util.List;

/**
 * @author Xinux
 * @version $Id: RedisTest.java, v 0.1 2016-05-19 5:14 PM Xinux Exp $$
 */
public class RedisTest {

    public static void main(String[] args) {
        // 连接本地Redis服务
        Jedis jedis = new Jedis("localhost");
        System.out.println("Connection to server successfully!");
        // 查看服务是否运行
        System.out.println("Server is Running: " + jedis.ping());

        //设置 redis 字符串数据
        jedis.set("redis-test", "Hello Redis!");
        // 获取存储的数据并输出
        System.out.println("Stored string in redis:: "+ jedis.get("redis-test"));

        //存储数据到列表中
        jedis.lpush("tutorial-list", "Redis");
        jedis.lpush("tutorial-list", "Mongodb");
        jedis.lpush("tutorial-list", "Mysql");
        // 获取存储的数据并输出
        List<String> list = jedis.lrange("tutorial-list", 0, -1);
        for(int i=0; i<list.size(); i++) {
            System.out.println("Stored string in redis:: "+list.get(i));
        }

    }

}