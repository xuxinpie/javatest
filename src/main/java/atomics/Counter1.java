/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package atomics;

/**
 * 利用synchronized同步代码块来实现计数器功能
 * @author hanlin.xx
 * @version $Id: Counter1.java, v 0.1 2015-06-20 23:54 hanlin.xx Exp $$
 */
public class Counter1 {

	private int counter = 0;

	public int increase() {
		synchronized (this) {
			counter = counter + 1;
			return counter;
		}
	}

	public int decrease() {
		synchronized (this) {
			counter = counter - 1;
			return counter;
		}
	}
	
}

