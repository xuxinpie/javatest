/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package atomics;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 使用atomicInteger实现计数器的功能
 * 内部通过JNI的方式实现了硬件支持的CAS指令
 * @author hanlin.xx
 * @version $Id: Counter2.java, v 0.1 2015-06-20 23:57 hanlin.xx Exp $$
 */
public class Counter2 {

	private AtomicInteger counter = new AtomicInteger();

	public int increase() {
		return counter.incrementAndGet();
	}

	public int decrease() {
		return counter.decrementAndGet();
	}

}

