/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package testString;

import util.StringUtil;

/**
 * @author Xinux
 * @version $Id: subString.java, v 0.1 2016-07-03 9:41 PM Xinux Exp $$
 */
public class subString {

    public static void main(String[] args) {
        final String str = "HelloXinux";
        final String order_id = "20150727110501800000080000000603";
        System.out.println(str.substring(str.length() - 1));
        System.out.println(str.subSequence(0, str.length() - 1));

        System.out.print(StringUtil.substring(order_id, 0, 22) + "22" + StringUtil.right(order_id, 8));
    }

}