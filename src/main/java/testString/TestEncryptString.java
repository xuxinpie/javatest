/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package testString;

/**
 * @author Xinux
 * @version $Id: TestEncryptString.java, v 0.1 2016-06-02 10:55 AM Xinux Exp $$
 */
public class TestEncryptString {

    public static void main(String[] args) {
        String email1 = "conan@163.com";
        String email2 = "conan@navigator.com";
        String email3 = "mm@hotmail.com";
        String email4 = "mmm@navigator.com";

        String phoneNum_HK = "852-26288888";
        String telephoneNum = "18818212770";
        String phoneNum_TW = "952-262626266";

        // 短信显示
        // 邮箱
        System.out.println(EncryptUtil.encryptLogonId(email1, false));
        System.out.println(EncryptUtil.encryptLogonId(email2, false));
        System.out.println(EncryptUtil.encryptLogonId(email3, false));
        System.out.println(EncryptUtil.encryptLogonId(email4, false));

        // 手机号
        System.out.println(EncryptUtil.encryptLogonId(phoneNum_HK, false));
        System.out.println(EncryptUtil.encryptLogonId(telephoneNum, false));
        System.out.println(EncryptUtil.encryptLogonId(phoneNum_TW, false));

        // 主站,手机客户端显示
        // 邮箱
        System.out.println(EncryptUtil.encryptLogonId(email1, true));
        System.out.println(EncryptUtil.encryptLogonId(email2, true));
        System.out.println(EncryptUtil.encryptLogonId(email3, true));
        System.out.println(EncryptUtil.encryptLogonId(email4, true));

        // 手机号
        System.out.println(EncryptUtil.encryptLogonId(phoneNum_HK, true));
        System.out.println(EncryptUtil.encryptLogonId(telephoneNum, true));
        System.out.println(EncryptUtil.encryptLogonId(phoneNum_TW, true));



    }

}