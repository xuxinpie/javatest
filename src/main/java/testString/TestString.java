package testString;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

public class TestString {
	public static void main(String[] args) {
		String s1 = new String("Hello");
		String s2 = new String("Hello");
		String s3 = "Hello";
		String s4 = "Hello";


		System.out.println(s1 == s2);
		System.out.println(s1.equals(s2));
		System.out.println(s1 == s3);
		System.out.println(s1.equals(s3));
		System.out.println(s3 == s4);
		System.out.println(s3.equals(s4));

		try {
			test4();
		} catch (ParseException e) {
			e.printStackTrace();
		}

//		test3();
		
	}

	private static void test1() {
		String s1 = new String("hello");
		System.out.println(s1.lastIndexOf("x"));
	}

	private static void test2() {
		//		int[] datas = new int[] {1,2,3,4,5};   //the result is 1
		Integer[] datas = new Integer[] {1,2,3,4,5};
		List list = Arrays.asList(datas);
		System.out.println(list.size());
	}

	private static void test3() {
		String mobile = "138****0968";
		String mobileNo = mobile.replaceAll("[*]{4}", "*");
		System.out.println(mobileNo);
	}

	private static void test4() throws ParseException {
		String numStr = "-1";
		DecimalFormat df = new DecimalFormat("+#;-#");
		int num = df.parse(numStr).intValue();
		System.out.println(num);
	}
}
 