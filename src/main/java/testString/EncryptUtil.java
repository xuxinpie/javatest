/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package testString;

import org.apache.commons.lang.StringUtils;

/**
 * 敏感信息隐藏工具
 *
 * @author Xinux
 * @version $Id: EncryptUtil.java, v 0.1 2016-06-01 5:25 PM Xinux Exp $$
 */
public class EncryptUtil {

    /**
     * 隐藏用户登录名 —— 根据是在网站,客户端 还是 短信上显示选取不同的策略
     *
     * 1) Email账户
     *
     * 2) 手机账户
     *
     * @param src  用户登录名
     * @param flag true: 用于在网站和客户端显示 false: 用于短信显示
     * @return 隐藏后的用户登录名
     */
    public static String encryptLogonId(String src, boolean flag) {
        String ret = null;
        if (StringUtils.isNotBlank(src)) {
            if (isEmail(src)) {
                //email
                ret = flag ? EmailUtil.getEncryptedEmail(src) : EmailUtil
                    .getEncryptedEmailShowOnMsg(src);
            } else {
                //手机
                ret = flag ? MobileUtil.getEncryptedMobile(src) : MobileUtil
                    .getEncryptedMobileShowOnMsg(src);
            }
        }
        return ret;
    }

    /**
     * 判断是否email格式
     * @param src 输入字符串
     * @return 该字符串是否是email格式
     */
    private static boolean isEmail(String src) {
        return src.indexOf("@") > 0 && src.indexOf(".") > 0
               && src.lastIndexOf(".") > src.indexOf("@");
    }

}