/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package testString;

import org.apache.commons.lang.StringUtils;

/**
 *
 * 手机号脱敏规则工具类
 *
 * @author Xinux
 * @version $Id: MobileUtil.java, v 0.1 2016-06-01 4:54 PM Xinux Exp $$
 */
public final class MobileUtil {

    /** 4个星号  */
    public final static String FOUR_STAR = "****";

    /** 1个星号  */
    public final static String STAR      = "*";

    /**
     * 私有构造函数
     */
    private MobileUtil() {
    }

    /**
     * 有关海外用户手机号的隐藏规则如下（以香港用户为例 852-26288888）：
     * 支付宝手机账号的格式是三部分：1 海外区号 +  2“-“ + 3电话号码
     * 主站会将部分3进行前2位后2位以外 的隐藏 。例如：852-26****88
     *
     * 网站和手机客户端:
     * 香港、澳门：除区号外显示前2位＋****+后2位。如：90****85,判断：4位+8位
     * 台湾：除区号外显示前2位＋****+后3位。如：90****856，判断：4位+9位
     * 大陆：显示前3位 + **** + 后4位。 如：186****7313，判断：11位
     * 其他海外地区：使用缺省隐藏规则
     * 显示前1/3和后1/3，其他用*号代替（短信使用一个*）。内容长度不能被3整除时，
     * 显示前ceil[length/3.0]和后floor[length/3.0]
     *
     *
     * @param mobile 手机号
     * @return 隐藏某些位后的手机号
     */
    public static String getEncryptedMobile(final String mobile) {
        if (mobile == null) {
            return null;
        }
        String mobileNo = null;
        if (mobile.length() == 12 && mobile.indexOf("-") > 0) {
            mobileNo = getHongKongMobile(mobile);
        } else if (mobile.length() == 13 && mobile.indexOf("-") > 0) {
            mobileNo = getTaiWanMobile(mobile);
        } else if (mobile.length() == 11) {
            mobileNo = getMotherLandMobile(mobile);
        } else {
            mobileNo = getDefault(mobile);
        }
        return mobileNo;
    }

    /**
     * 短信: 使用*代替****,减少显示*的个数
     *
     * @param mobile
     * @return
     */
    public static String getEncryptedMobileShowOnMsg(final String mobile) {
        if (null == mobile) {
            return null;
        }
        String mobileNo = getEncryptedMobile(mobile);
        return mobileNo.replaceAll("[*]{4}", STAR);
    }

    /**
     * 香港、澳门：除区号外显示前2位＋****+后2位。如：90****85,判断：12位
     * @param mobile 手机号
     * @return 隐藏某些位后的手机号(香港)
     */
    private static String getHongKongMobile(final String mobile) {
        int index = mobile.indexOf("-") + 1;
        final String areaCode = StringUtils.substring(mobile, 0, index);
        final String begin = StringUtils.substring(mobile, index, index + 2);
        final String after = StringUtils.substring(mobile, -2);
        return areaCode + begin + FOUR_STAR + after;
    }

    /**
     * 大陆：显示前3位 +****+后4位。 如：186****7313，判断：11位
     * @param mobile 手机号
     * @return 隐藏某些位后的手机号(大陆)
     */
    private static String getMotherLandMobile(final String mobile) {
        final String begin = StringUtils.substring(mobile, 0, 3);
        final String after = StringUtils.substring(mobile, -4);
        return begin + FOUR_STAR + after;
    }

    /**
     * 台湾：除区号外显示前2位＋****+后3位。如：90****856，判断：13位
     * @param mobile 手机号
     * @return 隐藏某些位后的手机号(台湾)
     */
    private static String getTaiWanMobile(final String mobile) {
        int index = mobile.indexOf("-") + 1;
        final String areaCode = StringUtils.substring(mobile, 0, index);
        final String begin = StringUtils.substring(mobile, index, index + 2);
        final String after = StringUtils.substring(mobile, -3);
        return areaCode + begin + FOUR_STAR + after;
    }

    /**
     * 其他海外地区：使用缺省隐藏规则
     * 显示前1/3和后1/3，其他用*号代替（短信使用一个*）。内容长度不能被3整除时，
     * 显示前ceil[length/3.0]和后floor[length/3.0]
     * @param mobile 手机号
     * @return 隐藏某些位后的手机号(其他海外地区)
     */
    private static String getDefault(final String mobile) {
        int length = mobile.length();
        int begin = 0;
        int after = 0;
        int startNum = 0;
        int end = 0;

        if (length % 3 == 0) {
            begin = length / 3;
            after = length / 3;
        } else {
            begin = (int) Math.ceil(length / 3.0);
            after = (int) Math.floor(length / 3.0);
        }
        startNum = length - begin - after;
        end = 0 - after;

        String retMobile = StringUtils.substring(mobile, 0, begin);
        for (int i = 0; i < startNum; i++) {
            retMobile = retMobile + STAR;
        }
        retMobile = retMobile + StringUtils.substring(mobile, end);

        return retMobile;
    }

}