/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package testString;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Xinux
 * @version $Id: TestStringFormat.java, v 0.1 2016-06-17 4:46 PM Xinux Exp $$
 */
public class TestStringFormat {

    public static void main(String[] args) {

        TestStringFormat format = new TestStringFormat();

        Map<String, String> map = new HashMap<String, String>();
        map.put("1", "1");
        map.put("2", "2");

        Integer dbNo = 2;
        System.out.println(String.format("%02d", dbNo));
        System.out.println(format.getClass().getName());
        System.out.println(map.toString());
    }

}