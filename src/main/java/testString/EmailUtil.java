/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package testString;

/**
 * Email脱敏规则工具类
 * 
 * @author Xinux
 * @version $Id: EmailUtil.java, v 0.1 2016-06-01 5:32 PM Xinux Exp $$
 */
public final class EmailUtil {

    /** 4个星号  */
    public final static String THREE_STAR = "***";

    /** 1个星号  */
    public final static String STAR       = "*";

    /** 一个@ */
    public final static String AT         = "@";

    /** 一个点号 */
    public final static String DOT        = ".";

    /**
     * 私有构造函数
     */
    private EmailUtil() {
    }

    /**
     * 隐藏email格式
     *
     * @前面的字符大于等于3位,显示前3位,再加上3个*, @后面完整显示如: con***@163.com
     * 如果少于三位,则全部显示,@前面***, 例如tt@163.com则显示为tt***@163.com
     *
     * @param src email
     * @return 隐藏后的email
     */
    public static String getEncryptedEmail(String src) {
        StringBuilder ret = new StringBuilder();
        String prefix = src.substring(0, src.indexOf(AT));
        String endfix = src.substring(src.indexOf(AT));

        if (prefix != null && prefix.length() > 3) {
            ret.append(prefix.substring(0, 3)).append(THREE_STAR);
        } else {
            ret.append(prefix).append(THREE_STAR);
        }
        ret.append(endfix);

        if (ret.length() > 0) {
            return ret.toString();
        } else {
            return null;
        }
    }

    /**
     * 隐藏email格式 --- 短信中显示
     *
     * @前面的字符显示规则:
     * 多于3位,则显示前3位后再加上*
     * 如果少于三位,则全部显示,@前面*
     *
     * @后面字符显示规则
     * 如果@后面第一个字符到第一个"."之前的字符数小于等于7位,则全部显示,并以.*结尾
     * 如果@后面第一个字符到第一个"."之前的字符数大于7位,则显示前7位字符,并以*结尾
     *
     * 示例:
     * 如果账户为 conan@126.com 则显示 con*@126.*
     * 如果账户为 mm@hotmail.com 则显示 mm*@hotmail.*
     * 如果账户为 conan@netvigator.com 则显示 con*@netviga*
     *
     * @param src email
     * @return 隐藏后的email
     */
    public static String getEncryptedEmailShowOnMsg(String src) {
        StringBuilder ret = new StringBuilder();
        String prefix = src.substring(0, src.indexOf(AT));
        String endfix = src.substring(src.indexOf(AT));

        // 处理@前字符串
        if (prefix != null && prefix.length() > 3) {
            ret.append(prefix.substring(0, 3)).append(STAR).append(AT);
        } else {
            ret.append(prefix).append(STAR).append(AT);
        }

        // 处理@后字符串
        String postfix = endfix.substring(1, endfix.indexOf(DOT));
        if (postfix != null && postfix.length() > 7) {
            ret.append(postfix.substring(0, 7)).append(STAR);
        } else {
            ret.append(postfix).append(DOT).append(STAR);
        }

        if (ret.length() > 0) {
            return ret.toString();
        } else {
            return null;
        }
    }
}