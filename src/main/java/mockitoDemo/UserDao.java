/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package mockitoDemo;

/**
 * @author hanlin.xx
 * @version $Id: UserDao.java, v 0.1 2015-06-09 11:11 hanlin.xx Exp $$
 */
public interface UserDao {
	public User findUserById(int userId);
}

