/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package mockitoDemo.asynchronised;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hanlin.xx
 * @version $Id: DummyCaller.java, v 0.1 2015-08-14 10:30 hanlin.xx Exp $$
 */
public class DummyCaller implements DummyCallback {

	//proxy class of asynchronous operation
	private final DummyCollaborator dummyCollaborator;

	//execute result
	private List<String> result = new ArrayList<String>();

	public DummyCaller(DummyCollaborator dummyCollaborator) {
		this.dummyCollaborator = dummyCollaborator;
	}

	public void doSomethingAsynchronously() {
		dummyCollaborator.doSomethingAsynchronously(this);
	}

	public List<String> getResult() {
		return result;
	}

	public void onSuccess(List<String> result) {
		this.result = result;
		System.out.println("Success");
	}

	public void onFail(int code) {
		System.out.println("Fail");
	}
}

