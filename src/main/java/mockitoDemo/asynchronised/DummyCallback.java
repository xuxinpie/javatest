/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package mockitoDemo.asynchronised;

import java.util.List;

/**
 * @author hanlin.xx
 * @version $Id: DummyCallback.java, v 0.1 2015-08-14 10:29 hanlin.xx Exp $$
 */
public interface DummyCallback {

	public void onSuccess(List<String> result);

	public void onFail(int code);

}

