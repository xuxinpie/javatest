/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package mockitoDemo.asynchronised;

import java.util.Collections;

/**
 *
 * 异步执行操作的代理类
 *
 * @author hanlin.xx
 * @version $Id: DummyCollaborator.java, v 0.1 2015-08-14 10:32 hanlin.xx Exp $$
 */
public class DummyCollaborator {

	public static final int ERROR_CODE = 1;

	public DummyCollaborator() {
	}

	public void doSomethingAsynchronously (final DummyCallback callback) {
		new Thread(new Runnable() {
			public void run() {
				try {
					Thread.sleep(5000);
					callback.onSuccess(Collections.EMPTY_LIST);
				} catch (InterruptedException e) {
					callback.onFail(ERROR_CODE);
					e.printStackTrace();
				}
			}
		}).start();
	}
}

