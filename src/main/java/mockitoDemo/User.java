/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package mockitoDemo;

/**
 * @author hanlin.xx
 * @version $Id: User.java, v 0.1 2015-06-09 11:08 hanlin.xx Exp $$
 */
public class User {
	//用户ID
	private final int userId;
	//用户姓名
	private final String userName;

	public String getUserName() {
		return userName;
	}

	public User(int userId, String userName) {
		super();
		this.userId = userId;
		this.userName = userName;
	}

}

