/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package mockitoDemo;

/**
 * @author hanlin.xx
 * @version $Id: UserService.java, v 0.1 2015-06-09 11:12 hanlin.xx Exp $$
 */
public class UserService {

	private UserDao userDao;

	public UserService(UserDao userDao) {
		super();
		this.userDao = userDao;
	}

	public User findUser(int userId) {
		User user = userDao.findUserById(userId);
		return user;
	}

}

