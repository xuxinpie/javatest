/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package stopWatch;

import org.apache.commons.lang.time.StopWatch;

/**
 * @author hanlin.xx
 * @version $Id: test.java, v 0.1 2015-07-22 16:08 hanlin.xx Exp $$
 */
public class test {

    public static void main(String[] args) {
		try {
			test01();
//			test02();
//			test03();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 调用split()方法记录持续时间，时间记录不停止
	 * @throws InterruptedException
	 */
    public static void test01() throws InterruptedException {
        StopWatch watch = new StopWatch();
        watch.start();
        Thread.sleep(1000);
        watch.split();
        /** 
         * This is the time between start and latest split. 
         * 调用start()方法到最后一次调用split()方法耗用的时间 
         */
        System.out.println(watch.getSplitTime());
        Thread.sleep(1000);
        watch.split();
        System.out.println(watch.getSplitTime());
        Thread.sleep(500);
        watch.stop();
        /** 
         * This is either the time between the start and the moment this method 
         * is called, or the amount of time between start and stop 
         * 调用start()方法到调用getTime()或stop()方法耗用的时间 
         */
        System.out.println(watch.getTime());

    }

	/**
	 * 调用reset方法，时间归零
	 * @throws InterruptedException
	 */
    private static void test02() throws InterruptedException {
        StopWatch watch = new StopWatch();
        watch.start();
        Thread.sleep(1000);
        System.out.println(watch.getTime());
        /** 复位 归零 */
        watch.reset();
        watch.start();
        Thread.sleep(1000);
        System.out.println(watch.getTime());
    }

	/**
	 * suspend()暂停计时
	 * @throws InterruptedException
	 */
    private static void test03() throws InterruptedException {
        StopWatch watch = new StopWatch();
        watch.start();
        Thread.sleep(1000);
        System.out.println(watch.getTime());
        /** 暂停 */
        watch.suspend();
        System.out.println("do something");
        Thread.sleep(500);
        /** 恢复 */
        watch.resume();
        Thread.sleep(2000);
        System.out.println(watch.getTime());
    }

}
