/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package thistest;

/**
 *
 * 测试this的用法，通过this调用本类中别的构造函数
 * 值得注意的是：
 * 1：在构造调用另一个构造函数，调用动作必须置于最起始的位置。
 * 2：不能在构造函数以外的任何函数内调用构造函数。
 * 3：在一个构造函数内只能调用一个构造
 * @author Xinux
 * @version $Id: UserInfo.java, v 0.1 2015-06-14 11:19 PM Xinux Exp $$
 */
public class UserInfo {

	private String userId;

	private int age;

	private String email;

	UserInfo(String userId) {
		this.userId = userId;
		System.out.println("userId: " + userId);
	}

	UserInfo(String userId, int age) {
		this(userId);
		this.age = age;
		System.out.println("age: " + age);
	}

	UserInfo(String userId, int age, String email) {
		this(userId, age);
		this.email = email;
		System.out.println("email: " + email);
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}