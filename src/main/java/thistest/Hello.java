/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package thistest;


/**
 * @author Xinux
 * @version $Id: Hello.java, v 0.1 2015-06-14 11:10 PM Xinux Exp $$
 */
public class Hello {

	int i = 1;

	public Hello() {
		Thread thread = new Thread() {
			public void run() {
				for (int j = 0; j < 20; j++) {
					Hello.this.run();
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};

		thread.start();

	}

	public void run() {
		System.out.println("i = " + i);
		i++;
	}

	public static void main(String[] args) throws Exception {
		new Hello();
	}
}