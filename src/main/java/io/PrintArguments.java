/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package io;

import java.io.File;

/**
 * 测试File.getParentFile和File.getParent
 *
 * @author hanlin.xx
 * @version $Id: PrintArguments.java, v 0.1 2015-08-02 18:40 hanlin.xx Exp $$
 */
public class PrintArguments {

    public static void main(String[] args) {
        File file = new File("aaa\\test.class");
        System.out.print(file.exists());

        System.out.println(file.getAbsolutePath());
        System.out.println(file.getParent());
        System.out.println(file.getName());

        File file2 = file.getParentFile();
        System.out.println(file2.getAbsolutePath());
        if (null == file2) {
            System.out.println("null");
        }


    }
}
