/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package io;

import java.io.*;

/**
 * 字符流的输入和输出
 *
 * @author hanlin.xx
 * @version $Id: byteStream.java, v 0.1 2015-07-20 16:59 hanlin.xx Exp $$
 */
public class byteStream2 {

    public static void main(String[] args) throws IOException {
        //采用非Buffered缓冲区
        FileInputStream inputStream = null;

        FileOutputStream outputStream = null;

        try {
            inputStream = new FileInputStream(
                    "D:\\Myproject\\gitRepository\\javaTest\\src\\main\\resources\\picture\\transformers.jpg");
            outputStream = new FileOutputStream(
                    "D:\\Myproject\\gitRepository\\javaTest\\src\\main\\resources\\picture\\copy.jpg");

            byte[] buf = new byte[1024];

            int len = 0;

            while (-1 != (len = inputStream.read(buf))) {
                outputStream.write(buf, 0, len);
            }

        } catch (IOException e) {
            throw new RuntimeException("复制文件失败");
        } finally {
            try {
                if (null != inputStream) {
                    inputStream.close();
                }
            } catch (IOException e) {
                throw new RuntimeException("读取关闭失败");
            }

            try {
                if (null != outputStream) {
                    outputStream.close();
                }
            } catch (IOException e) {
                throw new RuntimeException("写入关闭失败");
            }
        }

    }
}
