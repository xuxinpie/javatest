/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package io;

import java.io.*;
import java.util.concurrent.TimeUnit;

/**
 * 字符流的输入与输出
 *
 * @author hanlin.xx
 * @version $Id: charStream.java, v 0.1 2015-07-20 16:33 hanlin.xx Exp $$
 */
public class charStream {

	public static void main(String[] agrs) {
		BufferedReader bufferedReader = null;
		BufferedWriter bufferedWriter = null;

		try {
			bufferedReader = new BufferedReader(new FileReader("D:\\Myproject\\gitRepository\\javaTest\\src\\main\\resources\\Text1.txt"));
			bufferedWriter = new BufferedWriter(new FileWriter("D:\\Myproject\\gitRepository\\javaTest\\src\\main\\resources\\Text2.txt"));

			String line = null;

			Long startTime = System.currentTimeMillis();

			while ((line = bufferedReader.readLine()) != null) {
				bufferedWriter.write(line);
				//换行
				bufferedWriter.newLine();

				bufferedWriter.flush();
			}

//			Long consumeTime = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime);

			Long consumeTime = System.currentTimeMillis() - startTime;

			System.out.println("耗时：" + consumeTime + " ms");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("读写失败");
		} finally {
			try {
				if (null != bufferedReader) {
					bufferedReader.close();
				}
			} catch (IOException e) {
				throw new RuntimeException("读写关闭失败");
			}

			try {
				if (null != bufferedWriter) {
					bufferedWriter.close();
				}
			} catch (IOException e) {
				throw new RuntimeException("写入关闭失败");
			}
		}
	}

}

