package io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Xinux on 10/19/14.
 */
public class AddFile {
	public static void main(String[] args) throws IOException {
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter("testFile.txt", true));
			out.write("Hello world!");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

}
