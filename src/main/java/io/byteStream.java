/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package io;

import java.io.*;

/**
 * 字符流的输入和输出
 *
 * @author hanlin.xx
 * @version $Id: byteStream.java, v 0.1 2015-07-20 16:59 hanlin.xx Exp $$
 */
public class byteStream {

    public static void main(String[] args) throws IOException {
        //采用Buffered缓冲区
        BufferedInputStream inputStream = null;

        BufferedOutputStream outputStream = null;

        try {
            inputStream = new BufferedInputStream(
                new FileInputStream(
                    "D:\\Myproject\\gitRepository\\javaTest\\src\\main\\resources\\picture\\transformers.jpg"));
            outputStream = new BufferedOutputStream(new FileOutputStream(
                "D:\\Myproject\\gitRepository\\javaTest\\src\\main\\resources\\picture\\copy.jpg"));

            int tempByte = 0;

            while (-1 != (tempByte = inputStream.read())) {
                outputStream.write(tempByte);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
			outputStream.close();
			inputStream.close();
        }

    }
}
