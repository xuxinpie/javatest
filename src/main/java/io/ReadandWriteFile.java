package io;

import java.io.*;

/**
 * 实现读取和写入到文本文档的功能
 * Created by Xinux on 10/19/14.
 */
public class ReadandWriteFile {
	public static void main(String[] args) {
		String pathName = "/Users/Xinux/helloXinux/MyProject/gitReposity/testReposity/src/com/Util/input.txt";
		File filename = new File(pathName);
		//判断input.txt是否存在，如果不存在的话，需要先创建一个,写入对应的文本
		if(!filename.exists()) {
			try {
				filename.createNewFile();
				BufferedWriter out = new BufferedWriter(new FileWriter(filename));
				out.write("Hey, I am a handsome boy! \r\n");
				out.flush();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			InputStreamReader reader = new InputStreamReader(new FileInputStream(filename));
			BufferedReader br = new BufferedReader(reader);  // 建立一个对象，把文件内容转成计算机能懂的语言
			StringBuffer message = new StringBuffer();
			String line = null;
			while((line = br.readLine()) != null) {
				message.append(line);
			}
			System.out.println(message.toString());

			File writeName = new File("/Users/Xinux/helloXinux/MyProject/gitReposity/testReposity/src/com/Util/output.txt");
			writeName.createNewFile();
			BufferedWriter out = new BufferedWriter(new FileWriter(writeName));
			out.write("You are so funny! \r\n");
			out.flush();
			out.close();

		} catch (FileNotFoundException e) {
			System.out.println("can not find the txt file!");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
