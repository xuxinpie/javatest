package util.thirdParty;

import org.dom4j.DocumentException;
import org.junit.Test;

/**
 * Created by Xinux on 11/17/16.
 */
public class AlipayUtilsTest {

@Test
public void testUrlParams2Map() throws Exception {
    String url = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<alipay>\n" +
            "   <is_success>T</is_success>\n" +
            "   <request>\n" +
            "       <param name=\"trade_no\">2010073000030344</param>\n" +
            "       <param name=\"service\">single_trade_query</param>\n" +
            "       <param name=\"partner\">2088002007018916</param>\n" +
            "   </request>\n" +
            "   <response>\n" +
            "       <trade>\n" +
            "           <body>合同催款通知</body>\n" +
            "           <buyer_email>ltrade008@alitest.com</buyer_email>\n" +
            "           <buyer_id>2088102002723445</buyer_id>\n" +
            "           <discount>0.00</discount>\n" +
            "           <gmt_last_modified_time>2010-07-30 12:30:29 </gmt_last_modified_time>\n" +
            "           <out_trade_no>1280463992953</out_trade_no>\n" +
            "           <payment_type>1</payment_type>\n" +
            "           <price>1.00</price>\n" +
            "       </trade>\n" +
            "   </response>\n" +
            "   <sign>56ae9c3286886f76e57e0993625c71fe</sign>\n" +
            "   <sign_type>MD5</sign_type>\n" +
            "</alipay>";

    AlipayApiCallResult callResult = new AlipayApiCallResult();

    try {
        callResult = AlipayUtils.orderQueryURLParams2Map(url);
    } catch (DocumentException e) {
        e.printStackTrace();
    }

    System.out.print(callResult.toString());


}
}