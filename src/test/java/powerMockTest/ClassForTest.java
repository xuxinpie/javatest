/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest;

import java.io.File;

/**
 * 创建一个类,供PowerMock来进行测试
 *
 * @author Xinux
 * @version $Id: ClassForTest.java, v 0.1 2016-09-13 11:18 AM Xinux Exp $$
 */
public class ClassForTest {

    public boolean callArgumentInstance(File file) {
        return file.exists();
    }

    public boolean callInternalInstance(String path) {
        File file = new File(path);
        return file.exists();
    }

    public boolean callFinalMethod(ClassDependency refer) {
        return refer.isAlive();

    }

    public boolean callStaticMethod() {
        return ClassDependency.isExist();
    }

    public boolean callPrivateMethod() {
        return isExist();
    }

    public boolean callSystemFinalMethod(String str) {
        return str.isEmpty();
    }

    public String callSystemStaticMethod(String str) {
        return System.getProperty(str);
    }

    // void method
    public void voidMethod() {
        // do nothing
        System.out.println("execute void method");
    }

    // private method
    private boolean isExist() {
        System.out.println("execute private method");
        return false;
    }

}