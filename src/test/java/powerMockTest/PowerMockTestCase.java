/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest;

import java.io.File;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * @RunWith(PowerMockRunner.class)语句告诉JUnit用PowerMockRunner来执行测试。
 * @PrepareForTest(Employee.class)语句告诉PowerMock准备Employee类进行测试
 * 适用于模拟final类或有final, private, static, native方法的类。
 *
 * @author Xinux
 * @version $Id: PowerMocklogAndDiscardPolicy.java, v 0.1 2016-09-13 11:19 AM Xinux Exp $$
 */
@RunWith(PowerMockRunner.class)
public class PowerMockTestCase {

    @InjectMocks
    private ClassForTest classForTest = new ClassForTest();

    @Before
    public void setUp() {
    }


    /**
     * 普通Mock： Mock参数传递的对象
     *
     * 普通Mock不需要加@RunWith和@PrepareForTest注解
     */
    @Test
    public void testCallArgumentInstance() {
        File file = PowerMockito.mock(File.class);


        PowerMockito.when(file.exists()).thenReturn(true);
        Assert.assertTrue(classForTest.callArgumentInstance(file));
    }

    /**
     * Mock方法内部new出来的对象
     *
     * PowerMockito.whenNew方法时,必须加注解@PrepareForTest和@RunWith
     * 注解@PrepareForTest里写的类是需要mock的new对象代码所在的类
     * @throws Exception
     */
    @Test
    @PrepareForTest(ClassForTest.class)
    public void testCallInternalInstance() throws Exception {
        File file = PowerMockito.mock(File.class);
        ClassForTest classForTest = new ClassForTest();

//        PowerMockito.whenNew(File.class).withAnyArguments().thenReturn(file);
        PowerMockito.whenNew(File.class).withArguments("bbb").thenReturn(file);
        PowerMockito.when(file.exists()).thenReturn(true);
        Assert.assertTrue(classForTest.callInternalInstance("bbb"));
    }

    /**
     * Mock普通对象的final方法
     *
     * 当需要mock final方法的时候，必须加注解@PrepareForTest和@RunWith
     * 注解@PrepareForTest里写的类是final方法所在的类。
     */
    @Test
    @PrepareForTest(ClassDependency.class)
    public void testCallFinalMethod() {
        ClassDependency dependency = PowerMockito.mock(ClassDependency.class);
        ClassForTest classForTest = new ClassForTest();

        PowerMockito.when(dependency.isAlive()).thenReturn(true);

        Assert.assertTrue(classForTest.callFinalMethod(dependency));
    }

    /**
     * Mock普通类的静态方法
     *
     * 当需要mock静态方法的时候，必须加注解@PrepareForTest和@RunWith
     * 注解@PrepareForTest里写的类是静态方法所在的类。同时使用mockStatic方法
     */
    @Test
    @PrepareForTest(ClassDependency.class)
    public void testCallStaticMethod() {
        PowerMockito.mockStatic(ClassDependency.class);
        ClassForTest classForTest = new ClassForTest();

        PowerMockito.when(ClassDependency.isExist()).thenReturn(true);

        Assert.assertTrue(classForTest.callStaticMethod());

        // 如果需要验证被Mock的方法是否被调用
        // 则需要调用PowerMockito.verifyStatic()方法，紧随其后的是被mock的方法
        PowerMockito.verifyStatic();
        ClassDependency.isExist();
    }

    /**
     *  Mock 私有方法
     *
     * 和Mock普通方法一样，只是需要加注解@PrepareForTest(ClassUnderTest.class)
     * 注解里写的类是私有方法所在的类。
     * @throws Exception
     */
    @Test
    @PrepareForTest(ClassForTest.class)
    public void testCallPrivateMethod() throws Exception {
        ClassForTest classForTest = PowerMockito.mock(ClassForTest.class);
        PowerMockito.when(classForTest.callPrivateMethod()).thenCallRealMethod();
        PowerMockito.when(classForTest, "isExist").thenReturn(true);

        Assert.assertTrue(classForTest.callPrivateMethod());
    }

    /**
     * Mock系统类的静态和final方法
     *
     * 和Mock普通对象的静态方法、final方法一样
     * 只不过注解@PrepareForTest里写的类不一样 ，注解里写的类是需要调用系统方法所在的类。
     */
    @Test
    @PrepareForTest(ClassForTest.class)
    public void testCallSystemStaticMethod() {
        ClassForTest classForTest = new ClassForTest();

        PowerMockito.mockStatic(System.class);

        PowerMockito.when(System.getProperty("aaa")).thenReturn("bbb");

        Assert.assertEquals("bbb", classForTest.callSystemStaticMethod("aaa"));
    }

    /**
     * spy方法需要使用doReturn方法才不会调用实际方法,使用thenReturn会调用实际的方法
     */
    @Test
    public void testVoidMethod() {

        ClassForTest classForTest = PowerMockito.spy(new ClassForTest());
        PowerMockito.doNothing().when(classForTest).voidMethod();

        classForTest.voidMethod();

    }



}