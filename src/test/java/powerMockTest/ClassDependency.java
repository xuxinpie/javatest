/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest;

/**
 * @author Xinux
 * @version $Id: ClassDependency.java, v 0.1 2016-09-13 11:37 AM Xinux Exp $$
 */
public class ClassDependency {

    // final 方法
    public final boolean isAlive() {
        // do something
        System.out.println("Class dependency final method be called");
        return false;

    }

    // static 方法
    public static boolean isExist() {
        // do something
        System.out.println("Class dependency static method be called");
        return false;

    }

}