/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.employee;

/**
 * @author Xinux
 * @version $Id: ExistedEmployeeException.java, v 0.1 2016-09-13 3:26 PM Xinux Exp $$
 */
public class ExistedEmployeeException extends RuntimeException {

    /** 序列号 */
    private static final long serialVersionUID = -2054787630532947224L;

    /**
     * 默认构造方法
     */
    public ExistedEmployeeException() {
    }

    /**
     * 构造方法
     * @param message
     */
    public ExistedEmployeeException(String message) {
        super(message);
    }

    /**
     * 构造方法
     * @param message
     * @param cause
     */
    public ExistedEmployeeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构造方法
     * @param cause
     */
    public ExistedEmployeeException(Throwable cause) {
        super(cause);
    }
}