/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.employee;

import java.util.List;

/**
 * @author Xinux
 * @version $Id: EmployeeRepository.java, v 0.1 2016-09-13 3:24 PM Xinux Exp $$
 */
public class EmployeeRepository {
    private EmployeeTableUtil tableUtil;

    public int count() {
        return new EmployeeTableUtil().count();
    }

    public List<Employee> findAll() {
        return EmployeeTableUtil.findAll();
    }

    public boolean insert(Employee employee) {
        try {
            tableUtil.insert(employee);
            return true;
        } catch (ExistedEmployeeException e) {
            return false;
        }
    }

    public boolean update(Employee employee) {
        try {
            EmployeeTableUtil.update(employee);
            return true;
        } catch (NullEmployeeException e) {
            return false;
        }
    }

    public boolean delete(Employee employee) {
        return tableUtil.delete(employee);
    }

    private double bonus(Employee employee) {
        return employee.getSalary() * 0.1d;
    }

    public void setTableUtil(EmployeeTableUtil tableUtil) {
        this.tableUtil = tableUtil;
    }
}