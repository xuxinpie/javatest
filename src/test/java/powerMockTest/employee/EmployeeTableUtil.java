/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.employee;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Xinux
 * @version $Id: EmployeeTableUtil.java, v 0.1 2016-09-13 3:17 PM Xinux Exp $$
 */
public class EmployeeTableUtil {
    public int count() {
        return 0;
    }

    public static final List<Employee> findAll() {
        return new ArrayList<Employee>();
    }

    public void insert(Employee employee) {
        if (existed(employee.getId())) {
            throw new ExistedEmployeeException();
        }

        //insert employee
    }

    public static final void update(Employee employee) {
        if (employee == null) {
            throw new NullEmployeeException();
        }
    }

    public boolean delete(Employee employee) {
        if (existed(employee.getId())) {
            //delete employee
            return true;
        }
        return false;
    }

    private boolean existed(String id) {
        return true;
    }
}