/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.employee;

/**
 * @author Xinux
 * @version $Id: TestCase.java, v 0.1 2016-09-13 4:40 PM Xinux Exp $$
 */
public class NullEmployeeException extends RuntimeException {

    /**
     * 默认构造方法
     */
    public NullEmployeeException() {
    }

    /**
     * 构造方法
     * @param message
     */
    public NullEmployeeException(String message) {
        super(message);
    }

    /**
     * 构造方法
     * @param message
     * @param cause
     */
    public NullEmployeeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * 构造方法
     * @param cause
     */
    public NullEmployeeException(Throwable cause) {
        super(cause);
    }

}