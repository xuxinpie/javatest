/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.employee;

import static org.mockito.Matchers.any;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

/**
 * @author Xinux
 * @version $Id: TestCase.java, v 0.1 2016-09-13 4:53 PM Xinux Exp $$
 */
@RunWith(PowerMockRunner.class)
public class TestCase {

    private EmployeeRepository employeeRepository;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeRepository();
    }

    @Test
    @PrepareForTest(EmployeeTableUtil.class)
    public void testThrowException() throws Exception {
        Employee employee = new Employee("1");

        // Mock 静态方法
        PowerMockito.mockStatic(EmployeeTableUtil.class);

        PowerMockito.when(EmployeeTableUtil.class, "update", any(Employee.class)).thenThrow(
            new NullEmployeeException());

        Assert.assertFalse(employeeRepository.update(employee));

        PowerMockito.verifyStatic();
        EmployeeTableUtil.update(employee);

    }

    @Test
    public void testPrivateMethod() throws Exception {
        Employee employee = new Employee("1");
        employee.setSalary(8000);

        double result = Whitebox.invokeMethod(employeeRepository, "bonus", employee);
        System.out.println(result);
        Assert.assertTrue(result == 800d);
    }

    @Test(expected = ExistedEmployeeException.class)
    public void testThrowException2() throws Exception {
        Employee employee = new Employee("1");
        employee.setId("1");

        EmployeeTableUtil tableUtil = PowerMockito.mock(EmployeeTableUtil.class);
        PowerMockito.when(tableUtil, "insert", any(Employee.class)).thenThrow(new ExistedEmployeeException());

        tableUtil.insert(employee);


    }

}