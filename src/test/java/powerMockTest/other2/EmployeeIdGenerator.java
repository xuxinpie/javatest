/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.other2;

/**
 * @author Xinux
 * @version $Id: EmployeeIdGenerator.java, v 0.1 2016-09-14 3:51 PM Xinux Exp $$
 */
public class EmployeeIdGenerator {

    public static String getNextId() {
        return "1111";
    }

}