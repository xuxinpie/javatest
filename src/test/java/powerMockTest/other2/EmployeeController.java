/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.other2;


/**
 * @author Xinux
 * @version $Id: EmployeeController.java, v 0.1 2016-09-14 2:27 PM Xinux Exp $$
 */
public class EmployeeController {

    private EmployeeService employeeService;

    public EmployeeController() {

    }

    public int getProjectedEmployeeCount() {

        final int actualEmployeeCount = employeeService.getEmployeeCount();
        return (int) Math.ceil(actualEmployeeCount * 1.2);
    }

    public void saveEmployee(Employee employee) {

        employeeService.saveEmployee(employee);
    }

    public boolean giveIncrementToAllEmployeesOf(int percentage) {
        try{
            EmployeeService.giveIncrementOf(percentage);
            return true;
        } catch(Exception e) {
            return false;
        }
    }

    /**
     * Setter method for property employeeService.
     *
     * @param employeeService value to be assigned to property employeeService
     */
    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }
}