/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.other2;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * @RunWith(PowerMockRunner.class)语句告诉JUnit用PowerMockRunner来执行测试。
 * @PrepareForTest(Employee.class)语句告诉PowerMock准备Employee类进行测试
 * 适用于模拟final类或有final, private, static, native方法的类。
 *
 * @author Xinux
 * @version $Id: EmployeeControllerTest.java, v 0.1 2016-09-14 2:28 PM Xinux Exp $$
 */
@RunWith(PowerMockRunner.class)
public class EmployeeControllerTest {

    @InjectMocks
    private EmployeeController controller = new EmployeeController();

    @Mock
    private EmployeeService    service;

    @Before
    public void setUp() {
        controller.setEmployeeService(service);
    }

    @Test
    public void shouldReturnProjectedCountOfEmployeesFromTheService() {

        PowerMockito.when(service.getEmployeeCount()).thenReturn(8);
        Assert.assertEquals(10, controller.getProjectedEmployeeCount());
    }

    @Test
    public void shouldInvokeSaveEmployeeOnTheServiceWhileSavingTheEmployee() {

        Employee employee = new Employee("1");
        controller.saveEmployee(employee);
        Mockito.verify(service).saveEmployee(employee);
    }

    @Test
    @PrepareForTest(EmployeeService.class)
    public void shouldReturnFalseWhenIncrementOf10PercentageIsNotGivenSuccessfully()
                                                                                    throws Exception {

        PowerMockito.mockStatic(EmployeeService.class);
        // 不会调用原有的方法, 前提是一个public的方法，如果是private的方法，仍然会被调用
        PowerMockito.doNothing().when(EmployeeService.class);
        Assert.assertTrue(controller.giveIncrementToAllEmployeesOf(10));

        // 此种方法会调用原来的方法
        //        PowerMockito.when(EmployeeService.class, "giveIncrementOf", anyInt());
        //        Assert.assertFalse(controller.giveIncrementToAllEmployeesOf(10));
    }

    @Test
    @PrepareForTest({ EmployeeIdGenerator.class, EmployeeService.class })
    public void shouldSendWelcomeEmailToNewEmployees() throws Exception {

        Employee employeeMock = PowerMockito.mock(Employee.class);
        PowerMockito.when(employeeMock.isNew()).thenReturn(true);
        // mock static class
        PowerMockito.mockStatic(EmployeeIdGenerator.class);
        WelcomeEmail welcomeEmailMock = PowerMockito.mock(WelcomeEmail.class);
        // mock create a new class
        PowerMockito.whenNew(WelcomeEmail.class)
            .withArguments(employeeMock, "Welcome to Mocking with PowerMock How-to!")
            .thenReturn(welcomeEmailMock);
        EmployeeService employeeService = new EmployeeService();
        employeeService.saveEmployee(employeeMock);

        PowerMockito.verifyNew(WelcomeEmail.class).withArguments(employeeMock,
            "Welcome to Mocking with PowerMock How-to!");
        Mockito.verify(welcomeEmailMock).send();
    }

    @Test
    @PrepareForTest(EmployeeService.class)
    public void testPrivateMethodCreateEmployee() throws Exception {
        EmployeeService service = PowerMockito.spy(new EmployeeService());
        Employee employeeMock = PowerMockito.mock(Employee.class);

        PowerMockito.when(employeeMock, "isNew").thenReturn(true);
        PowerMockito.doNothing().when(service, "createEmployee", employeeMock);

        service.save(employeeMock);
        PowerMockito.verifyPrivate(service).invoke("createEmployee", employeeMock);

    }

}