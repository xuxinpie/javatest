/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.other2;

/**
 * @author Xinux
 * @version $Id: Employee.java, v 0.1 2016-09-13 3:24 PM Xinux Exp $$
 */
public class Employee {

    public Employee(String id) {
        this.id = id;
    }

    private String id;

    private double salary;

    public boolean isNew() {
        return false;
    }

    public void create() {
        // do nothing
    }

    public void update() {
        // do nothing
    }

    /**
     * Getter method for property salary.
     *
     * @return property value of salary
     */
    public double getSalary() {
        return salary;
    }

    /**
     * Setter method for property salary.
     *
     * @param salary value to be assigned to property salary
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    /**
     * Getter method for property id.
     *
     * @return property value of id
     */
    public String getId() {
        return id;
    }

    /**
     * Setter method for property id.
     *
     * @param id value to be assigned to property id
     */
    public void setId(String id) {
        this.id = id;
    }
}