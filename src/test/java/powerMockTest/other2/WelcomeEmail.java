/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.other2;

/**
 * @author Xinux
 * @version $Id: WelcomeEmail.java, v 0.1 2016-09-14 3:45 PM Xinux Exp $$
 */
public class WelcomeEmail {

    public WelcomeEmail(final Employee employee, final String message) {
        throw new UnsupportedOperationException();
    }

    public void send() {
        throw new UnsupportedOperationException();
    }
}