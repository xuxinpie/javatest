/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.other2;


/**
 * @author Xinux
 * @version $Id: EmployeeService.java, v 0.1 2016-09-14 2:27 PM Xinux Exp $$
 */
public class EmployeeService {

    public int getEmployeeCount() {
        throw new UnsupportedOperationException();
    }

    // static method
    public static void giveIncrementOf(int percentage) {
        System.out.println("Throws UnsupportedOperationException");
        throw new UnsupportedOperationException();
    }

    public void saveEmployee(Employee employee) {
        if(employee.isNew()) {
            employee.setId(EmployeeIdGenerator.getNextId());
            employee.create();
            WelcomeEmail emailSender = new WelcomeEmail(employee,
                    "Welcome to Mocking with PowerMock How-to!");
            emailSender.send();
            return;
        }
        employee.update();
    }

    public void save(Employee employee) {
        if (employee.isNew()) {
            createEmployee(employee);
        }

    }

    // private method
    private void createEmployee(Employee employee) {

    }
}