/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.other;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

/**
 * @author Xinux
 * @version $Id: StaticClassSampleTest.java, v 0.1 2016-09-13 5:59 PM Xinux Exp $$
 */
public class StaticClassSampleTest {

    @Test
    public void testPrivateMethod() throws Exception {
        // 模拟 private的方法
        // spy方法需要使用doReturn方法时不会调用实际方法。
        MyClass spy = PowerMockito.spy(new MyClass());
        PowerMockito.doReturn(3).when(spy, "private_method", 1);
        Assert.assertEquals(3, spy.test_private_method(1));
        PowerMockito.verifyPrivate(spy, Mockito.times(1)).invoke("private_method", 1);
    }

    @Test
    public void testStaticReturnMethod() throws Exception {
        // 模拟 静态有返回值的方法
        PowerMockito.mockStatic(MyClass.class);
        Mockito.when(MyClass.static_return_method()).thenReturn(2);
        Assert.assertEquals(2, MyClass.static_return_method());
    }

    @Test
    public void testVoidMethod() throws Exception {
        // 模拟 不执行void的方法
        MyClass spy = PowerMockito.spy(new MyClass());
        PowerMockito.doNothing().when(spy).void_method();
        spy.void_method();
    }

    @Test
    public void testStaticMethod1() throws Exception {
        // 模拟 不执行没参数的静态void的方法
        PowerMockito.mockStatic(MyClass.class);
        PowerMockito.doNothing().when(MyClass.class, "static_void_method");
        MyClass.static_void_method();
    }

    @Test
    public void testStaticMethod2() throws Exception {
        // 模拟 不执行带参数的静态void的方法
        PowerMockito.mockStatic(MyClass.class);
        PowerMockito.doNothing().when(MyClass.class, "staticMethod", "123");
        MyClass.staticMethod("123");

        PowerMockito.doNothing().when(MyClass.class, "staticMethod", Mockito.anyString());
        MyClass.staticMethod("456");
    }

}