/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2016 All Rights Reserved.
 */
package powerMockTest.other;

/**
 * @author Xinux
 * @version $Id: MyClass.java, v 0.1 2016-09-13 5:58 PM Xinux Exp $$
 */
public class MyClass {

    final private int private_method(int a) {
        return a;
    }

    public int test_private_method(int a) {
        return private_method(a);
    }

    public static int static_return_method() {
        return 1;
    }

    void void_method() {
        throw new IllegalStateException("should not go here");
    }

    public static void static_void_method() {
        throw new IllegalStateException("should not go here");
    }

    public static void staticMethod(String a) {
        throw new IllegalStateException(a);
    }
}