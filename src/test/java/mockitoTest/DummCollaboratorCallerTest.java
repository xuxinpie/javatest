/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package mockitoTest;

import mockitoDemo.asynchronised.DummyCallback;
import mockitoDemo.asynchronised.DummyCaller;
import mockitoDemo.asynchronised.DummyCollaborator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.Times;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertThat;

/**
 *
 * 测试类
 * @author xinux
 * @version $Id: DummCollaboratorCallerTest.java, v 0.1 2015-08-14 10:41 hanlin.xx Exp $$
 */
public class DummCollaboratorCallerTest {

	private DummyCaller dummyCaller;

	@Mock
	private DummyCollaborator mockDummyCollaborator;

	@Captor
	private ArgumentCaptor<DummyCallback> dummyCallbackArgumentCaptor;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		dummyCaller = new DummyCaller(mockDummyCollaborator);
	}

	/*@TestCase
	public void testDoSomethingAsynchronouslyUsingDoAnswer() {
		final List<String> results = Arrays.asList("One", "Two", "Three");
		// 为callback执行一个同步anwser
		doAnswer(new Answer() {
			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				((DummyCallback)invocation.getArguments()[0]).onSuccess(results);
				return null;
			}
		}).when(mockDummyCollaborator).doSomethingAsynchronously(
				any(DummyCallback.class));

		// 调用被测试的函数
		dummyCaller.doSomethingAsynchronously();

		// 验证状态与结果
		verify(mockDummyCollaborator, times(1)).doSomethingAsynchronously(
				any(DummyCallback.class));
		assertThat(dummyCaller.getResult(), is(equalTo(results)));
	}


	@TestCase
	public void testDoSomethingAsynchronouslyUsingArgumentCaptor() {
		// 调用要被测试发函数
		dummyCaller.doSomethingAsynchronously();

		final List<String> results = Arrays.asList("One", "Two", "Three");

		// Let's call the callback. ArgumentCaptor.capture() works like a matcher.
		verify(mockDummyCollaborator, times(1)).doSomethingAsynchronously(
				dummyCallbackArgumentCaptor.capture());

		// 在执行回调之前验证结果
		assertThat(dummyCaller.getResult().isEmpty(), is(true));

		// 调用回调的onSuccess函数
		dummyCallbackArgumentCaptor.getValue().onSuccess(results);

		// 再次验证结果
		assertThat(dummyCaller.getResult(), is(equalTo(results)));
	}*/

}

