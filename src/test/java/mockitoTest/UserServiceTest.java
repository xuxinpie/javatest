/**
 * Alipay.com Inc.
 * Copyright (c) 2004-2015 All Rights Reserved.
 */
package mockitoTest;

import mockitoDemo.User;
import mockitoDemo.UserDao;
import mockitoDemo.UserService;

import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * @author hanlin.xx
 * @version $Id: UserServiceTest.java, v 0.1 2015-06-09 11:18 hanlin.xx Exp $$
 */
public class UserServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	/**
	 * 测试UserService的findUser方法
	 */
	@Test
	public void test01() {
		//mock被测试的类
		UserDao userDao = Mockito.mock(UserDao.class);

		UserService userService = new UserService(userDao);

		//设置预期的结果
		User user_test = new User(1, "Xinux");
		//mock方法调用的返回值
		Mockito.when(userDao.findUserById(1)).thenReturn(user_test);

		User user_really = userService.findUser(1);

		assertEquals(user_really.getUserName(), "Xinux");

		//验证UserDao的findUserById方法是否被调用
		Mockito.verify(userDao).findUserById(1);

	}

	/**
	 * 验证方法调用的次数
	 */
	@Test
	public void test02() {
		//mock待测试的类
		UserDao userDao = Mockito.mock(UserDao.class);

		UserService userService = new UserService(userDao);

		User user_test = new User(1, "Xinux");

		Mockito.when(userDao.findUserById(1)).thenReturn(user_test);

		userService.findUser(1);
		userService.findUser(1);

		//验证方法被调用的次数
		Mockito.verify(userDao, Mockito.times(2)).findUserById(1);

		//验证方法没有被调用
		Mockito.verify(userDao, Mockito.never()).findUserById(2);

	}

	/**
	 * 验证方法调用的顺序
	 */
	@Test
	public void test03() {
		UserDao userDao = Mockito.mock(UserDao.class);

		UserService userService = new UserService(userDao);

		//定义User对象
		User user_test1 = new User(1, "Xinux");
		User user_test2 = new User(2, "Hulda");

		//Mock方法调用的返回值
		Mockito.when(userDao.findUserById(1)).thenReturn(user_test1);
		Mockito.when(userDao.findUserById(2)).thenReturn(user_test2);

		userService.findUser(1);
		userService.findUser(2);

		//验证方法的调用顺序
		InOrder inOrder = Mockito.inOrder(userDao);
		inOrder.verify(userDao).findUserById(1);
		inOrder.verify(userDao).findUserById(2);
	}

	/**
	 * 对连续的调用执行不同的返回
	 * 默认情况下，对方法的对此调用只会返回最后一次的设置的值
	 */
	@Test
	public void test04() {
		//mock DAO方法
		UserDao userDao = Mockito.mock(UserDao.class);

		UserService userService = new UserService(userDao);

		User user_test1 = new User(1, "Xinux");
		User user_test2 = new User(2, "Hulda");

		Mockito.when(userDao.findUserById(1)).thenReturn(user_test1);
		Mockito.when(userDao.findUserById(1)).thenReturn(user_test2);

		if (logger.isInfoEnabled()) {
			logger.info(userService.findUser(1).getUserName());
			logger.info(userService.findUser(1).getUserName());
		}

	}

	/**
	 * 对连续的调用返回不同的值
	 */
	@Test
	public void test05() {

		UserDao userDao = Mockito.mock(UserDao.class);

		UserService userService = new UserService(userDao);

		//定义User对象
		User user_test1 = new User(1, "Xinux");
		User user_test2 = new User(2, "Hulda");

		//Mock方法调用的返回值
		Mockito.when(userDao.findUserById(1)).thenReturn(user_test1).thenReturn(user_test2);

		if (logger.isInfoEnabled()) {
			logger.info(userService.findUser(1).getUserName());
			logger.info(userService.findUser(1).getUserName());
		}

	}

	/**
	 * Mock提供了anyInt()，anyString()等方法匹配任意类型的值，调用时传入任意的参数
	 */
	@Test
	public void test06() {
		UserDao userDao = Mockito.mock(UserDao.class);

		UserService userService = new UserService(userDao);

		User user_test1 = new User(1, "Xinux");

		//使用的anyInt()
		Mockito.when(userDao.findUserById(Mockito.anyInt())).thenReturn(user_test1);

		Random random = new Random(System.currentTimeMillis());
		//返回一个0~99֮的整数
		int userId1 = random.nextInt(100);
		int max = 20;
		int min = 10;
		//返回10-20֮之间的整数
		int userId2 = random.nextInt(max) % (max - min) + min;


		if (logger.isInfoEnabled()) {
			logger.info(userService.findUser(userId1).getUserName());
			logger.info(userService.findUser(userId2).getUserName());
		}

	}

	/**
	 * 返回异常
	 */
	@Test
	public void test07() {
		UserDao userDao = Mockito.mock(UserDao.class);
		User user = new User(1, "Xinux");
		Mockito.when(userDao.findUserById(-1)).thenThrow(new RuntimeException());
		Mockito.verify(userDao).findUserById(-1);
	}



}

