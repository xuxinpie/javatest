/**
 * Bilibili.com Inc.
 * Copyright (c) 2009-2017 All Rights Reserved.
 */
package utilTest;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Xinux
 * @version $Id: GenIdTest.java, v 0.1 2017-01-19 3:12 PM Xinux Exp $$
 */
public class GenIdTest {

    public static void main(String[] args) {
        System.out.println(System.currentTimeMillis());
        String date = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
        System.out.println(date);
    }

}